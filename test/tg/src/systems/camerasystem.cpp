#include "camerasystem.h"

namespace tg {

CameraSystem::CameraSystem(sf::RenderTarget *renderTarget) :
    m_renderTarget(renderTarget)
{

}

void CameraSystem::update(sf::Time)
{
    for (const un::Entity &entity : entities()) {
        un::TransformComponent *transformComponent = entity.component<un::TransformComponent>();
        sf::View view = m_renderTarget->getView();
        view.setCenter(transformComponent->position());
        m_renderTarget->setView(view);
    }
}

}
