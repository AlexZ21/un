#include "movementsystem.h"

#include <cmath>

namespace tg {

void MovementSystem::update(sf::Time)
{
    for (const un::Entity &entity : entities()) {
        MovementComponent *movementComp = entity.component<MovementComponent>();
//        un::TransformComponent *transformComp = entity.component<un::TransformComponent>();
        un::Box2dComponent *box2dComponent = entity.component<un::Box2dComponent>();

        if (movementComp->velocity() == 0)
            continue;

        float moveX = movementComp->velocity() * std::cos(movementComp->direction() * (M_PI / 180));
        float moveY = movementComp->velocity() * std::sin(movementComp->direction() * (M_PI / 180));

        box2dComponent->setLinearVelocity(moveX, moveY);
    }
}

}
