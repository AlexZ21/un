#ifndef MOVEMENTSYSTEM_H
#define MOVEMENTSYSTEM_H

#include "../components/movementcomponent.h"

#include <un/core/scenesystem.h>
#include <un/components/transformcomponent.h>
#include <un/components/box2dcomponent.h>

namespace tg {

class MovementSystem : public un::SceneSystem<un::Filter::Requires<MovementComponent, un::TransformComponent,
        un::Box2dComponent>>
{
public:
    MovementSystem() = default;
    ~MovementSystem() = default;

    un::utils::TypeId typeId() {
        return un::utils::ClassTypeId<un::BaseSystem>::id<MovementSystem>();
    }

    void update(sf::Time);

};

}

#endif // MOVEMENTSYSTEM_H
