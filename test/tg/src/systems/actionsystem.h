#ifndef ACTIONSYSTEM_H
#define ACTIONSYSTEM_H

#include "../components/actioncomponent.h"

#include <un/core/scenesystem.h>

namespace tg {

class ActionSystem : public un::SceneSystem<un::Filter::Requires<ActionComponent>>
{
public:
    ActionSystem() = default;
    ~ActionSystem() = default;

    un::utils::TypeId typeId() {
        return un::utils::ClassTypeId<un::BaseSystem>::id<ActionSystem>();
    }

    void update(sf::Time);

};

}

#endif // ACTIONSYSTEM_H
