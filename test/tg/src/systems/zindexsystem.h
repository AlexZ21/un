#ifndef ZINDEXSYSTEM_H
#define ZINDEXSYSTEM_H

#include "../components/zindexcomponent.h"

#include <un/core/scenesystem.h>
#include <un/components/rendercomponent.h>
#include <un/components/transformcomponent.h>

namespace tg {

class ZIndexSystem : public un::SceneSystem<un::Filter::Requires<ZIndexComponent, un::RenderComponent,
        un::TransformComponent>>
{
public:
    ZIndexSystem() = default;
    ~ZIndexSystem() = default;

    un::utils::TypeId typeId() {
        return un::utils::ClassTypeId<un::BaseSystem>::id<ZIndexSystem>();
    }

    void update(sf::Time);

};

}

#endif // ZINDEXSYSTEM_H
