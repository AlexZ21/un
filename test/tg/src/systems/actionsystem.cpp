#include "actionsystem.h"

namespace tg {

void ActionSystem::update(sf::Time)
{
    for (const un::Entity &entity : entities()) {
        ActionComponent *actionComponent = entity.component<ActionComponent>();

        if (!actionComponent->action)
            continue;

        if (actionComponent->action->isFinished()) {
            delete actionComponent->action;
            actionComponent->action = nullptr;
            continue;
        }

        if (!actionComponent->action->isRunning())
            actionComponent->action->start();

        actionComponent->action->update();
    }
}

}
