#ifndef CAMERASYSTEM_H
#define CAMERASYSTEM_H

#include "../components/playercomponent.h"

#include <un/core/scenesystem.h>
#include <un/components/transformcomponent.h>

#include <SFML/Graphics/RenderTarget.hpp>

namespace tg {

class CameraSystem : public un::SceneSystem<un::Filter::Requires<PlayerComponent, un::TransformComponent>>
{
public:
    CameraSystem(sf::RenderTarget *renderTarget);
    ~CameraSystem() = default;

    un::utils::TypeId typeId() {
        return un::utils::ClassTypeId<un::BaseSystem>::id<CameraSystem>();
    }

    void update(sf::Time);

private:
    sf::RenderTarget *m_renderTarget;

};

}

#endif // CAMERASYSTEM_H
