#include "zindexsystem.h"

namespace tg {

void ZIndexSystem::update(sf::Time)
{
    for (const un::Entity &entity : entities()) {
        un::RenderComponent *renderComponent = entity.component<un::RenderComponent>();
        un::TransformComponent *transformComponent = entity.component<un::TransformComponent>();
        un::DrawableGroup *drawableGroup = renderComponent->drawableGroup();
        if (drawableGroup) {
            for (un::Drawable *drawable : *drawableGroup->drawables(renderComponent->state())) {
                sf::FloatRect r = drawable->globalBounds();
                r =  transformComponent->transform().transformRect(r);
                drawable->setZIndex(r.top + r.height);
            }
        }
    }
}

}
