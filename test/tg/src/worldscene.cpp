#include "worldscene.h"
#include "player.h"
#include "action.h"

#include "components/playercomponent.h"
#include "components/statecomponent.h"
#include "components/actioncomponent.h"
#include "components/movementcomponent.h"
#include "components/zindexcomponent.h"

#include "systems/actionsystem.h"
#include "systems/movementsystem.h"
#include "systems/zindexsystem.h"
#include "systems/camerasystem.h"

#include <un/graphics.h>
#include <un/components.h>

#include <functional>

#define MOVE_UP 0
#define MOVE_DOWN 1
#define MOVE_LEFT 2
#define MOVE_RIGHT 3

namespace tg {

un::DrawableGroup *createTestEntityDg() {
    un::DrawableGroup *dg = new un::DrawableGroup();
    un::Texture *player_stay_left = un::Application::manager<un::Textures>()->
            loadStaticTexture("player_stay_left", "local:char.png", sf::IntRect(0, 192, 64, 64));
    un::TexturedQuad *player_stay_left_quad = new un::TexturedQuad(player_stay_left);
    dg->add(StateComponent::STAY_LEFT, player_stay_left_quad);
    return dg;
}


WorldScene::WorldScene() :
    Scene()
{
    // Add systems to world
    addSystem(new ActionSystem());
    addSystem(new MovementSystem());
    addSystem(new ZIndexSystem());
    addSystem(new CameraSystem(un::Application::window()));

    initPlayerEntity();
    initCameraEntity();

    initActionKeyBinder();

    // Test map
    un::Entity map = createEntity();
    un::TileMap *map1 = new un::TileMap("assets:untitled.tmx");
    un::DrawableGroup *map1Dg = new un::DrawableGroup();
    map1Dg->add(map1);
    map.component<un::RenderComponent>()->setDrawableGroup(map1Dg);
    map.enable();

    // Build1
    un::Entity b1 = createEntity();
    un::TileMap *b1map = new un::TileMap("assets:build1.tmx");
    un::DrawableGroup *b1Dg = new un::DrawableGroup();
    b1Dg->add(b1map);
    b1.component<un::RenderComponent>()->setDrawableGroup(b1Dg);
    b1.component<un::TransformComponent>()->setPosition(400, 100);
    b1.enable();

    // test entities
    un::Entity t1e = createEntity();
    t1e.addComponent<ZIndexComponent>();
    t1e.component<un::RenderComponent>()->setDrawableGroup(createTestEntityDg());
    t1e.component<un::RenderComponent>()->setState(StateComponent::STAY_LEFT);
//    t1e.component<un::TransformComponent>()->setPosition(200, 200);

    std::vector<sf::Vector2f> verts;
    verts.push_back(sf::Vector2f(23, 59));
    verts.push_back(sf::Vector2f(23, 51));
    verts.push_back(sf::Vector2f(20, 47));
    verts.push_back(sf::Vector2f(20, 36));
    verts.push_back(sf::Vector2f(21, 28));
    verts.push_back(sf::Vector2f(21, 19));
    verts.push_back(sf::Vector2f(27, 13));
    verts.push_back(sf::Vector2f(36, 13));
    verts.push_back(sf::Vector2f(42, 19));
    verts.push_back(sf::Vector2f(42, 29));
    verts.push_back(sf::Vector2f(41, 37));
    verts.push_back(sf::Vector2f(41, 47));
    verts.push_back(sf::Vector2f(41, 59));
    verts.push_back(sf::Vector2f(41, 59));
    verts.push_back(sf::Vector2f(33, 62));

    t1e.component<un::Box2dComponent>()->createBody(verts);

    t1e.component<un::Box2dComponent>()->setPosition(100, 100);

    sf::FloatRect lb = t1e.component<un::RenderComponent>()->drawableGroup()->localBounds(StateComponent::STAY_LEFT);
    t1e.component<un::TransformComponent>()->setOrigin(lb.width / 2, lb.height /2 );


//    t1e.component<un::CollisionComponent>()->addPolygon(verts);

    t1e.enable();

//    m_moveMouseEventListner = std::make_unique<un::EventListener>
//            (std::bind(&WorldScene::moveMouseEventCallback, this, std::placeholders::_1));
}

WorldScene::~WorldScene()
{

}

un::Entity WorldScene::player() const
{
    return m_playerEntity;
}

un::Entity WorldScene::camera() const
{
    return m_cameraEntity;
}

void WorldScene::onSetScene()
{
    // Setup scene action key binder to application
    if (m_actionKeyBinder)
        un::Application::setActionKeyBinder(m_actionKeyBinder.get());
//    un::Application::setEventListener(un::Event::MouseMoved, m_moveMouseEventListner.get());
}

void WorldScene::onUnsetScene()
{
    if (m_actionKeyBinder.get() == un::Application::actionKeyBinder())
        un::Application::setActionKeyBinder(nullptr);
//    un::Application::unsetEventListener(m_moveMouseEventListner.get());
}

void WorldScene::initPlayerEntity()
{
    m_playerEntity = createEntity();
    m_player.reset(new Player(m_playerEntity));
}

void WorldScene::initCameraEntity()
{
    m_cameraEntity = createEntity(false);
    m_cameraEntity.enable();
}

void WorldScene::initActionKeyBinder()
{
    m_actionKeyBinder = std::make_unique<un::ActionKeyBinder>();

    // ####
    // Start player movement up
    m_actionKeyBinder->addAction("start_player_run_up", [this](){
        un::Application::sendEvent(PLAYER_RUN_UP_EVENT);
    });

    // Stop player movement up
    m_actionKeyBinder->addAction("stop_player_run_up", [this](){
        un::Application::sendEvent(PLAYER_STAY_UP_EVENT);
    });

    m_actionKeyBinder->bind("start_player_run_up", un::ActionKeyBinder::PressOnce, sf::Keyboard::W);
    m_actionKeyBinder->bind("stop_player_run_up", un::ActionKeyBinder::ReleaseOnce, sf::Keyboard::W);

    // ####
    // Start player movement down
    m_actionKeyBinder->addAction("start_player_run_down", [this](){
        un::Application::sendEvent(PLAYER_RUN_DOWN_EVENT);
    });

    // Stop player movement down
    m_actionKeyBinder->addAction("stop_player_run_down", [this](){
        un::Application::sendEvent(PLAYER_STAY_DOWN_EVENT);
    });

    m_actionKeyBinder->bind("start_player_run_down", un::ActionKeyBinder::PressOnce, sf::Keyboard::S);
    m_actionKeyBinder->bind("stop_player_run_down", un::ActionKeyBinder::ReleaseOnce, sf::Keyboard::S);

    // ####
    // Start player movement left
    m_actionKeyBinder->addAction("start_player_run_left", [this](){
        un::Application::sendEvent(PLAYER_RUN_LEFT_EVENT);
    });

    // Stop player movement left
    m_actionKeyBinder->addAction("stop_player_run_left", [this](){
        un::Application::sendEvent(PLAYER_STAY_LEFT_EVENT);
    });

    m_actionKeyBinder->bind("start_player_run_left", un::ActionKeyBinder::PressOnce, sf::Keyboard::A);
    m_actionKeyBinder->bind("stop_player_run_left", un::ActionKeyBinder::ReleaseOnce, sf::Keyboard::A);

    // ####
    // Start player movement right
    m_actionKeyBinder->addAction("start_player_run_right", [this](){
        un::Application::sendEvent(PLAYER_RUN_RIGHT_EVENT);
    });

    // Stop player movement right
    m_actionKeyBinder->addAction("stop_player_run_right", [this](){
        un::Application::sendEvent(PLAYER_STAY_RIGHT_EVENT);
    });

    m_actionKeyBinder->bind("start_player_run_right", un::ActionKeyBinder::PressOnce, sf::Keyboard::D);
    m_actionKeyBinder->bind("stop_player_run_right", un::ActionKeyBinder::ReleaseOnce, sf::Keyboard::D);
}

}
