#ifndef ACTION_H
#define ACTION_H

#include <SFML/System/Time.hpp>
#include <SFML/System/Clock.hpp>

namespace tg {

class Action
{
public:
    Action();
    virtual ~Action() = default;

    sf::Time lifetime() const;
    void setLifetime(sf::Time time);
    void setInfinity();

    void start();
    void finish();
    bool isRunning() const;
    bool isFinished() const;

    void update();

protected:
    virtual void onStart() {}
    virtual void onFinish() {}
    virtual void onUpdate(sf::Time) {}

private:
    bool m_infinity;
    sf::Time m_lifetime;
    bool m_running;
    bool m_finished;
    sf::Clock m_clock;
};

}

#endif // ACTION_H
