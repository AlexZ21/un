#ifndef DEFINES_H
#define DEFINES_H

#include <stdint.h>

//! Actions for key binder


//! Events
const int32_t PLAYER_RUN_UP_EVENT = 10000;
const int32_t PLAYER_RUN_DOWN_EVENT = 10001;
const int32_t PLAYER_RUN_LEFT_EVENT = 10002;
const int32_t PLAYER_RUN_RIGHT_EVENT = 10003;

const int32_t PLAYER_STAY_UP_EVENT = 10100;
const int32_t PLAYER_STAY_DOWN_EVENT = 10101;
const int32_t PLAYER_STAY_LEFT_EVENT = 10102;
const int32_t PLAYER_STAY_RIGHT_EVENT = 10103;

#endif // DEFINES_H
