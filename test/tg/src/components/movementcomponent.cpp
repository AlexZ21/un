#include "movementcomponent.h"

#include <cmath>

namespace tg {

MovementComponent::MovementComponent() :
    m_velocity(0),
    m_direction(0)
{

}

float MovementComponent::velocity() const
{
    return m_velocity;
}

float MovementComponent::direction() const
{
    return m_direction;
}

void MovementComponent::addMovement(int32_t type, float velocity, float direction)
{
    m_movements.emplace(type, std::make_pair(velocity, direction));
    recalc();
}

void MovementComponent::removeMovement(int32_t type)
{
    auto it = m_movements.find(type);
    if (it == m_movements.end())
        return;
    m_movements.erase(it);
    recalc();
}

void MovementComponent::removeAllMovements()
{
    m_movements.clear();
    recalc();
}

void MovementComponent::recalc()
{
    m_velocity = 0;

    if (!m_movements.empty()) {
        m_direction = 0;

        float vecX = 0;
        float vecY = 0;

        for (const auto &it : m_movements) {
            const std::pair<float, float> &movement = it.second;
            vecX += movement.first * std::cos((movement.second - 90) * (M_PI / 180));
            vecY -= movement.first * std::sin((movement.second - 90) * (M_PI / 180));
        }

        m_velocity = std::sqrt((vecX * vecX) + (vecY * vecY));
        m_direction = std::atan2(vecX, vecY) / (M_PI / 180);
    }
}

}
