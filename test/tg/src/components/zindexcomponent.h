#ifndef ZINDEXCOMPONENT_H
#define ZINDEXCOMPONENT_H

#include <un/core.h>

namespace tg {

class ZIndexComponent : public un::Component
{
public:
    ZIndexComponent() = default;
    ~ZIndexComponent() = default;

};

}

#endif // ZINDEXCOMPONENT_H
