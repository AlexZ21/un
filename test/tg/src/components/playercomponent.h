#ifndef PLAYERCOMPONENT_H
#define PLAYERCOMPONENT_H

#include <un/core.h>

namespace tg {

class PlayerComponent : public un::Component
{
public:
    PlayerComponent();
    ~PlayerComponent() = default;

};

}

#endif // PLAYERCOMPONENT_H
