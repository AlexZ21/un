#include "statecomponent.h"

namespace tg {

StateComponent::StateComponent() :
    m_state(STAY_LEFT)
{

}

StateComponent::State StateComponent::state() const
{
    return m_state;
}

void StateComponent::setState(State state)
{
    m_state = state;
    if (m_onStateChanged)
        m_onStateChanged(m_state);
}

void StateComponent::onStateChanged(const std::function<void (StateComponent::State)> &onStateChanged)
{
    m_onStateChanged = onStateChanged;
}

}
