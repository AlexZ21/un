#ifndef MOVEMENTCOMPONENT_H
#define MOVEMENTCOMPONENT_H

#include <un/core.h>

#include <unordered_map>
#include <functional>

namespace tg {

class MovementComponent : public un::Component
{
public:
    MovementComponent();
    ~MovementComponent() = default;

    float velocity() const;
    float direction() const;

    void addMovement(int32_t type, float velocity, float direction);
    void removeMovement(int32_t type);
    void removeAllMovements();

private:
    void recalc();

private:
    float m_velocity;
    float m_direction;
    std::unordered_map<int32_t, std::pair<float, float>> m_movements;

};

}

#endif // MOVEMENTCOMPONENT_H
