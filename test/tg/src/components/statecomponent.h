#ifndef STATECOMPONENT_H
#define STATECOMPONENT_H

#include <un/core.h>

#include <functional>

namespace tg {

class StateComponent : public un::Component
{
public:
    enum State {
        STAY_UP,
        STAY_UP_LEFT,
        STAY_UP_RIGHT,
        STAY_DOWN,
        STAY_DOWN_LEFT,
        STAY_DOWN_RIGHT,
        STAY_LEFT,
        STAY_RIGHT,

        RUN_UP,
        RUN_UP_LEFT,
        RUN_UP_RIGHT,
        RUN_DOWN,
        RUN_DOWN_LEFT,
        RUN_DOWN_RIGHT,
        RUN_LEFT,
        RUN_RIGHT
    };

    StateComponent();
    ~StateComponent() = default;

    State state() const;
    void setState(State state);

    void onStateChanged(const std::function<void (State)> &onStateChanged);

private:
    State m_state;
    std::function<void (State)> m_onStateChanged;
};

}

#endif // STATECOMPONENT_H
