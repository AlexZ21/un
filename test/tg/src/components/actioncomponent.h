#ifndef ACTIONCOMPONENT_H
#define ACTIONCOMPONENT_H

#include "../action.h"

#include <un/un_defines.h>
#include <un/core.h>

#include <vector>

namespace tg {

class ActionComponent : public un::Component
{
public:
    ActionComponent();
    ~ActionComponent() = default;

    Action *action = nullptr;

    void addAction(Action *action);
    void removeAction(Action *action);

    const std::vector<uuptr<Action>> &actions() const;

private:
    std::vector<uuptr<Action>> m_actions;

};

}

#endif // ACTIONCOMPONENT_H
