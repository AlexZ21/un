#include "player.h"

#include "components/playercomponent.h"
#include "components/statecomponent.h"
#include "components/actioncomponent.h"
#include "components/movementcomponent.h"
#include "components/zindexcomponent.h"

#include <un/components.h>
#include <un/graphics.h>
#include <un/utils/log.h>

#define RUN_UP 0
#define RUN_DOWN 1
#define RUN_LEFT 2
#define RUN_RIGHT 3

namespace tg {

un::DrawableGroup *createTestChar() {
    un::DrawableGroup *dg = new un::DrawableGroup();

    un::Texture *player_stay_left = un::Application::manager<un::Textures>()->
            loadStaticTexture("player_stay_left", "assets:char.png", sf::IntRect(0, 192, 64, 64));

    //    un::Texture *player_stay_90 = un::Application::manager<un::Textures>()->
    //            loadStaticTexture("player_stay_90", "local:char.png", sf::IntRect(0, 128, 64, 64));

    //    un::Texture *player_stay_180 = un::Application::manager<un::Textures>()->
    //            loadStaticTexture("player_stay_180", "local:char.png", sf::IntRect(0, 64, 64, 64));

    //    un::Texture *player_stay_270 = un::Application::manager<un::Textures>()->
    //            loadStaticTexture("player_stay_270", "local:char.png", sf::IntRect(0, 0, 64, 64));

    un::TexturedQuad *player_stay_left_quad = new un::TexturedQuad(player_stay_left);
    dg->add(StateComponent::STAY_LEFT, player_stay_left_quad);

    //    un::TexturedQuad *player_stay_90_quad = new un::TexturedQuad(player_stay_90);
    //    dg->add(StateComponent::STAY_90, player_stay_90_quad);

    //    un::TexturedQuad *player_stay_180_quad = new un::TexturedQuad(player_stay_180);
    //    dg->add(StateComponent::STAY_180, player_stay_180_quad);

    //    un::TexturedQuad *player_stay_270_quad = new un::TexturedQuad(player_stay_270);
    //    dg->add(StateComponent::STAY_270, player_stay_270_quad);

    //    un::Texture *player_run_0 = un::Application::manager<un::Textures>()->
    //            loadAnimatedTexture("player_run_0", "local:char.png", true,
    //                                sf::milliseconds(50), sf::Vector2i(64, 64),
    //                                sf::Vector2i(1, 3), sf::Vector2i(8, 3));

    //    un::Texture *player_run_90 = un::Application::manager<un::Textures>()->
    //            loadAnimatedTexture("player_run_90", "local:char.png", true,
    //                                sf::milliseconds(50), sf::Vector2i(64, 64),
    //                                sf::Vector2i(1, 2), sf::Vector2i(8, 2));

    //    un::Texture *player_run_180 = un::Application::manager<un::Textures>()->
    //            loadAnimatedTexture("player_run_180", "local:char.png", true,
    //                                sf::milliseconds(50), sf::Vector2i(64, 64),
    //                                sf::Vector2i(1, 1), sf::Vector2i(8, 1));

    //    un::Texture *player_run_270 = un::Application::manager<un::Textures>()->
    //            loadAnimatedTexture("player_run_270", "local:char.png", true,
    //                                sf::milliseconds(50), sf::Vector2i(64, 64),
    //                                sf::Vector2i(1, 0), sf::Vector2i(8, 0));

    //    un::TexturedQuad *player_run_0_quad = new un::TexturedQuad(player_run_0);
    //    dg->add(StateComponent::RUN_0, player_run_0_quad);

    //    un::TexturedQuad *player_run_90_quad = new un::TexturedQuad(player_run_90);
    //    dg->add(StateComponent::RUN_90, player_run_90_quad);

    //    un::TexturedQuad *player_run_180_quad = new un::TexturedQuad(player_run_180);
    //    dg->add(StateComponent::RUN_180, player_run_180_quad);

    //    un::TexturedQuad *player_run_270_quad = new un::TexturedQuad(player_run_270);
    //    dg->add(StateComponent::RUN_270, player_run_270_quad);

    return dg;
}

Player::Player(un::Entity entity) :
    m_entity(entity)
{
    initEntity();
    initEventListeners();
}

Player::~Player()
{
    // Unset listeners
    // Run up
    un::Application::unsetEventListener(m_playerRunUpEventListener.get());
    un::Application::unsetEventListener(m_playerStayUpEventListener.get());
    // Run down
    un::Application::unsetEventListener(m_playerRunDownEventListener.get());
    un::Application::unsetEventListener(m_playerStayDownEventListener.get());
    // Run left
    un::Application::unsetEventListener(m_playerRunLeftEventListener.get());
    un::Application::unsetEventListener(m_playerStayLeftEventListener.get());
    // Run right
    un::Application::unsetEventListener(m_playerRunRightEventListener.get());
    un::Application::unsetEventListener(m_playerStayRightEventListener.get());
    // Mouse capture for rotating player
    un::Application::unsetEventListener(m_mouseMoveEventListener.get());
}

void Player::initEntity()
{
    m_entity.addComponent<PlayerComponent>();
    m_entity.addComponent<StateComponent>();
    m_entity.component<StateComponent>()->onStateChanged([this](StateComponent::State state){
        m_entity.component<un::RenderComponent>()->setState(state);
    });
    m_entity.addComponent<ActionComponent>();
    m_entity.addComponent<MovementComponent>();
    m_entity.addComponent<ZIndexComponent>();

    m_entity.component<un::RenderComponent>()->setDrawableGroup(createTestChar());
    m_entity.component<un::RenderComponent>()->setState(StateComponent::STAY_LEFT);

    std::vector<sf::Vector2f> verts;
    verts.push_back(sf::Vector2f(23, 59));
    verts.push_back(sf::Vector2f(23, 51));
    verts.push_back(sf::Vector2f(20, 47));
    verts.push_back(sf::Vector2f(20, 36));
    verts.push_back(sf::Vector2f(21, 28));
    verts.push_back(sf::Vector2f(21, 19));
    verts.push_back(sf::Vector2f(27, 13));
    verts.push_back(sf::Vector2f(36, 13));
    verts.push_back(sf::Vector2f(42, 19));
    verts.push_back(sf::Vector2f(61, 19));
    verts.push_back(sf::Vector2f(42, 29));
    verts.push_back(sf::Vector2f(41, 37));
    verts.push_back(sf::Vector2f(41, 47));
    verts.push_back(sf::Vector2f(41, 59));
    verts.push_back(sf::Vector2f(41, 59));
    verts.push_back(sf::Vector2f(33, 62));

//    m_entity.component<un::CollisionComponent>()->setVertices(verts);
    sf::FloatRect lb = m_entity.component<un::RenderComponent>()->drawableGroup()->localBounds(StateComponent::STAY_LEFT);

    LDEBUG(lb.width);

    m_entity.component<un::Box2dComponent>()->createBody(verts, un::Box2dComponent::Dynamic/*,
                                                         sf::Vector2f(lb.width / 2, lb.height / 2)*/);
    m_entity.component<un::Box2dComponent>()->setPosition(200, 200);
//    m_entity.component<un::Box2dComponent>()->setFixedRotation(true);



    m_entity.component<un::TransformComponent>()->setOrigin(lb.width / 2, lb.height /2 );

    m_entity.enable();
}

void Player::initEventListeners()
{
    // ####
    // Run up event
    m_playerRunUpEventListener = std::make_unique<un::EventListener>([this](un::Event *){
            MovementComponent *mc = m_entity.component<MovementComponent>();
            mc->addMovement(RUN_UP, 1, 270);
            updateState();
});
    un::Application::setEventListener(PLAYER_RUN_UP_EVENT, m_playerRunUpEventListener.get());

    // Stay up event
    m_playerStayUpEventListener = std::make_unique<un::EventListener>([this](un::Event *){
            MovementComponent *mc = m_entity.component<MovementComponent>();
            mc->removeMovement(RUN_UP);
            updateState();
});
    un::Application::setEventListener(PLAYER_STAY_UP_EVENT, m_playerStayUpEventListener.get());

    // ####
    // Run down event
    m_playerRunDownEventListener = std::make_unique<un::EventListener>([this](un::Event *){
            MovementComponent *mc = m_entity.component<MovementComponent>();
            mc->addMovement(RUN_DOWN, 1, 90);
            updateState();
});
    un::Application::setEventListener(PLAYER_RUN_DOWN_EVENT, m_playerRunDownEventListener.get());

    // Stay down event
    m_playerStayDownEventListener = std::make_unique<un::EventListener>([this](un::Event *){
            MovementComponent *mc = m_entity.component<MovementComponent>();
            mc->removeMovement(RUN_DOWN);
            updateState();
});
    un::Application::setEventListener(PLAYER_STAY_DOWN_EVENT, m_playerStayDownEventListener.get());

    // ####
    // Run Left event
    m_playerRunLeftEventListener = std::make_unique<un::EventListener>([this](un::Event *){
            MovementComponent *mc = m_entity.component<MovementComponent>();
            mc->addMovement(RUN_LEFT, 1, 180);
            updateState();
});
    un::Application::setEventListener(PLAYER_RUN_LEFT_EVENT, m_playerRunLeftEventListener.get());

    // Stay Left event
    m_playerStayLeftEventListener = std::make_unique<un::EventListener>([this](un::Event *){
            MovementComponent *mc = m_entity.component<MovementComponent>();
            mc->removeMovement(RUN_LEFT);
            updateState();
});
    un::Application::setEventListener(PLAYER_STAY_LEFT_EVENT, m_playerStayLeftEventListener.get());

    // ####
    // Run Right event
    m_playerRunRightEventListener = std::make_unique<un::EventListener>([this](un::Event *){
            MovementComponent *mc = m_entity.component<MovementComponent>();
            mc->addMovement(RUN_RIGHT, 1, 0);
            updateState();
});
    un::Application::setEventListener(PLAYER_RUN_RIGHT_EVENT, m_playerRunRightEventListener.get());

    // Stay Right event
    m_playerStayRightEventListener = std::make_unique<un::EventListener>([this](un::Event *){
            MovementComponent *mc = m_entity.component<MovementComponent>();
            mc->removeMovement(RUN_RIGHT);
            updateState();
});
    un::Application::setEventListener(PLAYER_STAY_RIGHT_EVENT, m_playerStayRightEventListener.get());

    // ####
    // Rotating player by mouse
    m_mouseMoveEventListener = std::make_unique<un::EventListener>([this](un::Event *event){
            un::MouseMovedEvent *e = reinterpret_cast<un::MouseMovedEvent *>(event);
            m_mouseLastPosition = sf::Vector2i(e->x, e->y);
            updateState();
});
    un::Application::setEventListener(un::Event::MouseMoved, m_mouseMoveEventListener.get());

}

void Player::updateState()
{

}

}
