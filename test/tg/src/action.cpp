#include "action.h"

namespace tg {

Action::Action() :
    m_infinity(true),
    m_lifetime(sf::milliseconds(0)),
    m_running(false),
    m_finished(false)
{

}

sf::Time Action::lifetime() const
{
    return m_lifetime;
}

void Action::setLifetime(sf::Time time)
{
    m_lifetime = time;
    m_infinity = false;
}

void Action::setInfinity()
{
    m_infinity = true;
}

void Action::start()
{
    m_running = true;
    m_clock.restart();
    onStart();
}

void Action::finish()
{
    m_running = false;
    onFinish();
}

bool Action::isRunning() const
{
    return m_running;
}

bool Action::isFinished() const
{
    return m_finished;
}

void Action::update()
{
    if (!m_running)
        return;

    sf::Time time = m_clock.getElapsedTime();

    if (!m_infinity && time > m_lifetime) {
        onFinish();
        return;
    }

    onUpdate(time);
}

}
