#ifndef PLAYER_H
#define PLAYER_H

#include "defines.h"

#include <un/core.h>

#include <SFML/System/Vector2.hpp>

namespace tg {

class Player
{
public:
    Player(un::Entity entity);
    ~Player();

private:
    void initEntity();
    void initEventListeners();

    void updateState();

private:
    un::Entity m_entity;

    //! Event listners
    //! Run up
    uuptr<un::EventListener> m_playerRunUpEventListener;
    uuptr<un::EventListener> m_playerStayUpEventListener;
    //! Run down
    uuptr<un::EventListener> m_playerRunDownEventListener;
    uuptr<un::EventListener> m_playerStayDownEventListener;
    //! Run left
    uuptr<un::EventListener> m_playerRunLeftEventListener;
    uuptr<un::EventListener> m_playerStayLeftEventListener;
    //! Run right
    uuptr<un::EventListener> m_playerRunRightEventListener;
    uuptr<un::EventListener> m_playerStayRightEventListener;
    //! Mouse capture for rotating player
    uuptr<un::EventListener> m_mouseMoveEventListener;
    sf::Vector2i m_mouseLastPosition;

};

}

#endif // PLAYER_H
