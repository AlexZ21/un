#include "game.h"
#include "worldscene.h"

#include <un/core.h>

using namespace un;
using namespace tg;

int main(int argc, char *argv[])
{
    LENABLE;

    Application::create();
    Application::showWindow(800, 600, "Game", false);

    Application::manager<Assets>()->addSource("assets", Assets::Local, "./assets");

    Application::manager<Lua>()->file("assets:scripts/main.lua");

    Game::create();

    tg::WorldScene *scene = new tg::WorldScene();
    Game::setScene(scene);

    return Application::exec();
}
