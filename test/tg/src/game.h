#ifndef GAME_H
#define GAME_H

#include "scene.h"

#include <un/un_defines.h>

namespace tg {

class Scene;

class Game
{
public:
    static Game *instance();
    static void create();

    static Scene *scene();
    static void setScene(Scene *scene);

private:
    Game();
    ~Game();

private:
    static Game *m_game;

    uuptr<Scene> m_scene;

};

}

#endif // GAME_H
