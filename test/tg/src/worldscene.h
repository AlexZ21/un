﻿#ifndef WORLDSCENE_H
#define WORLDSCENE_H

#include "defines.h"
#include "scene.h"

namespace tg {

class Player;

class WorldScene : public Scene
{
public:
    WorldScene();
    ~WorldScene();

    //! Player
    un::Entity player() const;

    //! Camera
    un::Entity camera() const;

    virtual void onSetScene();
    virtual void onUnsetScene();

private:
    void initPlayerEntity();
    void initCameraEntity();

    void initActionKeyBinder();

private:
    un::Entity m_playerEntity;
    un::Entity m_cameraEntity;

    uuptr<Player> m_player;

    uuptr<un::EventListener> m_moveMouseEventListner;
    sf::Vector2i m_lastMousePos;

};

}

#endif // WORLDSCENE_H
