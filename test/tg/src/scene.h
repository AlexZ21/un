#ifndef SCENE_H
#define SCENE_H

#include <un/un_defines.h>
#include <un/core.h>

namespace tg {

class Scene : public un::Scene
{
public:
    Scene();
    virtual ~Scene() = default;

    un::ActionKeyBinder *actionKeyBinder() const;

    virtual void onSetScene() = 0;
    virtual void onUnsetScene() = 0;

protected:
    uuptr<un::ActionKeyBinder> m_actionKeyBinder;

};

}

#endif // SCENE_H
