#include "game.h"

#include <un/core.h>
#include <un/utils.h>

#define CHECK_INSTANCE_RET(_return_) \
    while (!m_game) { \
    LERR("Game instance not created"); \
    return _return_; \
    }

#define CHECK_INSTANCE \
    while (!m_game) { \
    LERR("Game instance not created"); \
    return; \
    }

#define CHECK_INSTANCE_NULL CHECK_INSTANCE_RET(nullptr)

namespace tg {

Game *Game::m_game = nullptr;

Game *Game::instance()
{
    if (m_game)
        return m_game;
    static Game game;
    m_game = &game;
    return m_game;
}

void Game::create()
{
    Game::instance();
}

Scene *Game::scene()
{
    CHECK_INSTANCE_NULL;
    return m_game->m_scene.get();
}

void Game::setScene(Scene *scene)
{
    CHECK_INSTANCE;
    if (m_game->m_scene) {
        m_game->m_scene->onUnsetScene();
        m_game->m_scene.release();
    }
    m_game->m_scene.reset(scene);
    scene->onSetScene();
    // Setup scene to application
    un::Application::setScene(scene);
}

Game::Game()
{

}

Game::~Game()
{
    if (m_scene) {
        m_game->m_scene->onUnsetScene();
        un::Application::setScene(nullptr);
    }
}

}
