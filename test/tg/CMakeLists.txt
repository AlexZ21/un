cmake_minimum_required(VERSION 3.1)

project(tg)

set(CMAKE_CXX_STANDARD 14)

file(GLOB_RECURSE TG_SRC
    src/*.cpp
    src/*.c
    src/*.h
    )

add_executable(${PROJECT_NAME} ${TG_SRC})

# unnamed engine
include_directories(${UN_INCLUDE})
target_link_libraries(${PROJECT_NAME} un)

# imgui
include_directories(${IMGUI_INCLUDE})

add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD
    COMMAND ${CMAKE_COMMAND} -E copy $<TARGET_FILE:un> .)

# Copy assets to build directory
file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/assets DESTINATION ${CMAKE_CURRENT_BINARY_DIR})
add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD COMMAND
    ${CMAKE_COMMAND} -E tar "cfv" "assets.zip" --format=zip ${CMAKE_CURRENT_BINARY_DIR}/assets)

