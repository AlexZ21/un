#include <un/systems/transformsystem.h>
#include <un/utils/log.h>

namespace un {

TransformSystem::TransformSystem()
{

}

void TransformSystem::update(sf::Time elapsed)
{
    for (const Entity &entity : entities()) {
        TransformComponent *transformComponent = entity.component<TransformComponent>();
        if (transformComponent->m_changed)
            transformComponent->m_changed = false;
    }
}

}
