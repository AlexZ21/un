#include <un/systems/rendersystem.h>
#include <un/components/transformcomponent.h>
#include <un/utils/log.h>

#include <queue>

namespace un {

struct ZIndex
{
    bool operator() (const ZIndex &a, const ZIndex &b) const {
        return a.drawable->zIndex() > b.drawable->zIndex();
    }
    const sf::Transform *transform;
    Drawable *drawable;
};

RenderSystem::RenderSystem()
{

}

void RenderSystem::update(sf::Time)
{

}

void RenderSystem::draw(sf::RenderTarget &target, sf::RenderStates states, sf::Time)
{
    const sf::View &view = target.getView();
    sf::Vector2f viewPos = view.getCenter();
    sf::Vector2f viewSize = view.getSize();
    sf::FloatRect viewportRect(viewPos.x - viewSize.x / 2, viewPos.y - viewSize.y / 2,
                               viewSize.x, viewSize.y);

    std::priority_queue<ZIndex, std::vector<ZIndex>, ZIndex> renderQueue;

    for (const Entity &entity : entities()) {
        RenderComponent *renderComponent = entity.component<RenderComponent>();
        TransformComponent *transformComponent = entity.component<TransformComponent>();
        DrawableGroup *drawableGroup = renderComponent->drawableGroup();
        if (drawableGroup) {

            for (un::Drawable *drawable : *drawableGroup->drawables(renderComponent->state())) {
                ZIndex zindex = {&transformComponent->transform(), drawable};
                renderQueue.push(zindex);
            }

//            sf::FloatRect drawableBounds = drawableGroup->globalBounds(renderComponent->state(),
//                                                                       states.transform * transformComponent->transform());

//            if (drawableBounds.intersects(viewportRect))
//                renderQueue.push(entity);
        }
    }

    while (!renderQueue.empty()) {
        ZIndex zindex = renderQueue.top();
        sf::RenderStates s = states;
        s.transform *= *zindex.transform;
        target.draw(*zindex.drawable, s);
        renderQueue.pop();
    }
}

}
