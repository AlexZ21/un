#include <un/systems/box2dsystem.h>

namespace un {

Box2dSystem::Box2dSystem() :
    m_world(b2Vec2(0, 0))
{

}

b2World *Box2dSystem::world()
{
    return &m_world;
}

void Box2dSystem::update(sf::Time)
{
    m_world.Step(1.f/60.f, 8, 3);

    for (const Entity &entity : entities()) {
        Box2dComponent *box2dComponent = entity.component<Box2dComponent>();
        TransformComponent *transformComponent = entity.component<TransformComponent>();

        b2Body *body = box2dComponent->body();
        if (body) {
            transformComponent->setPosition(box2dComponent->worldCenter());
            transformComponent->setRotation(box2dComponent->rotation());
        }
    }
}

void Box2dSystem::onEntityAdded(const Entity &entity)
{
    Box2dComponent *box2dComponent = entity.component<Box2dComponent>();
    box2dComponent->m_world = &m_world;
}

void Box2dSystem::onEntityRemoved(const Entity &entity)
{
    Box2dComponent *box2dComponent = entity.component<Box2dComponent>();
    if (box2dComponent->m_body)
        m_world.DestroyBody(box2dComponent->m_body);
    box2dComponent->m_world = nullptr;
}

}
