#include <un/systems/scenetreesystem.h>
#include <un/utils/log.h>

namespace un {

void SceneTreeSystem::update(sf::Time)
{
    for (const un::Entity &entity : entities()) {
        TransformComponent *transformComponent = entity.component<TransformComponent>();

        if (transformComponent->isChanged()) {
            SceneTreeComponent *sceneTreeComponent = entity.component<SceneTreeComponent>();

            if (sceneTreeComponent->m_proxyId == -1) {
                onEntityAdded(entity);
                continue;
            }

            RenderComponent *renderComponent = entity.component<RenderComponent>();

            if (!renderComponent->drawableGroup())
                continue;

            sf::FloatRect globalBounds = renderComponent->drawableGroup()->
                    globalBounds(renderComponent->state(), transformComponent->transform());
            sf::Vector2f offset = transformComponent->position() - sceneTreeComponent->m_proxyPosition;
            m_tree.moveProxy(sceneTreeComponent->m_proxyId, globalBounds, offset);
            sceneTreeComponent->m_proxyPosition = transformComponent->position();
        }
    }
}

void SceneTreeSystem::onEntityAdded(const Entity &entity)
{
    TransformComponent *transformComponent = entity.component<TransformComponent>();
    RenderComponent *renderComponent = entity.component<RenderComponent>();
    SceneTreeComponent *sceneTreeComponent = entity.component<SceneTreeComponent>();

    if (!renderComponent->drawableGroup())
        return;

    sf::FloatRect globalBounds = renderComponent->drawableGroup()->
            globalBounds(renderComponent->state(), transformComponent->transform());
    int32_t proxyId = m_tree.createProxy(globalBounds, entity);
    sceneTreeComponent->m_proxyId = proxyId;
}

void SceneTreeSystem::onEntityRemoved(const Entity &entity)
{
    SceneTreeComponent *sceneTreeComponent = entity.component<SceneTreeComponent>();
    m_tree.destroyProxy(sceneTreeComponent->m_proxyId);
    sceneTreeComponent->m_proxyId = -1;
}

}
