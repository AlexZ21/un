#include <un/systems/childrensystem.h>

namespace un {

void ChildrenSystem::update(sf::Time)
{
    for (const Entity &entity : entities()) {
        ChildrenComponent *childrenComponent = entity.component<ChildrenComponent>();
        TransformComponent *transformComponent = entity.component<TransformComponent>();

        // Add new children
        while (!childrenComponent->m_achildren.empty()) {
            Entity childEntity = childrenComponent->m_achildren.front();
            childrenComponent->m_achildren.erase(childrenComponent->m_achildren.begin());
            childrenComponent->m_children.push_back(childEntity);
            TransformComponent *childTransformComponent = childEntity.component<TransformComponent>();
            childTransformComponent->setParentTransform(transformComponent->transform());
        }

        // Erase removed children
        while (!childrenComponent->m_rchildren.empty()) {
            Entity childEntity = childrenComponent->m_achildren.front();
            childrenComponent->m_achildren.erase(childrenComponent->m_achildren.begin());
            std::remove(childrenComponent->m_children.begin(),
                        childrenComponent->m_children.end(), childEntity);
            TransformComponent *childTransformComponent = childEntity.component<TransformComponent>();
            childTransformComponent->unsetParentTransform();
        }

        // Recursively update parent trasform
        if (transformComponent->isChanged())
            updateParentTransform(entity);
    }
}

void ChildrenSystem::updateParentTransform(const Entity &entity)
{
    ChildrenComponent *childrenComponent = entity.component<ChildrenComponent>();
    TransformComponent *transformComponent = entity.component<TransformComponent>();

    for (const Entity &childEntity : childrenComponent->m_children) {
        TransformComponent *childTransformComponent = childEntity.component<TransformComponent>();
        childTransformComponent->setParentTransform(transformComponent->transform());
        updateParentTransform(childEntity);
    }
}

}
