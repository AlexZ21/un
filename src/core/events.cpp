#include <un/core/events.h>
#include <un/utils/log.h>

#include <algorithm>

namespace un {

Event *createSfmlEvent(const sf::Event &sfmlEvent)
{
    switch (sfmlEvent.type) {
    case sf::Event::Closed:
        return new ClosedEvent();
    case sf::Event::Resized:
    {
        ResizedEvent *event = new ResizedEvent();
        event->width = sfmlEvent.size.width;
        event->height = sfmlEvent.size.height;
        return event;
    }
    case sf::Event::LostFocus:
        return new LostFocusEvent();
    case sf::Event::GainedFocus:
        return new GainedFocusEvent();
    case sf::Event::TextEntered:
    {
        TextEnteredEvent *event = new TextEnteredEvent();
        event->unicode = sfmlEvent.text.unicode;
        return event;
    }
    case sf::Event::KeyPressed:
    {
        KeyPressedEvent *event = new KeyPressedEvent();
        event->code = sfmlEvent.key.code;
        event->alt = sfmlEvent.key.alt;
        event->control = sfmlEvent.key.control;
        event->shift = sfmlEvent.key.shift;
        event->system = sfmlEvent.key.system;
        return event;
    }
    case sf::Event::KeyReleased:
    {
        KeyReleasedEvent *event = new KeyReleasedEvent();
        event->code = sfmlEvent.key.code;
        event->alt = sfmlEvent.key.alt;
        event->control = sfmlEvent.key.control;
        event->shift = sfmlEvent.key.shift;
        event->system = sfmlEvent.key.system;
        return event;
    }
    case sf::Event::MouseWheelMoved:
    {
        MouseWheelMovedEvent *event = new MouseWheelMovedEvent();
        event->delta = sfmlEvent.mouseWheel.delta;
        event->x = sfmlEvent.mouseWheel.x;
        event->y = sfmlEvent.mouseWheel.y;
        return event;
    }
    case sf::Event::MouseWheelScrolled:
    {
        MouseWheelScrolledEvent *event = new MouseWheelScrolledEvent();
        event->wheel = sfmlEvent.mouseWheelScroll.wheel;
        event->delta = sfmlEvent.mouseWheelScroll.delta;
        event->x = sfmlEvent.mouseWheelScroll.x;
        event->y = sfmlEvent.mouseWheelScroll.y;
        return event;
    }
    case sf::Event::MouseButtonPressed:
    {
        MouseButtonPressedEvent *event = new MouseButtonPressedEvent();
        event->button = sfmlEvent.mouseButton.button;
        event->x = sfmlEvent.mouseButton.x;
        event->y = sfmlEvent.mouseButton.y;
        return event;
    }
    case sf::Event::MouseButtonReleased:
    {
        MouseButtonReleasedEvent *event = new MouseButtonReleasedEvent();
        event->button = sfmlEvent.mouseButton.button;
        event->x = sfmlEvent.mouseButton.x;
        event->y = sfmlEvent.mouseButton.y;
        return event;
    }
    case sf::Event::MouseMoved:
    {
        MouseMovedEvent *event = new MouseMovedEvent();
        event->x = sfmlEvent.mouseMove.x;
        event->y = sfmlEvent.mouseMove.y;
        return event;
    }
    case sf::Event::MouseEntered:
        return new MouseEnteredEvent();
    case sf::Event::MouseLeft:
        return new MouseLeftEvent();
    case sf::Event::JoystickButtonPressed:
    {
        JoystickButtonPressedEvent *event = new JoystickButtonPressedEvent();
        event->joystickId = sfmlEvent.joystickButton.joystickId;
        event->button = sfmlEvent.joystickButton.button;
        return event;
    }
    case sf::Event::JoystickButtonReleased:
    {
        JoystickButtonReleasedEvent *event = new JoystickButtonReleasedEvent();
        event->joystickId = sfmlEvent.joystickButton.joystickId;
        event->button = sfmlEvent.joystickButton.button;
        return event;
    }
    case sf::Event::JoystickMoved:
    {
        JoystickMovedEvent *event = new JoystickMovedEvent();
        event->joystickId = sfmlEvent.joystickMove.joystickId;
        event->axis = sfmlEvent.joystickMove.axis;
        event->position = sfmlEvent.joystickMove.position;
        return event;
    }
    case sf::Event::JoystickConnected:
    {
        JoystickConnectedEvent *event = new JoystickConnectedEvent();
        event->joystickId = sfmlEvent.joystickConnect.joystickId;
        return event;
    }
    case sf::Event::JoystickDisconnected:
    {
        JoystickDisconnectedEvent *event = new JoystickDisconnectedEvent();
        event->joystickId = sfmlEvent.joystickConnect.joystickId;
        return event;
    }
    case sf::Event::TouchBegan:
    {
        TouchBeganEvent *event = new TouchBeganEvent();
        event->finger = sfmlEvent.touch.finger;
        event->x = sfmlEvent.touch.x;
        event->y = sfmlEvent.touch.y;
        return event;
    }
    case sf::Event::TouchMoved:
    {
        TouchMovedEvent *event = new TouchMovedEvent();
        event->finger = sfmlEvent.touch.finger;
        event->x = sfmlEvent.touch.x;
        event->y = sfmlEvent.touch.y;
        return event;
    }
    case sf::Event::TouchEnded:
    {
        TouchEndedEvent *event = new TouchEndedEvent();
        event->finger = sfmlEvent.touch.finger;
        event->x = sfmlEvent.touch.x;
        event->y = sfmlEvent.touch.y;
        return event;
    }
    case sf::Event::SensorChanged:
    {
        SensorChangedEvent *event = new SensorChangedEvent();
        event->sensorType = sfmlEvent.sensor.type;
        event->x = sfmlEvent.sensor.x;
        event->y = sfmlEvent.sensor.y;
        event->z = sfmlEvent.sensor.z;
        return event;
    }
    }

    return nullptr;
}

Events::Events() : Manager(true)
{

}

void Events::sendEvent(int32_t type)
{
    std::lock_guard<std::mutex> lock(m_eventsmx);
    Event *event = new Event();
    event->type = type;
    m_events.emplace(event);
}

void Events::sendEvent(Event *event)
{
    if (!event)
        return;
    std::lock_guard<std::mutex> lock(m_eventsmx);
    m_events.emplace(event);
}

void Events::setEventListener(int32_t type, EventListener *eventListener)
{
    std::lock_guard<std::mutex> lock(m_eventsmx);
    using const_ref = std::unordered_multimap<int32_t, uuptr<EventListener>>::const_reference;
    auto it = std::find_if(m_eventListeners.begin(), m_eventListeners.end(),
                           [eventListener](const_ref a) { return a.second.get() == eventListener; });
    if (it != m_eventListeners.end()) {
        LERR("Event listener already setted");
        return;
    }
    m_eventListeners.emplace(type, uuptr<EventListener>(eventListener));
}

void Events::unsetEventListener(EventListener *eventListener)
{
    std::lock_guard<std::mutex> lock(m_eventsmx);
    using const_ref = std::unordered_multimap<int32_t, uuptr<EventListener>>::const_reference;
    auto it = std::find_if(m_eventListeners.begin(), m_eventListeners.end(),
                           [eventListener](const_ref a) { return a.second.get() == eventListener; });
    if (it == m_eventListeners.end()) {
        LERR("Event listener not set");
        return;
    }
    it->second.release();
    m_eventListeners.erase(it);
}

void Events::update(sf::Time)
{
    size_t eventCount = m_events.size();
    for (int i = 0; i < eventCount; ++i) {
        m_eventsmx.lock();
        uuptr<Event> event = std::move(m_events.front());
        m_events.pop();
        m_eventsmx.unlock();
        auto listeners = m_eventListeners.equal_range(event->type);
        for (auto i = listeners.first; i != listeners.second; ++i) {
            if (i->second->callback)
                i->second->callback(event.get());
        }
    }
}

}
