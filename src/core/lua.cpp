#include <un/core/lua.h>
#include <un/core/application.h>
#include <un/core/assets.h>
#include <un/utils/ptr.h>
#include <un/utils/log.h>

#include <un/core/lua/core_bind.h>
#include <un/graphics/lua/graphics_bind.h>

namespace sol {
template <typename T>
struct unique_usertype_traits<::un::utils::Ptr<T>> {
    typedef T type;
    typedef ::un::utils::Ptr<T> actual_type;
    static const bool value = true;

    static bool is_null(const actual_type& value) {
        return !value.isValid();
    }

    static type *get (const actual_type& p) {
        return p.get();
    }
};
}

namespace un {

Lua::Lua() : Manager()
{
    m_state.open_libraries();
    bind();
}

void Lua::script(const std::string &script)
{
    m_state.script(script);
}

void Lua::file(const std::string &filePath)
{
    std::vector<char> data = Application::manager<Assets>()->readFile(filePath);
    if (data.empty())
        return;
    m_state.script(std::string(data.data(), data.size()));
}

void Lua::bind()
{
    sol::table unTable = m_state.create_table("un");
    unTable["require"] = [this](const std::string &filePath){
        std::vector<char> data = Application::manager<Assets>()->readFile(filePath);
        return m_state.script(std::string(data.data(), data.size()));
    };
    // Bind core classes
    luaCoreBind(&m_state);
    // Bind graphics classes
    luaGraphicsBind(&m_state);
}

}
