#include <un/core/application.h>
#include <un/core/assets.h>
#include <un/core/scene.h>
#include <un/core/actionkeybinder.h>
#include <un/core/lua.h>
#include <un/graphics/textures.h>
#include <un/utils/ptr.h>

#include <SFML/Window/Event.hpp>

#include <functional>
#include <algorithm>

namespace un {

Application *Application::m_instance = nullptr;

Application *Application::instance()
{
    return m_instance;
}

Application *Application::create()
{
    if (m_instance) {
        return nullptr;
    } else {
        LINFO("Create application");
        static Application app;
        return m_instance;
    }
}

int Application::exec()
{
    if (!m_instance) {
        LERR("Application not created");
        return 1;
    }
    m_instance->mainLoop();
    return 0;
}

void Application::exit()
{
    m_instance->m_running.store(false);
}

void Application::showWindow(uint32_t width, uint32_t height, std::string title, bool fullscreen)
{
    if (!m_instance)
        return;

    m_instance->m_width = width;
    m_instance->m_height = height;
    m_instance->m_title = title;
    m_instance->m_fullscreen = fullscreen;

    m_instance->m_window = std::make_unique<sf::RenderWindow>(sf::VideoMode(m_instance->m_width,
                                                                            m_instance->m_height),
                                                              m_instance->m_title,
                                                              m_instance->m_fullscreen ? sf::Style::Fullscreen :
                                                                                         sf::Style::Default);
    m_instance->m_window->setFramerateLimit(60);
}

void Application::closeWindow()
{
    if (!m_instance)
        return;
    m_instance->m_window.reset();
}

sf::RenderWindow *Application::window()
{
    if (!m_instance)
        return nullptr;
    return m_instance->m_window.get();
}

void Application::unsetAllManagers()
{
    if (!m_instance)
        return;
    m_instance->m_updatableManages.clear();
    m_instance->m_managers.clear();
}

void Application::sendEvent(int32_t type)
{
    m_instance->manager<Events>()->sendEvent(type);
}

void Application::sendEvent(Event *event)
{
    m_instance->manager<Events>()->sendEvent(event);
}

void Application::setEventListener(int32_t type, EventListener *eventListener)
{
    m_instance->manager<Events>()->setEventListener(type, eventListener);
}

void Application::unsetEventListener(EventListener *eventListener)
{
    m_instance->manager<Events>()->unsetEventListener(eventListener);
}

Scene *Application::scene()
{
    if (!m_instance)
        return nullptr;
    return m_instance->m_scene.get();
}

void Application::setScene(Scene *scene)
{
    if (!m_instance)
        return;
    m_instance->m_scene.release();
    m_instance->m_scene.reset(scene);
}

ActionKeyBinder *Application::actionKeyBinder()
{
    if (!m_instance)
        return nullptr;
    return m_instance->m_actionKeyBinder.get();
}

void Application::setActionKeyBinder(ActionKeyBinder *actionKeyBinder)
{
    if (!m_instance)
        return;
    m_instance->m_actionKeyBinder.release();
    m_instance->m_actionKeyBinder.reset(actionKeyBinder);
}

Application::Application() :
    m_tickTime(sf::milliseconds(16)),
    m_running(false)
{
    m_instance = this;
    utils::PtrStorage::instance();

    // Setup managers
    setManager<Events>();
    setManager<Assets>();
    setManager<Lua>();
    setManager<Textures>();

    // Listen close event
    setEventListener(Event::Closed, new EventListener([](Event *){
                         Application::exit();
                     }));
}

Application::~Application()
{
    m_scene.reset();
    unsetAllManagers();
}

void Application::mainLoop()
{
    m_running.store(true);
    sf::Clock loopClock;
    sf::Time elapsed = sf::milliseconds(0);
    sf::Time lag = sf::milliseconds(0);
    while (m_running.load()) {
        lag += (elapsed = loopClock.restart());

        // Process window events end clear screen
        if (m_window) {
            sf::Event event;
            while (m_window->pollEvent(event)) {
                // Action key binder
                if (m_actionKeyBinder)
                    m_actionKeyBinder->update(event);
                sendEvent(createSfmlEvent(event));
            }
            m_window->clear();
        }

        // Update tick
        while (lag >= m_tickTime) {
            tick(elapsed);
            lag -= m_tickTime;
        }

        // Draw scene
        if (m_scene)
            m_scene->draw(*m_window, sf::RenderStates(), elapsed);

        // Display window
        if (m_window)
            m_window->display();
    }
}

void Application::tick(sf::Time elapsed)
{
    // Update managers
    for (Manager *manger : m_updatableManages)
        manger->update(elapsed);

    // Update scene
    if (m_scene)
        m_scene->update(elapsed);
}

}

