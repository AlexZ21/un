#include <un/core/system.h>

#include <algorithm>

namespace un {

BaseSystem::BaseSystem(const Filter &filter) :
    m_world(nullptr),
    m_filter(filter)
{

}

World *BaseSystem::world() const
{
    return m_world;
}

const Filter &BaseSystem::filter() const
{
    return m_filter;
}

const std::vector<Entity> &BaseSystem::entities() const
{
    return m_entities;
}

void BaseSystem::add(const Entity &entity)
{
    m_entities.push_back(entity);
    onEntityAdded(entity);
}

void BaseSystem::remove(const Entity &entity)
{
    m_entities.erase(std::remove(m_entities.begin(), m_entities.end(), entity), m_entities.end());
    onEntityRemoved(entity);
}

void BaseSystem::setWorld(World *world)
{
    m_world = world;
    initialize();
}

}
