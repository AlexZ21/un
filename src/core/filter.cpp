#include <un/core/filter.h>

namespace un {

Filter::Filter(ComponentMask requires, ComponentMask excludes) :
    m_requires(requires), m_excludes(excludes)
{

}

bool Filter::doesPassFilter(const ComponentMask &typeList) const
{
    for(size_t i = 0; i < m_requires.size(); ++i) {
        if(m_requires[i] == true && typeList[i] == false)
            return false;
    }
    if((m_excludes & typeList).any())
        return false;
    return true;
}

}
