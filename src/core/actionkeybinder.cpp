#include <un/core/actionkeybinder.h>
#include <un/utils/log.h>

#include <bitset>
#include <algorithm>

namespace un {

template<typename ContainerT, typename PredicateT>
void erase_if(ContainerT &items, const PredicateT &predicate) {
    for (auto it = items.begin(); it != items.end(); ) {
        if (predicate(*it)) it = items.erase(it);
        else ++it;
    }
}

std::bitset<sf::Event::Count> eventsFilter() {
    std::bitset<sf::Event::Count> filter;
    filter.set(sf::Event::KeyPressed);
    filter.set(sf::Event::KeyReleased);
    filter.set(sf::Event::MouseButtonPressed);
    filter.set(sf::Event::MouseButtonReleased);
    filter.set(sf::Event::JoystickButtonPressed);
    filter.set(sf::Event::JoystickButtonReleased);
    return filter;
}

bool applyEventsFilter(sf::Event::EventType type) {
    static std::bitset<sf::Event::Count> ef = eventsFilter();
    return ef.test(type);
}

ActionKeyBinder::ActionKeyBinder()
{

}

void ActionKeyBinder::addAction(const std::string &name, const std::function<void ()> &callback)
{
    if (!callback) {
        LERR("Action callback is null");
        return;
    }
    if (m_actions.find(name) != m_actions.end()) {
        LERR("Action <", name, "> already exists");
        return;
    }
    m_actions.emplace(name, callback);
}

void ActionKeyBinder::removeAction(const std::string &name)
{
    auto it = m_actions.find(name);
    if (it == m_actions.end())
        return;
    m_actions.erase(it);
}

void ActionKeyBinder::clearAllActions()
{
    m_actions.clear();
    m_shortcuts.clear();
}

void ActionKeyBinder::bind(const std::string &actionName, ActionKeyBinder::InputType inputType,
                           sf::Keyboard::Key code, bool alt, bool control, bool shift, bool system)
{
//    if (m_actions.find(actionName) == m_actions.end()) {
//        LERR("Action <", actionName, "> not found");
//        return;
//    }

    size_t hash = 0;
    hash = std::hash<InputDeviceType>()(Keyboard);
    hash ^= code << 1;
    hash ^= alt << 2;
    hash ^= control << 3;
    hash ^= shift << 4;
    hash ^= system << 5;

    ShortcutState st;
    st.actionName = actionName;
    st.activated = false;
    st.inputType = inputType;

    m_shortcuts.emplace(hash, st);
}

void ActionKeyBinder::bind(const std::string &actionName, ActionKeyBinder::InputType inputType,
                           sf::Mouse::Button button)
{
//    if (m_actions.find(actionName) == m_actions.end()) {
//        LERR("Action <", actionName, "> not found");
//        return;
//    }

    size_t hash = 0;
    hash = std::hash<InputDeviceType>()(Mouse);
    hash ^= button << 1;

    ShortcutState st;
    st.actionName = actionName;
    st.activated = false;
    st.inputType = inputType;

    m_shortcuts.emplace(hash, st);
}

void ActionKeyBinder::bind(const std::string &actionName, ActionKeyBinder::InputType inputType,
                           uint32_t joystickId, uint32_t button)
{
//    if (m_actions.find(actionName) == m_actions.end()) {
//        LERR("Action <", actionName, "> not found");
//        return;
//    }

    size_t hash = 0;
    hash = std::hash<InputDeviceType>()(Mouse);
    hash ^= joystickId << 1;
    hash ^= button << 1;

    ShortcutState st;
    st.actionName = actionName;
    st.activated = false;
    st.inputType = inputType;

    m_shortcuts.emplace(hash, st);
}

void ActionKeyBinder::unbind(const std::string &actionName)
{
    erase_if(m_shortcuts, [actionName](const auto &it){ return it.second.actionName == actionName; });
}

void ActionKeyBinder::unbindAll()
{
    m_shortcuts.clear();
}

void ActionKeyBinder::update(const sf::Event &event)
{
    if (!applyEventsFilter(event.type))
        return;

    auto range = m_shortcuts.equal_range(sfmlEventHash(event));

    for (auto it = range.first; it != range.second; ++it) {
        ShortcutState &st = it->second;

        if (event.type == sf::Event::KeyPressed ||
                event.type == sf::Event::MouseButtonPressed ||
                event.type == sf::Event::JoystickButtonPressed) {
            if (st.inputType == PressOnce && !st.activated) {
                m_actions[st.actionName]();
                st.activated = true;
            } else if (st.inputType == Hold) {
                m_actions[st.actionName]();
            } else if (st.inputType == ReleaseOnce && st.activated) {
                st.activated = false;
            }
        } else {
            if (st.inputType == ReleaseOnce && !st.activated) {
                m_actions[st.actionName]();
                st.activated = true;
            } else if (st.inputType == PressOnce && st.activated) {
                st.activated = false;
            }
        }

    }
}

size_t ActionKeyBinder::sfmlEventHash(const sf::Event &event)
{
    size_t hash = 0;
    if (event.type == sf::Event::KeyPressed ||
            event.type == sf::Event::KeyReleased) {
        hash = std::hash<InputDeviceType>()(Keyboard);
        hash ^= event.key.code << 1;
        hash ^= event.key.alt << 2;
        hash ^= event.key.control << 3;
        hash ^= event.key.shift << 4;
        hash ^= event.key.system << 5;

    } else if (event.type == sf::Event::MouseButtonPressed ||
               event.type == sf::Event::MouseButtonReleased) {
        hash = std::hash<InputDeviceType>()(Mouse);
        hash ^= event.mouseButton.button << 1;

    } else if (event.type == sf::Event::JoystickButtonPressed ||
               event.type == sf::Event::JoystickButtonReleased) {
        hash = std::hash<InputDeviceType>()(Joystick);
        hash ^= event.joystickButton.joystickId << 1;
        hash ^= event.joystickButton.button << 2;

    }

    return hash;
}

}
