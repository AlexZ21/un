#include <un/core/entity.h>
#include <un/core/world.h>
#include <un/utils/log.h>

namespace un {

Entity::Entity()
{

}

Entity::Entity(World *world, Entity::Id id) :
    m_world(world),
    m_id(id)
{
}

Entity::~Entity()
{

}

World *Entity::world() const
{
    return m_world;
}

bool Entity::isValid() const
{
    if (!m_world)
        return false;
    return m_world->isValid(*this);
}

bool Entity::isEnabled() const
{
    if (!m_world)
        return false;
    return m_world->isEnabled(*this);
}

void Entity::enable()
{
    if (!m_world)
        return;
    return m_world->enableEntity(*this);
}

void Entity::disable()
{
    if (!m_world)
        return;
    return m_world->disableEntity(*this);
}

void Entity::destroy()
{
    if (!m_world)
        return;
    return m_world->destroyEntity(*this);
}

void Entity::removeAllComponents()
{
    if (!m_world)
        return;
    m_world->m_entityAttributes.componentStorage.removeAllComponents(*this);
}

std::vector<Component *> Entity::components() const
{
    if (!m_world)
        return std::vector<Component *>();
    return m_world->m_entityAttributes.componentStorage.components(*this);
}

ComponentMask Entity::componentTypeList() const
{
    if (!m_world)
        return ComponentMask();
    return m_world->m_entityAttributes.componentStorage.componentTypeList(*this);
}

bool Entity::operator==(const Entity &entity) const
{
    return m_id == entity.m_id && entity.m_world == m_world;
}

void Entity::addComponent(Component *component, utils::TypeId componentTypeId)
{
    if (!m_world)
        return;
    m_world->m_entityAttributes.componentStorage.addComponent(*this, component, componentTypeId);
}

void Entity::removeComponent(utils::TypeId componentTypeId)
{
    if (!m_world)
        return;
    m_world->m_entityAttributes.componentStorage.removeComponent(*this, componentTypeId);
}

Component *Entity::component(utils::TypeId componentTypeId) const
{
    if (!m_world)
        return nullptr;
    return m_world->m_entityAttributes.componentStorage.component(*this, componentTypeId);
}

bool Entity::hasComponent(utils::TypeId componentTypeId) const
{
    if (!m_world)
        return false;
    return m_world->m_entityAttributes.componentStorage.hasComponent(*this, componentTypeId);
}

EntityIdPool::EntityIdPool(size_t poolSize) :
    m_poolSize(poolSize),
    m_nextid(0),
    m_counts(poolSize)
{

}

Entity::Id EntityIdPool::create()
{
    Entity::Id id;

    if(!m_freeids.empty()) {
        id = m_freeids.front();
        m_freeids.erase(m_freeids.begin());
    } else {
        id.index = m_nextid++;
        id.counter = 1;
        m_counts[id.index] = id.counter;
    }

    return id;
}

void EntityIdPool::remove(Entity::Id id)
{
    int32_t &counter = m_counts[id.index];
    ++counter;
    Entity::Id fid = {id.index, counter};
    m_freeids.push_back(fid);
}

Entity::Id EntityIdPool::get(size_t index) const
{
    return index < m_counts.size() ? Entity::Id{(int32_t)index, m_counts[index]} : Entity::Id{(int32_t)index, 0};
}

bool EntityIdPool::isValid(Entity::Id id) const
{
    return id.index >= m_counts.size() ? false : ( (id.counter == m_counts[id.index]) && (id.counter > 0));
}

size_t EntityIdPool::size() const
{
    return m_counts.size();
}

void EntityIdPool::resize(std::size_t size)
{
    m_counts.resize(size);
}

void EntityIdPool::clear()
{
    m_counts.clear();
    m_freeids.clear();
    m_nextid = 0;
}

}
