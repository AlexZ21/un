#include <un/core/scene.h>
#include <un/core/scenesystem.h>
#include <un/components/scenecomponent.h>
#include <un/components/transformcomponent.h>
#include <un/components/rendercomponent.h>
#include <un/components/childrencomponent.h>
#include <un/components/box2dcomponent.h>
#include <un/components/scenetreecomponent.h>
#include <un/core/application.h>
#include <un/utils/log.h>

#include <algorithm>

namespace un {

Scene::Scene() :
    m_world(std::make_unique<World>(1024)),
    m_transformSystem(new TransformSystem()),
    m_childrenSystem(new ChildrenSystem()),
    m_box2dSystem(new Box2dSystem()),
    m_sceneTreeSystem(new SceneTreeSystem()),
    m_renderSystem(new RenderSystem())
{
    m_world->addSystem(m_transformSystem.get());
    m_world->addSystem(m_childrenSystem.get());
    m_world->addSystem(m_box2dSystem.get());
    m_world->addSystem(m_sceneTreeSystem.get());
    m_world->addSystem(m_renderSystem.get());
}

Scene::~Scene()
{
    m_world.reset();
}

Entity Scene::createEntity(bool setDefaultComponents)
{
    Entity entity = m_world->createEntity();
    if (setDefaultComponents) {
        entity.addComponent<SceneComponent>(this);
        entity.addComponent<TransformComponent>();
        entity.addComponent<ChildrenComponent>();
        entity.addComponent<Box2dComponent>();
        entity.addComponent<SceneTreeComponent>();
        entity.addComponent<RenderComponent>();
    }
    entity.enable();
    return entity;
}

void Scene::destroyEntity(Entity &entity)
{
    m_world->destroyEntity(entity);
}

void Scene::addSystem(BaseSceneSystem *system)
{
    if (m_world->addSystem(system))
        m_systems.emplace_back(system);
}

void Scene::removeSystem(BaseSceneSystem *system)
{
    if (m_world->removeSystem(system)) {
        auto it = std::find_if(m_systems.begin(), m_systems.end(), [system](const auto &ptr){ return ptr.get() == system; });
        it->release();
        m_systems.erase(it);
    }
}

void Scene::update(sf::Time elapsed)
{
    // Refresh ecs
    m_world->refresh();

    // Update user systems
    for (auto &system : m_systems)
        system->update(elapsed);

    // Update systems
    m_box2dSystem->update(elapsed);
    m_childrenSystem->update(elapsed);
    m_sceneTreeSystem->update(elapsed);
    m_transformSystem->update(elapsed);
}

void Scene::draw(sf::RenderTarget &target, sf::RenderStates states, sf::Time elapsed)
{
    if (m_renderSystem)
        m_renderSystem->draw(target, states, elapsed);
}

}
