#include <un/core/componentstorage.h>
#include <un/utils/log.h>

namespace un {

ComponentStorage::ComponentStorage(size_t size) :
    m_componentEntries(size)
{

}

void ComponentStorage::addComponent(Entity &entity, Component *component, utils::TypeId componentTypeId)
{
    if (!entity.isValid())
        return;

    int32_t index = entity.id().index;
    EntityComponents &componentDataForEntity = m_componentEntries[index];
    componentDataForEntity.components[componentTypeId].reset(component);
    componentDataForEntity.componentTypeList[componentTypeId] = true;
}

void ComponentStorage::removeComponent(Entity &entity, utils::TypeId componentTypeId)
{
    if (!entity.isValid())
        return;

    int32_t index = entity.id().index;
    EntityComponents &componentDataForEntity = m_componentEntries[index];
    componentDataForEntity.components[componentTypeId].reset();
    componentDataForEntity.componentTypeList[componentTypeId] = false;
}

void ComponentStorage::removeAllComponents(Entity &entity)
{
    if (!entity.isValid())
        return;

    int32_t index = entity.id().index;
    EntityComponents &componentDataForEntity = m_componentEntries[index];

    for(auto &c : componentDataForEntity.components)
        c.reset();
    componentDataForEntity.componentTypeList.reset();
}

Component *ComponentStorage::component(const Entity &entity, utils::TypeId componentTypeId) const
{
    if (!entity.isValid() || !hasComponent(entity, componentTypeId))
        return nullptr;
    return componentsImpl(entity)[componentTypeId].get();
}

ComponentMask ComponentStorage::componentTypeList(const Entity &entity) const
{
    if (!entity.isValid())
        return ComponentMask();
    return m_componentEntries[entity.id().index].componentTypeList;
}

std::vector<Component *> ComponentStorage::components(const Entity &entity) const
{
    if (!entity.isValid())
        return std::vector<Component *>();

    const std::array<uuptr<Component>, MAX_AMOUNT_OF_COMPONENTS> &componentsToConvert = componentsImpl(entity);
    std::vector<Component *> temp;
    temp.reserve(componentsToConvert.size());

    for(auto &i : componentsToConvert)
        temp.emplace_back(i.get());

    return temp;
}

bool ComponentStorage::hasComponent(const Entity &entity, utils::TypeId componentTypeId) const
{
    if (!entity.isValid())
        return false;
    const std::array<uuptr<Component>, MAX_AMOUNT_OF_COMPONENTS> &components = componentsImpl(entity);
    return (components.size() > componentTypeId) && (components[componentTypeId] != nullptr);
}

void ComponentStorage::resize(size_t size)
{
    m_componentEntries.resize(size);
}

void ComponentStorage::clear()
{
    m_componentEntries.clear();
}

const std::array<uuptr<Component>, MAX_AMOUNT_OF_COMPONENTS> &ComponentStorage::componentsImpl(const Entity &entity) const
{
    return m_componentEntries[entity.id().index].components;
}

std::array<uuptr<Component>, MAX_AMOUNT_OF_COMPONENTS> &ComponentStorage::componentsImpl(const Entity &entity)
{
    return m_componentEntries[entity.id().index].components;
}

}
