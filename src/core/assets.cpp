#include <un/core/assets.h>
#include <un/utils/log.h>

#include <fstream>
#include <algorithm>
#include <iterator>

namespace un {

AssetsLocalFileLoader::AssetsLocalFileLoader(const std::string &path) :
    m_path(path)
{

}

std::vector<char> AssetsLocalFileLoader::readFile(const std::string &path)
{
    std::vector<char> fileData;

    std::ifstream ifs(m_path + "/" + path, std::ios::in | std::ifstream::binary);
    if (!ifs.is_open()) {
        LERR("Assets: file not found");
        return fileData;
    }

    std::istreambuf_iterator<char> iter(ifs);
    std::istreambuf_iterator<char> end{};
    std::copy(std::istreambuf_iterator<char>(ifs), std::istreambuf_iterator<char>(),
              std::back_inserter(fileData));

    return fileData;
}

void Assets::addSource(const std::string &name, Assets::LoaderType loaderType, const std::string &path)
{
    if (name.empty() || path.empty())
        return;

    AssetsFileLoader *loader = nullptr;

    switch (loaderType) {
    case Local:
        loader = new AssetsLocalFileLoader(path);
        break;
    default:
        break;
    }

    m_sources.emplace(name, loader->toPtr());
}

void Assets::removeSource(const std::string &name)
{
    auto it = m_sources.find(name);
    if (it == m_sources.end())
        return;
    m_sources.erase(it);
}

std::vector<char> Assets::readFile(const std::string &path)
{
    std::vector<char> fileData;

    size_t pos = path.find(":");
    if (pos == std::string::npos) {
        LERR("Assets: wrong resource path");
        return fileData;
    }

    std::string sourceName = path.substr(0, pos);
    std::string filePath = path.substr(pos + 1);

    if (sourceName.empty() || filePath.empty()) {
        LERR("Assets: wrong resource path");
        return fileData;
    }

    auto it = m_sources.find(sourceName);
    if (it == m_sources.end()) {
        LERR("Assets: source not found");
        return fileData;
    }

    fileData = std::move(it->second->readFile(filePath));
    return fileData;
}

std::string Assets::sourceFromPath(const std::string &path)
{
    size_t pos = path.find(":");
    if (pos == std::string::npos) {
        LERR("Assets: wrong resource path");
        return std::string();
    }
    return path.substr(0, pos);
}

}
