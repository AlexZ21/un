#include <un/core/world.h>
#include <un/utils/utils.h>
#include <un/utils/log.h>

namespace un {

World::World(size_t entityPoolSize) :
    m_entityIdPool(entityPoolSize),
    m_entityAttributes(entityPoolSize)
{
}

bool World::addSystem(BaseSystem *system)
{
    if (!system)
        return false;
    if (m_systems.find(system->typeId()) != m_systems.end()) {
        LERR("System already added to world");
        return false;
    }
    m_systems[system->typeId()].reset(system);
    system->m_world = this;
    system->initialize();
    return true;
}

bool World::removeSystem(BaseSystem *system)
{
    if (!system)
        return false;
    if (m_systems.find(system->typeId()) == m_systems.end()) {
        LERR("System not added to world");
        return false;
    }
    m_systems.erase(system->typeId());
    return true;
}

void World::removeAllSystems()
{
    m_systems.clear();
}

bool World::systemExists(BaseSystem *system)
{
    if (!system)
        return false;
    return m_systems.find(system->typeId()) != m_systems.end();
}

Entity World::createEntity()
{
    checkForResize(1);
    m_entityCache.alive.emplace_back(this, m_entityIdPool.create());
    return m_entityCache.alive.back();
}

std::vector<Entity> World::createEntities(size_t amount)
{
    std::vector<Entity> temp;
    temp.reserve(amount);

    checkForResize(amount);

    for(size_t i = 0; i < amount; ++i) {
        Entity e{this, m_entityIdPool.create()};
        m_entityCache.alive.push_back(e);
        temp.push_back(e);
    }

    return temp;
}

void World::destroyEntity(const Entity &entity)
{
    disableEntity(entity);
    m_entityCache.killed.push_back(entity);
}

void World::destroyEntities(const std::vector<Entity> &entities)
{
    for(const Entity &entity : entities)
        destroyEntity(entity);
}

void World::enableEntity(const Entity &entity)
{
    if (!isValid(entity))
        return;
    m_entityCache.enabled.push_back(entity);
    refresh();
}

void World::disableEntity(const Entity &entity)
{
    if (!isValid(entity))
        return;
    m_entityCache.disabled.push_back(entity);
    refresh();
}

bool World::isEnabled(const Entity &entity) const
{
    return isValid(entity) ? m_entityAttributes.attributes[entity.id().index].activated : false;
    }

    bool World::isValid(const Entity &entity) const
    {
    return m_entityIdPool.isValid(entity.id());
}

Entity World::entity(size_t index)
{
    return Entity{this, m_entityIdPool.get(index)};
}

const std::vector<Entity> &World::entities() const
{
    return m_entityCache.alive;
}

void World::checkForResize(size_t amountOfEntitiesToBeAllocated)
{
    auto newSize = entities().size() + amountOfEntitiesToBeAllocated;
    if(newSize > m_entityIdPool.size())
        resize(newSize);
}

void World::resize(size_t amount)
{
    m_entityIdPool.resize(amount);
    m_entityAttributes.resize(amount);
}

void World::refresh()
{
    for(Entity &entity : m_entityCache.enabled) {
        EntityAttributes::Attribute &attribute = m_entityAttributes.attributes[entity.id().index];
        attribute.activated = true;

        for(auto &i : m_systems) {
            auto systemIndex = i.first;
            if(i.second->filter().doesPassFilter(m_entityAttributes.componentStorage.componentTypeList(entity))) {
                if(attribute.systems.size() <= systemIndex || !attribute.systems[systemIndex]) {
                    i.second->add(entity);
                    utils::ensureCapacity(attribute.systems, systemIndex);
                    attribute.systems[systemIndex] = true;
                }
            } else if(attribute.systems.size() > systemIndex && attribute.systems[systemIndex]) {
                i.second->remove(entity);
                attribute.systems[systemIndex] = false;
            }
        }
    }

    for(Entity &entity : m_entityCache.disabled) {
        EntityAttributes::Attribute &attribute = m_entityAttributes.attributes[entity.id().index];
        attribute.activated = false;

        for(auto& i : m_systems) {
            auto systemIndex = i.first;
            if(attribute.systems.size() <= systemIndex)
                continue;

            if(attribute.systems[systemIndex]) {
                i.second->remove(entity);
                attribute.systems[systemIndex] = false;
            }
        }
    }

    for(Entity &entity : m_entityCache.killed) {
        m_entityCache.alive.erase(std::remove(m_entityCache.alive.begin(),
                                              m_entityCache.alive.end(),
                                              entity), m_entityCache.alive.end());

        m_entityAttributes.componentStorage.removeAllComponents(entity);
        m_entityIdPool.remove(entity.id());
    }

    m_entityCache.clearTemp();
}

void World::clear()
{
    removeAllSystems();
    m_entityAttributes.clear();
    m_entityCache.clear();
    m_entityIdPool.clear();
}

}
