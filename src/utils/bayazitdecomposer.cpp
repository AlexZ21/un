#include <un/utils/bayazitdecomposer.h>
#include <un/utils/log.h>

#include <limits>
#include <cmath>

#define MAX_VERTICES 8

namespace un {
namespace utils {

BayazitDecomposer::BayazitDecomposer()
{

}

std::vector<std::vector<sf::Vector2f>> BayazitDecomposer::decompose(const std::vector<sf::Vector2f> &vertices)
{
    std::vector<std::vector<sf::Vector2f>> polygons;

    sf::Vector2f lowerInt;
    sf::Vector2f upperInt;

    int lowerIndex = 0;
    int upperIndex = 0;

    std::vector<sf::Vector2f> lowerPoly;
    std::vector<sf::Vector2f> upperPoly;

    for (int i = 0; i < vertices.size(); ++i) {
        if (reflex(i, vertices)) {
            float upperDist  = 0.f;
            float lowerDist = upperDist = std::numeric_limits<float>().max();

            for (int j = 0; j < vertices.size(); ++j) {
                float d = 0.f;
                sf::Vector2f p;

                if (left(at(i - 1, vertices), at(i, vertices), at(j, vertices)) &&
                        rightOn(at(i - 1, vertices), at(i, vertices), at(j - 1, vertices))) {

                    p = lineIntersect(at(i - 1, vertices), at(i, vertices), at(j, vertices), at(j - 1, vertices));

                    if (right(at(i + 1, vertices), at(i, vertices), p)) {
                        d = squareDist(at(i, vertices), p);
                        if (d < lowerDist) {
                            lowerDist = d;
                            lowerInt = p;
                            lowerIndex = j;
                        }
                    }
                }

                if (left(at(i + 1, vertices), at(i, vertices), at(j + 1, vertices)) &&
                        rightOn(at(i + 1, vertices), at(i, vertices), at(j, vertices))) {

                    p = lineIntersect(at(i + 1, vertices), at(i, vertices), at(j, vertices), at(j + 1, vertices));

                    if (left(at(i - 1, vertices), at(i, vertices), p)) {
                        d = squareDist(at(i, vertices), p);
                        if (d < upperDist) {
                            upperDist = d;
                            upperIndex = j;
                            upperInt = p;
                        }
                    }
                }
            }

            if (lowerIndex == (upperIndex + 1) % (int32_t)vertices.size()) {
                sf::Vector2f p = ((lowerInt + upperInt) / 2.0f);
                copy(i, upperIndex, vertices, lowerPoly);
                lowerPoly.push_back(p);
                copy(lowerIndex, i, vertices, upperPoly);
                upperPoly.push_back(p);
            } else {
                double highestScore = 0;
                double bestIndex = lowerIndex;
                while (upperIndex < lowerIndex)
                    upperIndex += (int32_t)vertices.size();

                for (int j = lowerIndex; j <= upperIndex; ++j) {
                    if (canSee(i, j, vertices)) {
                        double score = 1.0f / (squareDist(at(i, vertices), at(j, vertices)) + 1);

                        if (reflex(j, vertices)) {
                            if (rightOn(at(j - 1, vertices), at(j, vertices), at(i, vertices)) &&
                                    leftOn(at(j + 1, vertices), at(j, vertices), at(i, vertices)))
                                score += 3;
                            else
                                score += 2;
                        } else {
                            score += 1;
                        }

                        if (score > highestScore) {
                            bestIndex = j;
                            highestScore = score;
                        }
                    }
                }

                copy(i, (int32_t)bestIndex, vertices, lowerPoly);
                copy((int32_t)bestIndex, i, vertices, upperPoly);

            }

            auto lower = decompose(lowerPoly);
            for (auto p : lower)
                polygons.push_back(p);

            auto upper = decompose(upperPoly);
            for (auto p : upper)
                polygons.push_back(p);

            return polygons;
        }
    }

    if (vertices.size() > MAX_VERTICES) {
        copy(0, (int32_t)vertices.size() / 2, vertices, lowerPoly);
        copy((int32_t)vertices.size() / 2, 0, vertices, upperPoly);

        auto lower = decompose(lowerPoly);
        for (auto p : lower)
            polygons.push_back(p);

        auto upper = decompose(upperPoly);
        for (auto p : upper)
            polygons.push_back(p);

    } else {
        polygons.push_back(vertices);
    }

    return polygons;
}

void BayazitDecomposer::copy(int32_t i, int32_t j, const std::vector<sf::Vector2f> &vertices,
                             std::vector<sf::Vector2f> &to)
{
    to.clear();
    while (j < i)
        j += (int32_t)vertices.size();
    for (; i <= j; ++i)
        to.push_back(at(i, vertices));
}

sf::Vector2f BayazitDecomposer::at(int32_t i, const std::vector<sf::Vector2f> &vertices)
{
    size_t s = vertices.size();
    return vertices[i < 0 ? s - 1 - ((-i - 1) % s) : i % s];
}

bool BayazitDecomposer::reflex(int32_t i, const std::vector<sf::Vector2f> &vertices)
{
    return right(i, vertices);
}

bool BayazitDecomposer::left(const sf::Vector2f &a, const sf::Vector2f &b, const sf::Vector2f &c)
{
    return area(a, b, c) > 0;
}

bool BayazitDecomposer::leftOn(const sf::Vector2f &a, const sf::Vector2f &b, const sf::Vector2f &c)
{
    return area(a, b, c) >= 0;
}

bool BayazitDecomposer::right(int32_t i, const std::vector<sf::Vector2f> &vertices)
{
    return right(at(i - 1, vertices), at(i, vertices), at(i + 1, vertices));
}

bool BayazitDecomposer::right(const sf::Vector2f &a, const sf::Vector2f &b, const sf::Vector2f &c)
{
    return area(a, b, c) < 0;
}

bool BayazitDecomposer::rightOn(const sf::Vector2f &a, const sf::Vector2f &b, const sf::Vector2f &c)
{
    return area(a, b, c) <= 0;
}

float BayazitDecomposer::area(const sf::Vector2f &a, const sf::Vector2f &b, const sf::Vector2f &c)
{
    return (((b.x - a.x)*(c.y - a.y)) - ((c.x - a.x)*(b.y - a.y)));
}

sf::Vector2f BayazitDecomposer::lineIntersect(const sf::Vector2f &p1, const sf::Vector2f &p2,
                                              const sf::Vector2f &q1, const sf::Vector2f &q2)
{
    sf::Vector2f i;
    float a1 = p2.y - p1.y;
    float b1 = p1.x - p2.x;
    float c1 = a1 * p1.x + b1 * p1.y;
    float a2 = q2.y - q1.y;
    float b2 = q1.x - q2.x;
    float c2 = a2 * q1.x + b2 * q1.y;
    float det = a1 * b2 - a2 * b1;

    if (!floatEquals(det, 0)) {
        i.x = (b2 * c1 - b1 * c2) / det;
        i.y = (a1 * c2 - a2 * c1) / det;
    }

    return i;
}

bool BayazitDecomposer::lineIntersect(const sf::Vector2f &point1, const sf::Vector2f &point2,
                                      const sf::Vector2f &point3, const sf::Vector2f &point4,
                                      bool firstIsSegment, bool secondIsSegment, sf::Vector2f &point)
{
    point.x = 0;
    point.y = 0;

    float a = point4.y - point3.y;
    float b = point2.x - point1.x;
    float c = point4.x - point3.x;
    float d = point2.y - point1.y;

    float denom = (a * b) - (c * d);

    if (!(denom >= -0.0001f && denom <= 0.0001f)) {
        float e = point1.y - point3.y;
        float f = point1.x - point3.x;
        float oneOverDenom = 1.0f / denom;

        float ua = (c * e) - (a * f);
        ua *= oneOverDenom;

        if (!firstIsSegment || ua >= 0.0f && ua <= 1.0f) {
            float ub = (b * e) - (d * f);
            ub *= oneOverDenom;

            if (!secondIsSegment || ub >= 0.0f && ub <= 1.0f) {
                if (ua != 0.0f || ub != 0.0f) {
                    point.x = point1.x + ua * b;
                    point.y = point1.y + ua * d;
                    return true;
                }
            }
        }
    }

    return false;
}

bool BayazitDecomposer::lineIntersect(const sf::Vector2f &point1, const sf::Vector2f &point2,
                                      const sf::Vector2f &point3, const sf::Vector2f &point4,
                                      sf::Vector2f &intersectionPoint)
{
    return lineIntersect(point1, point2, point3, point4, true, true, intersectionPoint);
}

bool BayazitDecomposer::canSee(int i, int j, const std::vector<sf::Vector2f> &vertices)
{
    if (reflex(i, vertices)) {
        if (leftOn(at(i, vertices), at(i - 1, vertices), at(j, vertices)) &&
                rightOn(at(i, vertices), at(i + 1, vertices), at(j, vertices)))
            return false;
    } else {
        if (rightOn(at(i, vertices), at(i + 1, vertices), at(j, vertices)) ||
                leftOn(at(i, vertices), at(i - 1, vertices), at(j, vertices)))
            return false;
    }

    if (reflex(j, vertices)) {
        if (leftOn(at(j, vertices), at(j - 1, vertices), at(i, vertices)) &&
                rightOn(at(j, vertices), at(j + 1, vertices), at(i, vertices)))
            return false;
    } else {
        if (rightOn(at(j, vertices), at(j + 1, vertices), at(i, vertices)) ||
                leftOn(at(j, vertices), at(j - 1, vertices), at(i, vertices)))
            return false;
    }

    for (int k = 0; k < (int32_t)vertices.size(); ++k) {
        if ((k + 1) % (int32_t)vertices.size() == i || k == i || (k + 1) % (int32_t)vertices.size() == j || k == j)
            continue;
        sf::Vector2f intersectionPoint;
        if (lineIntersect(at(i, vertices), at(j, vertices), at(k, vertices), at(k + 1, vertices), intersectionPoint))
            return false;
    }

    return true;
}

bool BayazitDecomposer::floatEquals(float value1, float value2)
{
    return std::fabs(value1 - value2) <= 0.0001f;
}

float BayazitDecomposer::squareDist(const sf::Vector2f &a, const sf::Vector2f &b)
{
    float dx = b.x - a.x;
    float dy = b.y - a.y;
    return dx * dx + dy * dy;
}

}
}
