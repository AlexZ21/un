#include <un/utils/utils.h>

#include <chrono>

namespace un {
namespace utils {

uint64_t currentTimestamp()
{
    return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
}

uint64_t cantorPairing(int64_t v1, int64_t v2)
{
    uint64_t a = (uint64_t)(v1 >= 0 ? 2 * (uint64_t)v1 : -2 * (uint64_t)v1 - 1);
    uint64_t b = (uint64_t)(v2 >= 0 ? 2 * (uint64_t)v2 : -2 * (uint64_t)v2 - 1);
    uint64_t c = (uint64_t)((a >= b ? a * a + a + b : a + b * b) / 2);
    return (v1 < 0 && v2 < 0) || (v1 >= 0 && v2 >= 0) ? c : -c - 1;
}

}
}
