#include <un/utils/log.h>

namespace un {
namespace utils {

Log &Log::instance()
{
    static Log log;
    return log;
}

}
}
