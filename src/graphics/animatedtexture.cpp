#include <un/graphics/animatedtexture.h>

namespace un {

AnimatedTexture::AnimatedTexture(sf::Texture *source,
                                 bool looped,
                                 const sf::Time &frameTime,
                                 const std::vector<sf::IntRect> frames) :
    m_source(source),
    m_frames(frames),
    m_looped(looped),
    m_frameTime(frameTime)
{

}

sf::IntRect AnimatedTexture::frameRect(size_t frame) const
{
    if (frame >= m_frames.size())
        return sf::IntRect();
    return m_frames.at(frame);
}

size_t AnimatedTexture::frameCount() const
{
    return m_frames.size();
}

sf::Time AnimatedTexture::frameTime() const
{
    return m_frameTime;
}

bool AnimatedTexture::isLooped() const
{
    return m_looped;
}

}
