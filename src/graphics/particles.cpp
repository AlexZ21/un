#include <un/graphics/particles.h>
#include <un/graphics/animatedtexturestate.h>
#include <un/graphics/texture.h>
#include <un/utils/log.h>

namespace un {

ParticleEmitter::ParticleEmitter() :
    emissionRate(1.f),
    emissionDifference(0.f),
    particleLifetime(sf::seconds(1000)),
    particlePosition(sf::Vector2f(0.f, 0.f)),
    particleVelocity(0.f),
    particleDirection(0.f),
    particleRotation(0.f),
    particleRotationSpeed(0.f),
    particleScale(1.f),
    particleScaleSpeed(0.f),
    particleOpacity(1.f),
    particleOpacitySpeed(0.f),
    particleColor(sf::Color::White),
    particleTextureIndex(0)
{

}

Particle::Particle(const sf::Time &lifetime) :
    passed(sf::milliseconds(0)),
    lifetime(lifetime),
    position(),
    velocity(0.f),
    direction(0.f),
    rotation(0.f),
    rotationSpeed(0.f),
    scale(1.f),
    scaleSpeed(0.f),
    opacity(1.f),
    opacitySpeed(0.f),
    color(255, 255, 255),
    textureIndex(0)
{

}

Particles::Particles() :
    m_vertices(sf::Quads),
    m_texture(nullptr)
{

}

Particles::Particles(Texture *texture) :
    m_vertices(sf::Quads)
{
    setTexture(texture);
}

Texture *Particles::texture() const
{
    return m_texture;
}

void Particles::setTexture(Texture *texture)
{
    m_texture = texture;
    updateQuads();
}

void Particles::addTextureRect(const sf::IntRect &rect)
{
    m_textureRects.push_back(rect);
    updateQuads();
}

void Particles::removeTextureRect(uint32_t index)
{
    if (index >= m_textureRects.size())
        return;
    m_textureRects.erase(m_textureRects.begin() + index);
    updateQuads();
}

void Particles::addEmitter(const ParticleEmitter &emitter)
{
    m_emitters.push_back(emitter);
}

void Particles::clearEmitters()
{
    m_emitters.clear();
}

void Particles::update(const sf::Transform &transform)
{
    uint32_t msec = m_updateClock.getElapsedTime().asMilliseconds();
    m_updateClock.restart();

    for (ParticleEmitter &emitter : m_emitters) {
        float particleAmount = emitter.emissionRate * ((float)msec / 1000) + emitter.emissionDifference;
        const size_t nbParticles = particleAmount;
        emitter.emissionDifference = particleAmount - nbParticles;

        for (size_t i = 0; i < nbParticles; ++i) {
            Particle particle( emitter.particleLifetime());
            particle.position = transform.transformPoint(emitter.particlePosition());
            particle.velocity = emitter.particleVelocity();
            particle.direction = emitter.particleDirection();
            particle.rotation = emitter.particleRotation();
            particle.rotationSpeed = emitter.particleRotationSpeed();
            particle.scale = emitter.particleScale();
            particle.scaleSpeed = emitter.particleScaleSpeed();
            particle.opacity = emitter.particleScale();
            particle.opacitySpeed = emitter.particleOpacitySpeed();
            particle.color = emitter.particleColor();
            particle.textureIndex = emitter.particleTextureIndex();
            m_particles.push_back(particle);
        }
    }

    for (auto it = m_particles.begin(); it != m_particles.end(); ) {
        updateParticle(*it, msec);
        if (it->passed > it->lifetime)
            it = m_particles.erase(it);
        else
            ++it;
    }
}

sf::FloatRect Particles::localBounds() const
{
    return m_vertices.getBounds();
}

sf::FloatRect Particles::globalBounds() const
{
    return getTransform().transformRect(localBounds());
}

void Particles::draw(sf::RenderTarget &target, sf::RenderStates state) const
{
    if (m_texture) {
        const_cast<Particles*>(this)->update(state.transform * getTransform());
        const_cast<Particles*>(this)->updateVertices();
        state.transform = sf::Transform();
        state.texture = m_texture->source();
        target.draw(m_vertices, state);
    }
}

void Particles::updateParticle(Particle &particle, uint32_t msec)
{
    particle.passed += sf::milliseconds(msec);

    if (particle.velocity != 0) {
        particle.position.x += particle.velocity * std::cos(particle.direction * (M_PI / 180));
        particle.position.y += particle.velocity * std::sin(particle.direction * (M_PI / 180));
    }

    if (particle.rotationSpeed != 0) {
        particle.rotation += particle.rotationSpeed;
        if (particle.rotation > 360 || particle.rotation < -360)
            particle.rotation = (int32_t)particle.rotation % 360;
    }

    if (particle.scaleSpeed != 0) {
        particle.scale += particle.scaleSpeed;
        if (particle.scale < 0)
            particle.scale = 0;
    }

    if (particle.opacitySpeed != 0) {
        particle.opacity += particle.opacitySpeed;
        if (particle.opacity < 0)
            particle.opacity = 0;
        if (particle.opacity > 1)
            particle.opacity = 1;
        particle.color.a = particle.opacity * (float)255;
    }
}

void Particles::updateVertices()
{
    m_vertices.clear();

    for (const Particle &particle : m_particles) {
        sf::Transform t;
        t.translate(particle.position);
        t.rotate(particle.rotation);
        t.scale(sf::Vector2f(particle.scale, particle.scale));

        const auto &quad = m_quads[particle.textureIndex];
        for (int32_t i = 0; i < 4; ++i) {
            sf::Vertex vertex;
            vertex.position = t.transformPoint(quad[i].position);
            vertex.texCoords = quad[i].texCoords;
            vertex.color = particle.color;
            m_vertices.append(vertex);
        }
    }
}

void Particles::updateQuads()
{
    if (m_textureRects.empty()) {
        m_quads.resize(1);
        updateQuad(m_quads[0], m_texture->rect());
    } else {
        m_quads.resize(m_textureRects.size());
        for (size_t i = 0; i < m_textureRects.size(); ++i)
            updateQuad(m_quads[i], m_textureRects[i]);
    }
}

void Particles::updateQuad(std::array<sf::Vertex, 4> &quad, const sf::IntRect &textureRect)
{
    sf::FloatRect rect(textureRect);

    quad[0].texCoords = sf::Vector2f(rect.left, rect.top);
    quad[1].texCoords = sf::Vector2f(rect.left + rect.width, rect.top);
    quad[2].texCoords = sf::Vector2f(rect.left + rect.width, rect.top + rect.height);
    quad[3].texCoords = sf::Vector2f(rect.left, rect.top + rect.height);

    quad[0].position = sf::Vector2f(-rect.width, -rect.height) / 2.f;
    quad[1].position = sf::Vector2f( rect.width, -rect.height) / 2.f;
    quad[2].position = sf::Vector2f( rect.width, rect.height) / 2.f;
    quad[3].position = sf::Vector2f(-rect.width, rect.height) / 2.f;
}

}
