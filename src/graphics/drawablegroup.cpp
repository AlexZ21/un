#include <un/graphics/drawablegroup.h>
#include <un/graphics/drawable.h>
#include <un/utils/utils.h>
#include <un/utils/log.h>

namespace un {

void DrawableGroup::add(Drawable *drawable)
{
    add(0, drawable);
}

void DrawableGroup::add(const std::vector<Drawable *> &drawables)
{
    add(0, drawables);
}

void DrawableGroup::add(uint32_t state, Drawable *drawable)
{
    auto it = m_stateDrawables.find(state);
    if (it == m_stateDrawables.end())
        m_stateDrawables.emplace(state, std::vector<Drawable*>());

    auto it2 = std::find_if(m_drawables.begin(), m_drawables.end(),
                            [drawable](const auto &d) { return drawable == d.get(); });
    if (it2 != m_drawables.end())
        return;

    m_drawables.emplace_back(uuptr<Drawable>(drawable));
    m_stateDrawables[state].emplace_back(drawable);
}

void DrawableGroup::add(uint32_t state, const std::vector<Drawable *> &drawables)
{
    auto it = m_stateDrawables.find(state);
    if (it == m_stateDrawables.end())
        m_stateDrawables.emplace(state, std::vector<Drawable *>());

    for (un::Drawable *drawable : drawables) {
        auto it2 = std::find_if(m_drawables.begin(), m_drawables.end(),
                                [drawable](const auto &d) { return drawable == d.get(); });
        if (it2 != m_drawables.end())
            continue;

        m_drawables.emplace_back(uuptr<un::Drawable>(drawable));
        m_stateDrawables[state].emplace_back(drawable);
    }
}

const std::vector<Drawable *> *DrawableGroup::drawables(uint32_t state) const
{
    auto it = m_stateDrawables.find(state);
    if (it == m_stateDrawables.end())
        return nullptr;
    return &m_stateDrawables.at(state);
}

sf::FloatRect DrawableGroup::localBounds(uint32_t state) const
{
    const std::vector<Drawable *> *d = drawables(state);
    if (!d)
        return sf::FloatRect();

    sf::FloatRect bounds;
    for (auto &drawable : *d)
        un::utils::combine(bounds, drawable->globalBounds());
    bounds.left = 0;
    bounds.top = 0;

    return bounds;
}

sf::FloatRect DrawableGroup::globalBounds(uint32_t state, sf::Transform transform) const
{
    const std::vector<Drawable *> *d = drawables(state);
    if (!d)
        return sf::FloatRect();

    sf::FloatRect bounds;
    for (auto &drawable : *d)
        un::utils::combine(bounds, drawable->globalBounds());

    bounds.width /= 2;
    bounds.height /= 2;

    return transform.transformRect(bounds);
}

void DrawableGroup::draw(uint32_t state, sf::RenderTarget &target, sf::RenderStates states) const
{
    const std::vector<Drawable *> *d = drawables(state);
    if (!d)
        return;
    for (Drawable *drawable : *d)
        target.draw(*drawable, states.transform);
}

}
