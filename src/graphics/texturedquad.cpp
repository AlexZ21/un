#include <un/graphics/texturedquad.h>
#include <un/graphics/animatedtexture.h>
#include <un/graphics/animatedtexturestate.h>
#include <un/core/application.h>
#include <un/utils/log.h>

namespace un {

TexturedQuad::TexturedQuad() :
    m_texture(nullptr),
    m_state(nullptr),
    m_vertices(sf::TriangleStrip)
{
    m_vertices.resize(4);
}

TexturedQuad::TexturedQuad(Texture *texture, bool updateSize) :
    m_state(nullptr),
    m_vertices(sf::TriangleStrip)
{
    m_vertices.resize(4);
    setTexture(texture, updateSize);
}

sf::Color TexturedQuad::color() const
{
    return m_color;
}

void TexturedQuad::setColor(const sf::Color &color)
{
    m_color = color;
    m_vertices[0].color = m_color;
    m_vertices[1].color = m_color;
    m_vertices[2].color = m_color;
    m_vertices[3].color = m_color;
}

Texture *TexturedQuad::texture() const
{
    return m_texture;
}

void TexturedQuad::setTexture(Texture *texture, bool updateSize)
{
    m_texture = texture;

    if (texture && texture->type() == Texture::Animated) {
        AnimatedTexture *animTex = reinterpret_cast<AnimatedTexture *>(texture);
        m_state.reset(new AnimatedTextureState(animTex));
        m_state->onUpdated(std::bind(&TexturedQuad::updateTexCoords, this));
    } else {
        m_state.reset();
    }

    updateTexCoords();
    if (updateSize)
        updatePositions();
}

AnimatedTextureState *TexturedQuad::animatedTextureState() const
{
    return m_state.get();
}

sf::Vector2i TexturedQuad::size() const
{
    return sf::Vector2i(m_vertices[2].position.x, m_vertices[1].position.y);
}

void TexturedQuad::setSize(int32_t width, int32_t height)
{
    m_vertices[1].position = sf::Vector2f(0, height);
    m_vertices[2].position = sf::Vector2f(width, 0);
    m_vertices[3].position = sf::Vector2f(width, height);
}

void TexturedQuad::setSize(const sf::Vector2i &size)
{
    m_vertices[1].position = sf::Vector2f(0, size.y);
    m_vertices[2].position = sf::Vector2f(size.x, 0);
    m_vertices[3].position = sf::Vector2f(size.x, size.y);
}

sf::FloatRect TexturedQuad::localBounds() const
{
    return m_vertices.getBounds();
}

sf::FloatRect TexturedQuad::globalBounds() const
{
    return getTransform().transformRect(localBounds());
}

void TexturedQuad::draw(sf::RenderTarget &target, sf::RenderStates state) const
{
    if (m_texture) {
        if (m_state)
            m_state->update();
        state.transform *= getTransform();
        state.texture = m_texture->source();
        target.draw(m_vertices, state);
    }
}

void TexturedQuad::updatePositions()
{
    if (m_texture) {
        m_vertices[0].position = sf::Vector2f(0, 0);
        m_vertices[1].position = sf::Vector2f(0, m_texture->rect().height);
        m_vertices[2].position = sf::Vector2f(m_texture->rect().width, 0);
        m_vertices[3].position = sf::Vector2f(m_texture->rect().width, m_texture->rect().height);
    }
}

void TexturedQuad::updateTexCoords()
{
    if (m_texture) {
        sf::IntRect frameRect;
        if (m_state)
            frameRect = m_state->currentRect();
        else
            frameRect = m_texture->rect();

        float left = static_cast<float>(frameRect.left);
        float right = left + frameRect.width;
        float top = static_cast<float>(frameRect.top);
        float bottom = top + frameRect.height;

        m_vertices[0].texCoords = sf::Vector2f(left, top);
        m_vertices[1].texCoords = sf::Vector2f(left, bottom);
        m_vertices[2].texCoords = sf::Vector2f(right, top);
        m_vertices[3].texCoords = sf::Vector2f(right, bottom);
    }
}

}
