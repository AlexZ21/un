#include <un/graphics/animatedtexturestate.h>
#include <un/graphics/animatedtexture.h>

#include <algorithm>

namespace un {

AnimatedTextureState::AnimatedTextureState(AnimatedTexture *texture) :
    m_texture(texture),
    m_frameTime(texture->frameTime()),
    m_currentFrame(0),
    m_paused(true),
    m_looped(texture->isLooped())
{

}

void AnimatedTextureState::play()
{
    m_paused = false;
    m_updateTime.restart();
}

void AnimatedTextureState::pause()
{
    m_looped = true;
}

void AnimatedTextureState::stop()
{
    m_looped = true;
    m_currentFrame = 0;
}

void AnimatedTextureState::onUpdated(const std::function<void ()> &onUpdated)
{
    m_onUpdated = onUpdated;
}

bool AnimatedTextureState::isLooped() const
{
    return m_looped;
}

void AnimatedTextureState::setLooped(bool looped)
{
    m_looped = looped;
}

sf::Time AnimatedTextureState::frameTime() const
{
    return m_frameTime;
}

void AnimatedTextureState::setFrameTime(const sf::Time &frameTime)
{
    m_frameTime = frameTime;
}

sf::IntRect AnimatedTextureState::currentRect() const
{
    return m_texture->frameRect(m_currentFrame);
}

void AnimatedTextureState::update()
{
    if (!m_paused) {
        sf::Time deltaTime = m_updateTime.getElapsedTime();
        if (deltaTime >= m_frameTime) {
            m_updateTime.restart();
            if (m_currentFrame + 1 < m_texture->frameCount()) {
                ++m_currentFrame;
            } else {
                m_currentFrame = 0;
                if (!m_looped)
                    m_paused = true;
            }
            if (m_onUpdated)
                m_onUpdated();
        }
    }
}

}
