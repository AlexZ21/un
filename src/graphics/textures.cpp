#include <un/graphics/textures.h>
#include <un/graphics/statictexture.h>
#include <un/graphics/animatedtexture.h>
#include <un/core/application.h>
#include <un/core/assets.h>
#include <un/utils/log.h>

namespace un {

Textures::Textures() : Manager()
{

}

Texture *Textures::texture(const std::string &name) const
{
    auto it = m_textures.find(name);
    if (it == m_textures.end()) {
        LERR("Texture <", name, "> not loaded");
        return nullptr;
    }
    return it->second.get();
}

StaticTexture *Textures::staticTexture(const std::string &name) const
{
    return static_cast<StaticTexture *>(texture(name));
}

StaticTexture *Textures::loadStaticTexture(const std::string &name, const std::string &path, const sf::IntRect &rect)
{
    auto it = m_textures.find(name);
    if (it != m_textures.end())
        return staticTexture(name);

    sf::Texture *source = sourceTexture(path);
    if (!source) {
        LERR("Can't load texure <", path, ">");
        return nullptr;
    }

    sf::IntRect r = rect;
    if (r.width == 0 || r.height == 0) {
        r.width = source->getSize().x;
        r.height = source->getSize().y;
    }

    StaticTexture *texture = new StaticTexture(source, r);
    m_textures.emplace(name, uuptr<Texture>(texture));

    return texture;
}

AnimatedTexture *Textures::animatedTexture(const std::string &name) const
{
    return static_cast<AnimatedTexture *>(texture(name));
}

AnimatedTexture *Textures::loadAnimatedTexture(const std::string &name, const std::string &path,
                                               bool looped, const sf::Time &frameTime,
                                               const sf::Vector2i &tileSize, const std::vector<sf::Vector2i> frames)
{
    auto it = m_textures.find(name);
    if (it != m_textures.end())
        return animatedTexture(name);

    sf::Texture *source = sourceTexture(path);
    if (!source) {
        LERR("Can't load texure <", path, ">");
        return nullptr;
    }

    std::vector<sf::IntRect> frameRects;
    for (const sf::Vector2i &frame : frames)
        frameRects.push_back(sf::IntRect(tileSize.x * frame.x, tileSize.y * frame.y, tileSize.x, tileSize.y));

    AnimatedTexture *texture = new AnimatedTexture(source, looped, frameTime, frameRects);
    m_textures.emplace(name, uuptr<Texture>(texture));

    return texture;
}

AnimatedTexture *Textures::loadAnimatedTexture(const std::string &name, const std::string &path, bool looped,
                                               const sf::Time &frameTime, const sf::Vector2i &tileSize,
                                               const sf::Vector2i &firstFrame, const sf::Vector2i &lastFrame)
{
    std::vector<sf::Vector2i> frames;
    for (int32_t x = firstFrame.x; x <= lastFrame.x; ++x) {
        for (int32_t y = firstFrame.y; y <= lastFrame.y; ++y) {
            frames.push_back(sf::Vector2i(x, y));
        }
    }
    return loadAnimatedTexture(name, path, looped, frameTime, tileSize, frames);
}

sf::Texture *Textures::sourceTexture(const std::string &path)
{
    auto it = m_sources.find(path);
    if (it != m_sources.end())
        return it->second.get();

    uuptr<sf::Texture> tex = std::make_unique<sf::Texture>();
    std::vector<char> data = Application::manager<Assets>()->readFile(path);

    if (!tex->loadFromMemory(data.data(), data.size()))
        return nullptr;

    auto emplaced = m_sources.emplace(path, std::move(tex));
    return emplaced.first->second.get();
}

}
