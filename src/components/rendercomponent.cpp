#include <un/components/rendercomponent.h>
#include <un/graphics/animatedtexturestate.h>
#include <un/utils/log.h>

#include <SFML/Graphics/Texture.hpp>

namespace un {

RenderComponent::RenderComponent() :
    m_state(0)
{

}

void RenderComponent::setDrawableGroup(DrawableGroup *drawableGroup)
{
    m_drawableGroup.reset(drawableGroup);
    play();
}

void RenderComponent::unsetDrawableGroup()
{
    m_drawableGroup.reset();
}

DrawableGroup *RenderComponent::drawableGroup() const
{
    return m_drawableGroup.get();
}

uint32_t RenderComponent::state() const
{
    return m_state;
}

void RenderComponent::setState(uint32_t state)
{
    m_state = state;
    play();
}

void RenderComponent::play()
{
    const std::vector<Drawable *> *drawables = m_drawableGroup->drawables(m_state);
    if (!drawables)
        return;
    for (const Drawable *drawable : *drawables) {
        AnimatedTextureState *atState = drawable->animatedTextureState();
        if (!atState)
            continue;
        atState->play();
    }
}

void RenderComponent::pause()
{
    const std::vector<Drawable *> *drawables = m_drawableGroup->drawables(m_state);
    if (!drawables)
        return;
    for (const Drawable *drawable : *drawables) {
        AnimatedTextureState *atState = drawable->animatedTextureState();
        if (!atState)
            continue;
        atState->pause();
    }
}

void RenderComponent::stop()
{
    const std::vector<Drawable *> *drawables = m_drawableGroup->drawables(m_state);
    if (!drawables)
        return;
    for (const Drawable *drawable : *drawables) {
        AnimatedTextureState *atState = drawable->animatedTextureState();
        if (!atState)
            continue;
        atState->stop();
    }
}

}
