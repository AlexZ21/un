#include <un/components/scenecomponent.h>

namespace un {

SceneComponent::SceneComponent(Scene *scene) :
    m_scene(scene)
{
}

Scene *SceneComponent::scene() const
{
    return m_scene;
}

}
