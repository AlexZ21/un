#include <un/components/box2dcomponent.h>
#include <un/utils/bayazitdecomposer.h>
#include <un/utils/log.h>

namespace un {

std::pair<sf::Vector2f, sf::Vector2f> minMaxVertex(const std::vector<sf::Vector2f> &vertices)
{
    sf::Vector2f minVertex;
    sf::Vector2f maxVertex;
    for (const sf::Vector2f &vertex : vertices) {
        if (vertex.x < minVertex.x)
            minVertex.x = vertex.x;
        if (vertex.y < minVertex.y)
            minVertex.y = vertex.y;
        if (vertex.x > maxVertex.x)
            maxVertex.x = vertex.x;
        if (vertex.y > maxVertex.y)
            maxVertex.y = vertex.y;
    }
    return std::make_pair(minVertex, maxVertex);
}

Box2dComponent::Box2dComponent() :
    m_world(nullptr),
    m_body(nullptr)
{

}

b2Body *Box2dComponent::body() const
{
    return m_body;
}

void Box2dComponent::createBody(std::vector<sf::Vector2f> vertices, BodyType bodyType, sf::Vector2f center)
{
    if (!m_world)
        return;

    using Polygon = std::vector<sf::Vector2f>;
    auto polygons = utils::BayazitDecomposer::decompose(vertices);

    b2BodyDef bodyDef;
    bodyDef.type = bodyType == Dynamic ? b2_dynamicBody : b2_staticBody;
    auto v = minMaxVertex(vertices);
    bodyDef.position.Set(((v.second.x - v.first.x) / 2) / SCALE,
                         ((v.second.y - v.first.y) / 2) / SCALE);
    bodyDef.fixedRotation = false;
    bodyDef.linearDamping = 1.3;
    bodyDef.angle = 0;

    m_body = m_world->CreateBody(&bodyDef);

    for (Polygon &polygon : polygons) {
        sf::Vector2f minVertex;
        sf::Vector2f maxVertex;

        b2Vec2 vertices[polygon.size()];

        for (size_t i = 0; i < polygon.size(); ++i) {
            const sf::Vector2f &vertex = polygon.at(i);

            if (vertex.x < minVertex.x)
                minVertex.x = vertex.x;
            if (vertex.y < minVertex.y)
                minVertex.y = vertex.y;
            if (vertex.x > maxVertex.x)
                maxVertex.x = vertex.x;
            if (vertex.y > maxVertex.y)
                maxVertex.y = vertex.y;

            vertices[i].Set(vertex.x / SCALE, vertex.y / SCALE);
        }

        float width = (maxVertex.x - minVertex.x) / 2;
        float height = (maxVertex.y = minVertex.y) / 2;

        b2PolygonShape shape;
        shape.SetAsBox(width / SCALE, height / SCALE,
                       b2Vec2(width / SCALE, height / SCALE), 0);
        shape.Set(vertices, polygon.size());

        b2FixtureDef fixtureDef;
        fixtureDef.shape = &shape;
        fixtureDef.density = 1;

        m_body->CreateFixture(&fixtureDef);
    }
}

sf::Vector2f Box2dComponent::position() const
{
    if (!m_body)
        return sf::Vector2f();
    b2Vec2 p = m_body->GetPosition();
    return sf::Vector2f(p.x * SCALE, p.y * SCALE);
}

void Box2dComponent::setPosition(float x, float y)
{
    if (!m_body)
        return;
    m_body->SetTransform(b2Vec2(x / SCALE, y / SCALE), m_body->GetAngle());
}

void Box2dComponent::setPosition(const sf::Vector2f &position)
{
    setPosition(position.x, position.y);
}

float Box2dComponent::rotation() const
{
    if (!m_body)
        return 0;
    return m_body->GetAngle() * DEG;
}

void Box2dComponent::setRotation(float angle)
{
    if (!m_body)
        return;
    m_body->SetTransform(m_body->GetPosition(), angle / DEG);
}

void Box2dComponent::move(float x, float y)
{
    if (!m_body)
        return;
    sf::Vector2f p = position();
    setPosition(p.x + x / SCALE, p.y + y / SCALE);
}

void Box2dComponent::move(const sf::Vector2f &position)
{
    sf::Vector2f p =  Box2dComponent::position();
    setPosition(position.x / SCALE + p.x, position.y / SCALE + p.y);
}

void Box2dComponent::rotate(float angle)
{
    setRotation(rotation() + angle);
}

bool Box2dComponent::isFixedRotation() const
{
    if (!m_body)
        return false;
    return m_body->IsFixedRotation();
}

void Box2dComponent::setFixedRotation(bool fixed)
{
    if (!m_body)
        return;
    m_body->SetFixedRotation(fixed);
}

sf::Vector2f Box2dComponent::worldCenter() const
{
    if (!m_body)
        return sf::Vector2f();
    b2Vec2 p = m_body->GetWorldCenter();
    return sf::Vector2f(p.x * SCALE, p.y * SCALE);
}

sf::Vector2f Box2dComponent::linearVelocity() const
{
    if (!m_body)
        return sf::Vector2f();
    b2Vec2 velocity = m_body->GetLinearVelocity();
    return sf::Vector2f(velocity.x, velocity.y);
}

void Box2dComponent::setLinearVelocity(float x, float y)
{
    if (!m_body)
        return;
    m_body->SetLinearVelocity(b2Vec2(x, y));
}

void Box2dComponent::setLinearVelocity(const sf::Vector2f &velocity)
{
    setLinearVelocity(velocity.x, velocity.y);
}

}
