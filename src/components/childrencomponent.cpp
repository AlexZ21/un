#include <un/components/childrencomponent.h>

#include <algorithm>

namespace un {

ChildrenComponent::ChildrenComponent()
{

}

void ChildrenComponent::addChild(const Entity &entity)
{
    ChildrenComponent *childChildrenComponent = entity.component<ChildrenComponent>();
    if (!childChildrenComponent || std::find(m_children.begin(), m_children.end(), entity) != m_children.end())
        return;
    m_achildren.push_back(entity);
}

void ChildrenComponent::removeChild(const Entity &entity)
{
    if (!entity.hasComponent<ChildrenComponent>())
        return;
    auto it = std::find(m_children.begin(), m_children.end(), entity);
    if (it == m_children.end())
        return;
    m_rchildren.push_back(entity);
}

void ChildrenComponent::removeChildren()
{
    m_rchildren = std::move(m_children);
}

const std::vector<Entity> &ChildrenComponent::children() const
{
    return m_children;
}

}
