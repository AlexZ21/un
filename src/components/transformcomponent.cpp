#include <un/components/transformcomponent.h>

#include <cmath>

namespace un {

TransformComponent::TransformComponent() :
    m_origin(0, 0),
    m_position(0, 0),
    m_rotation(0),
    m_scale(1, 1),
    m_zindex(0),
    m_parentTransform(),
    m_multiplyWithParent(true),
    m_transform(),
    m_transformNeedUpdate(true),
    m_inverseTransform(),
    m_inverseTransformNeedUpdate(true)
{

}

sf::Vector2f TransformComponent::position() const
{
    return m_position;
}

void TransformComponent::setPosition(float x, float y)
{
    m_changed = true;
    m_position.x = x;
    m_position.y = y;
    m_transformNeedUpdate = true;
    m_inverseTransformNeedUpdate = true;
}

void TransformComponent::setPosition(const sf::Vector2f &position)
{
    setPosition(position.x, position.y);
}

float TransformComponent::rotation() const
{
    return m_rotation;
}

void TransformComponent::setRotation(float angle)
{
    m_changed = true;
    m_rotation = static_cast<float>(fmod(angle, 360));
    if (m_rotation < 0)
        m_rotation += 360.f;
    m_transformNeedUpdate = true;
    m_inverseTransformNeedUpdate = true;
}

sf::Vector2f TransformComponent::scale() const
{
    return m_scale;
}

void TransformComponent::setScale(float factorX, float factorY)
{
    m_changed = true;
    m_scale.x = factorX;
    m_scale.y = factorY;
    m_transformNeedUpdate = true;
    m_inverseTransformNeedUpdate = true;
}

void TransformComponent::setScale(const sf::Vector2f &factors)
{
    setScale(factors.x, factors.y);
}

sf::Vector2f TransformComponent::origin() const
{
    return m_origin;
}

void TransformComponent::setOrigin(float x, float y)
{
    m_changed = true;
    m_origin.x = x;
    m_origin.y = y;
    m_transformNeedUpdate = true;
    m_inverseTransformNeedUpdate = true;
}

void TransformComponent::setOrigin(const sf::Vector2f &origin)
{
    setOrigin(origin.x, origin.y);
}

float TransformComponent::zIndex() const
{
    return m_zindex;
}

void TransformComponent::setZIndex(float zindex)
{
    m_zindex = zindex;
}

void TransformComponent::move(float offsetX, float offsetY)
{
    setPosition(m_position.x + offsetX, m_position.y + offsetY);
}

void TransformComponent::move(const sf::Vector2f &offset)
{
    setPosition(m_position.x + offset.x, m_position.y + offset.y);
}

void TransformComponent::rotate(float angle)
{
    setRotation(m_rotation + angle);
}

void TransformComponent::scale(float factorX, float factorY)
{
    setScale(m_scale.x * factorX, m_scale.y * factorY);
}

void TransformComponent::scale(const sf::Vector2f &factor)
{
    setScale(m_scale.x * factor.x, m_scale.y * factor.y);
}

const sf::Transform &TransformComponent::parentTransform() const
{
    return m_parentTransform;
}

void TransformComponent::setParentTransform(const sf::Transform &transform)
{
    m_parentTransform = transform;
    m_multiplyWithParent = m_parentTransform == sf::Transform::Identity;
    m_transformNeedUpdate = true;
    m_inverseTransformNeedUpdate = true;
}

void TransformComponent::unsetParentTransform()
{
    m_parentTransform = sf::Transform::Identity;
    m_multiplyWithParent = false;
    m_transformNeedUpdate = true;
    m_inverseTransformNeedUpdate = true;
}

const sf::Transform &TransformComponent::transform() const
{
    if (m_transformNeedUpdate) {
        float angle = -m_rotation * 3.141592654f / 180.f;
        float cosine = static_cast<float>(std::cos(angle));
        float sine = static_cast<float>(std::sin(angle));
        float sxc = m_scale.x * cosine;
        float syc = m_scale.y * cosine;
        float sxs = m_scale.x * sine;
        float sys = m_scale.y * sine;
        float tx = -m_origin.x * sxc - m_origin.y * sys + m_position.x;
        float ty =  m_origin.x * sxs - m_origin.y * syc + m_position.y;

        if (!m_multiplyWithParent) {
            m_transform = m_parentTransform * sf::Transform(sxc, sys, tx,
                                                            -sxs, syc, ty,
                                                            0.f, 0.f, 1.f);
        } else {
            m_transform = sf::Transform(sxc, sys, tx,
                                        -sxs, syc, ty,
                                        0.f, 0.f, 1.f);
        }

        m_transformNeedUpdate = false;
    }
    return m_transform;
}

const sf::Transform &TransformComponent::inverseTransform() const
{
    if (m_inverseTransformNeedUpdate) {
        m_inverseTransform = transform().getInverse();
        m_inverseTransformNeedUpdate = false;
    }
    return m_inverseTransform;
}

void TransformComponent::setTransform(const sf::Transform &transform)
{
    m_transform = transform;
}

bool TransformComponent::isChanged() const
{
    return m_changed;
}

}
