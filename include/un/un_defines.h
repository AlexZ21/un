#ifndef UN_DEFINES_H
#define UN_DEFINES_H

#include <stdint.h>
#include <memory>
#include <stdint.h>

// Define smart pointers for class
#define DEFINE_PTRS(__class__) \
    using uptr = std::unique_ptr<__class__>; \
    using sptr = std::shared_ptr<__class__>;

#define uuptr std::unique_ptr
#define usptr std::shared_ptr

#endif // UN_DEFINES_H
