#ifndef UN_TEXTURE_H
#define UN_TEXTURE_H

#include <un/un_export.h>
#include <un/un_defines.h>
#include <un/utils/ptr.h>
#include <SFML/Graphics/Texture.hpp>

namespace un {

class UN_EXPORT Texture
{
public:
    UN_PTRS(Texture)

    enum Type {
        Static,
        Animated
    };

    Texture() = default;
    virtual ~Texture() = default;

    virtual Type type() const { return Static; }
    virtual sf::Texture *source() const { return nullptr; }
    virtual sf::IntRect rect() const { return sf::IntRect(); }
};

}

#endif // UN_TEXTURE_H
