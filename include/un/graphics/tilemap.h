#ifndef UN_TILEMAP_H
#define UN_TILEMAP_H

#include <un/un_export.h>
#include <un/un_defines.h>
#include <un/graphics/drawable.h>

#include <vector>
#include <unordered_map>
#include <memory>

#include <SFML/Graphics/VertexArray.hpp>

namespace un {

class UN_EXPORT TileMap : public Drawable
{
public:
    using Chunk = sf::VertexArray;

    struct Tileset {
        std::string name;
        Texture *texture = nullptr;
        int32_t tileCount = 0;
        int32_t columns = 0;
        int32_t firstGid = 0;
        std::unordered_map<std::string, std::string> properties;
        std::unordered_map<int32_t, std::unordered_map<std::string, std::string>> tileProperties;
    };

    struct Layer {
        std::string name;
        Tileset *tileset = nullptr;
        std::unordered_map<int32_t, Chunk> chunks;
        std::unordered_map<std::string, std::string> properties;
    };

    TileMap(const std::string &tmxFile, sf::Vector2<uint32_t> chunkSize = sf::Vector2<uint32_t>(64, 64));
    ~TileMap() = default;

    Texture *texture() const { return nullptr; }
    AnimatedTextureState *animatedTextureState() const { return nullptr; }

    sf::FloatRect localBounds() const;
    sf::FloatRect globalBounds() const;

    void draw(sf::RenderTarget &target, sf::RenderStates states) const;

private:
    Layer *createLayer();
    sf::Vector2i chunkCount();
    Tileset *tileset(const std::string &name);
    Tileset *tileset(int32_t gid);

    void setTile(Layer *layer, size_t tileX, size_t tileY, const sf::IntRect &textureRect);
    void setTile(Layer *layer, size_t tileX, size_t tileY, const sf::Vector2i &texturePos);

    void loadTmxFile(const std::string &tmxFile);
    Tileset *loadTileset(const std::string assetsSource, const std::string &tsxFile);

private:
    sf::Vector2<uint32_t> m_mapSize;
    sf::Vector2<uint32_t> m_tileSize;
    sf::Vector2<uint32_t> m_chunkSize;

    std::vector<uuptr<Layer>> m_layers;
    std::vector<uuptr<Tileset>> m_tilesets;

};

}

#endif // UN_TILEMAP_H
