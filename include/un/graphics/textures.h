#ifndef UN_TEXTURES_H
#define UN_TEXTURES_H

#include <un/un_export.h>
#include <un/un_defines.h>
#include <un/core/manager.h>
#include <un/graphics/texture.h>

#include <string>
#include <unordered_map>

#include <SFML/Graphics.hpp>

namespace un {

class StaticTexture;
class AnimatedTexture;

class UN_EXPORT Textures : public Manager
{
public:
    UN_INHERIT_PTRS(Textures)

    Textures();
    ~Textures() = default;

    Texture *texture(const std::string &name) const;

    //! Static textures
    StaticTexture *staticTexture(const std::string &name) const;
    StaticTexture *loadStaticTexture(const std::string &name, const std::string &path,
                                     const sf::IntRect &rect = sf::IntRect());

    //! Animated textures
    AnimatedTexture *animatedTexture(const std::string &name) const;
    AnimatedTexture *loadAnimatedTexture(const std::string &name, const std::string &path,
                                         bool looped, const sf::Time &frameTime,
                                         const sf::Vector2i &tileSize,
                                         const std::vector<sf::Vector2i> frames);
    AnimatedTexture *loadAnimatedTexture(const std::string &name, const std::string &path,
                                         bool looped, const sf::Time &frameTime,
                                         const sf::Vector2i &tileSize,
                                         const sf::Vector2i &firstFrame,
                                         const sf::Vector2i &lastFrame);

private:
    sf::Texture *sourceTexture(const std::string &path);

private:
    std::unordered_map<std::string, uuptr<sf::Texture>> m_sources;
    std::unordered_map<std::string, uuptr<Texture>> m_textures;

};

}

#endif // UN_TEXTURES_H
