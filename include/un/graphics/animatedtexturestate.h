#ifndef UN_ANIMATEDTEXTURESTATE_H
#define UN_ANIMATEDTEXTURESTATE_H

#include <un/un_export.h>
#include <un/un_defines.h>

#include <SFML/System/Time.hpp>
#include <SFML/System/Clock.hpp>
#include <SFML/Graphics/Rect.hpp>

#include <vector>
#include <functional>

namespace un {

class AnimatedTexture;

class UN_EXPORT AnimatedTextureState
{
public:
    AnimatedTextureState(AnimatedTexture *texture);
    ~AnimatedTextureState() = default;

    void play();
    void pause();
    void stop();

    void onUpdated(const std::function<void ()> &onUpdated);

    bool isLooped() const;
    void setLooped(bool looped);

    sf::Time frameTime() const;
    void setFrameTime(const sf::Time &frameTime);

    sf::IntRect currentRect() const;

    void update();

private:
    AnimatedTexture *m_texture;
    sf::Clock m_updateTime;
    sf::Time m_frameTime;
    size_t m_currentFrame;
    bool m_paused;
    bool m_looped;
    std::function<void ()> m_onUpdated;

};

}

#endif // UN_ANIMATEDTEXTURESTATE_H
