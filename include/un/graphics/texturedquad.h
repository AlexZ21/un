#ifndef UN_TEXTUREDQUAD_H
#define UN_TEXTUREDQUAD_H

#include <un/un_export.h>
#include <un/un_defines.h>
#include <un/graphics/drawable.h>

namespace un {

class AnimatedTextureState;

class UN_EXPORT TexturedQuad : public Drawable
{
public:
    TexturedQuad();
    TexturedQuad(Texture *texture, bool updateSize = true);
    ~TexturedQuad() = default;

    sf::Color color() const;
    void setColor(const sf::Color &color);

    Texture *texture() const;
    void setTexture(Texture *texture, bool updateSize = true);

    AnimatedTextureState *animatedTextureState() const;

    sf::Vector2i size() const;
    void setSize(int32_t width, int32_t height);
    void setSize(const sf::Vector2i &size);

    sf::FloatRect localBounds() const;
    sf::FloatRect globalBounds() const;

    void draw(sf::RenderTarget &target, sf::RenderStates state) const;

private:
    void updatePositions();
    void updateTexCoords();

private:
    sf::Color m_color;
    Texture *m_texture;
    mutable uuptr<AnimatedTextureState> m_state;
    sf::VertexArray m_vertices;

};

}

#endif // UN_TEXTUREDQUAD_H
