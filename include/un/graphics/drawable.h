#ifndef UN_DRAWABLE_H
#define UN_DRAWABLE_H

#include <un/un_export.h>
#include <un/un_defines.h>

#include <SFML/Graphics/Transformable.hpp>
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/Vertex.hpp>
#include <SFML/Graphics/VertexArray.hpp>

namespace un {

class Texture;
class AnimatedTextureState;

class UN_EXPORT Drawable : public sf::Transformable, public sf::Drawable
{
public:
    Drawable() : m_zIndex(0) {}
    virtual ~Drawable() = default;

    float zIndex() const { return m_zIndex; }
    void setZIndex(float zIndex) { m_zIndex = zIndex; }

    virtual Texture *texture() const = 0;
    virtual AnimatedTextureState *animatedTextureState() const = 0;

    virtual sf::FloatRect localBounds() const = 0;
    virtual sf::FloatRect globalBounds() const = 0;

    virtual void draw(sf::RenderTarget &, sf::RenderStates) const = 0;


private:
    float m_zIndex;

};

}

#endif // UN_DRAWABLE_H
