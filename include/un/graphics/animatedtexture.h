#ifndef UN_ANIMATEDTEXTURE_H
#define UN_ANIMATEDTEXTURE_H

#include <un/un_export.h>
#include <un/un_defines.h>
#include <un/graphics/texture.h>
#include <un/utils/ptr.h>

#include <SFML/System/Time.hpp>

#include <vector>

namespace un {

class UN_EXPORT AnimatedTexture : public Texture
{
public:
    UN_INHERIT_PTRS(AnimatedTexture)

    AnimatedTexture(sf::Texture *source,
                    bool looped,
                    const sf::Time &frameTime,
                    const std::vector<sf::IntRect> frames);
    ~AnimatedTexture() = default;

    sf::IntRect frameRect(size_t frame) const;
    size_t frameCount() const;
    sf::Time frameTime() const;
    bool isLooped() const;

    Type type() const { return Animated; }
    sf::Texture *source() const { return m_source; }
    sf::IntRect rect() const { return !m_frames.empty() ? m_frames.at(0) : sf::IntRect(); }

private:
    sf::Texture *m_source;
    std::vector<sf::IntRect> m_frames;
    bool m_looped;
    sf::Time m_frameTime;

};

}

#endif // UN_ANIMATEDTEXTURE_H
