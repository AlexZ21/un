#ifndef TEXTURE_BIND_H
#define TEXTURE_BIND_H

#include <un/graphics/texture.h>
#include <un/utils/log.h>

#include <sol/sol.hpp>

namespace un {

inline void luaTextureBind(sol::state *state)
{
    // un namespace
    sol::table unTable = state->get<sol::table>("un");
    unTable.new_usertype<Texture>
            ("Texture",
             "new", sol::no_constructor,
             "create", sol::factories([](){ return Texture::create(); }),
             "type", &Texture::type,
             "source", &Texture::source,
             "rect", &Texture::rect);
}

}

#endif // TEXTURE_BIND_H
