#ifndef STATICTEXTURE_BIND_H
#define STATICTEXTURE_BIND_H

#include <un/graphics/statictexture.h>
#include <un/utils/log.h>

#include <sol/sol.hpp>

namespace un {

inline void luaStaticTextureBind(sol::state *state)
{
    // un namespace
    sol::table unTable = state->get<sol::table>("un");
    unTable.new_usertype<StaticTexture>
            ("StaticTexture",
             "new", sol::no_constructor,
             "create",
             sol::factories([](sf::Texture *source, const sf::IntRect &rect){
                 return StaticTexture::create(source, rect);
             }),
            "type", &StaticTexture::type,
            "source", &StaticTexture::source,
            "rect", &StaticTexture::rect);

}

}

#endif // STATICTEXTURE_BIND_H
