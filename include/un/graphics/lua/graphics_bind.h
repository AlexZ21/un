#ifndef GRAPHICS_BIND_H
#define GRAPHICS_BIND_H

#include "texture_bind.h"
#include "statictexture_bind.h"
#include "animatedtexture_bind.h"

#include <sol/sol.hpp>

namespace un {

inline void luaGraphicsBind(sol::state *state)
{
    luaTextureBind(state);
    luaStaticTextureBind(state);
    luaAnimatedTextureBind(state);
}

}

#endif // GRAPHICS_BIND_H
