#ifndef ANIMATEDTEXTURE_BIND_H
#define ANIMATEDTEXTURE_BIND_H

#include <un/graphics/animatedtexture.h>
#include <un/utils/log.h>

#include <sol/sol.hpp>

namespace un {

inline void luaAnimatedTextureBind(sol::state *state)
{
    // un namespace
    sol::table unTable = state->get<sol::table>("un");
    unTable.new_usertype<AnimatedTexture>
            ("AnimatedTexture",
             "new", sol::no_constructor,
             "create", sol::factories([](sf::Texture *source,
                                      bool looped,
                                      const sf::Time &frameTime,
                                      const std::vector<sf::IntRect> &frames){
                 return AnimatedTexture::create(source, looped, frameTime, frames);
             }),
            "frameRect", &AnimatedTexture::frameRect,
            "frameCount", &AnimatedTexture::frameCount,
            "frameTime", &AnimatedTexture::frameTime,
            "isLooped", &AnimatedTexture::isLooped,
            "type", &AnimatedTexture::type,
            "source", &AnimatedTexture::source,
            "rect", &AnimatedTexture::rect);

}

}

#endif // ANIMATEDTEXTURE_BIND_H
