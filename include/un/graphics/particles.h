#ifndef UN_PARTICLES_H
#define UN_PARTICLES_H

#include <un/un_export.h>
#include <un/un_defines.h>
#include <un/graphics/drawable.h>
#include <un/core/random.h>

#include <SFML/Graphics.hpp>

#include <vector>
#include <array>
#include <functional>

namespace un {

class Texture;
class AnimatedTextureState;
class Particle;
class Particles;
class ParticlesManager;

class UN_EXPORT ParticleEmitter
{
public:
    ParticleEmitter();
    ~ParticleEmitter() = default;

public:
    float emissionRate;
    float emissionDifference;

    Distribution<sf::Time> particleLifetime;

    Distribution<sf::Vector2f> particlePosition;
    Distribution<float> particleVelocity;
    Distribution<float> particleDirection;

    Distribution<float> particleRotation;
    Distribution<float> particleRotationSpeed;

    Distribution<float> particleScale;
    Distribution<float> particleScaleSpeed;

    Distribution<float> particleOpacity;
    Distribution<float> particleOpacitySpeed;

    Distribution<sf::Color> particleColor;

    Distribution<uint32_t> particleTextureIndex;
};

class UN_EXPORT Particle
{
public:
    Particle(const sf::Time &lifetime);
    ~Particle() = default;

public:
    sf::Time passed;
    sf::Time lifetime;

    sf::Vector2f position;
    float velocity;
    float direction;

    float rotation;
    float rotationSpeed;

    float scale;
    float scaleSpeed;

    float opacity;
    float opacitySpeed;

    sf::Color color;

    uint32_t textureIndex;
};

class UN_EXPORT Particles : public Drawable
{
    friend class ParticlesManager;
public:
    Particles();
    Particles(Texture *texture);
    ~Particles() = default;

    Texture *texture() const;
    void setTexture(Texture *texture);

    AnimatedTextureState *animatedTextureState() const { return nullptr; }

    void addTextureRect(const sf::IntRect &rect);
    void removeTextureRect(uint32_t index);

    void addEmitter(const ParticleEmitter &emitter);
    void clearEmitters();

    void update(const sf::Transform &transform);

    virtual sf::FloatRect localBounds() const;
    virtual sf::FloatRect globalBounds() const;

    void draw(sf::RenderTarget &target, sf::RenderStates state) const;

private:
    void updateParticle(Particle &particle, uint32_t msec);
    void updateVertices();
    void updateQuads();
    void updateQuad(std::array<sf::Vertex, 4> &quad, const sf::IntRect &textureRect);

private:
    Texture *m_texture;
    uuptr<AnimatedTextureState> m_state;
    std::vector<sf::IntRect> m_textureRects;

    std::vector<Particle> m_particles;
    std::vector<ParticleEmitter> m_emitters;

    sf::VertexArray m_vertices;
    std::vector<std::array<sf::Vertex, 4>> m_quads;

    sf::Clock m_updateClock;
};

}

#endif // UN_PARTICLES_H
