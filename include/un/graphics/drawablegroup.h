#ifndef UN_DRAWABLEGROUP_H
#define UN_DRAWABLEGROUP_H

#include <un/un_export.h>
#include <un/un_defines.h>

#include <SFML/Graphics/Rect.hpp>
#include <SFML/Graphics/Drawable.hpp>

#include <unordered_map>
#include <vector>

namespace un {

class Drawable;

class UN_EXPORT DrawableGroup
{
public:
    DrawableGroup() = default;
    ~DrawableGroup() = default;

    void add(un::Drawable *drawable);
    void add(const std::vector<Drawable *> &drawables);
    void add(uint32_t state, un::Drawable *drawable);
    void add(uint32_t state, const std::vector<Drawable *> &drawables);

    const std::vector<un::Drawable *> *drawables(uint32_t state) const;

    sf::FloatRect localBounds(uint32_t state = 0) const;
    sf::FloatRect globalBounds(uint32_t state = 0, sf::Transform transform = sf::Transform()) const;

    void draw(uint32_t state, sf::RenderTarget &target, sf::RenderStates states) const;

private:
    std::unordered_map<uint32_t, std::vector<Drawable *>> m_stateDrawables;
    std::vector<uuptr<un::Drawable>> m_drawables;

};

}

#endif // UN_DRAWABLEGROUP_H
