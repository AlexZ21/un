#ifndef UN_STATICTEXTURE_H
#define UN_STATICTEXTURE_H

#include <un/un_export.h>
#include <un/un_defines.h>
#include <un/graphics/texture.h>
#include <un/utils/ptr.h>

namespace un {

class UN_EXPORT StaticTexture : public Texture
{
public:
    UN_INHERIT_PTRS(StaticTexture)

    StaticTexture(sf::Texture *source, const sf::IntRect &rect = sf::IntRect())  :
        m_source(source),
        m_rect(rect) {}
    ~StaticTexture() = default;

    Type type() const { return Static; }
    sf::Texture *source() const { return m_source; }
    sf::IntRect rect() const { return m_rect; }

private:
    sf::Texture *m_source;
    sf::IntRect m_rect;

};

}

#endif // UN_STATICTEXTURE_H
