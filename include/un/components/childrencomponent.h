﻿#ifndef UN_CHILDRENCOMPONENT_H
#define UN_CHILDRENCOMPONENT_H

#include <un/un_export.h>
#include <un/un_defines.h>
#include <un/core/component.h>
#include <un/core/entity.h>

#include <vector>

namespace un {

class ChildrenSystem;

class UN_EXPORT ChildrenComponent : public Component
{
    friend class ChildrenSystem;
public:
    ChildrenComponent();
    ~ChildrenComponent() = default;

    void addChild(const Entity &entity);
    void removeChild(const Entity &entity);
    void removeChildren();   
    const std::vector<Entity> &children() const;

private:
    std::vector<Entity> m_children;
    std::vector<Entity> m_achildren;
    std::vector<Entity> m_rchildren;

};

}

#endif // UN_CHILDRENCOMPONENT_H
