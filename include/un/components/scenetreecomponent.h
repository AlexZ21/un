#ifndef UN_SCENETREECOMPONENT_H
#define UN_SCENETREECOMPONENT_H

#include <un/un_export.h>
#include <un/un_defines.h>
#include <un/core/component.h>
#include <un/core/entity.h>
#include <un/utils/quadtreev2.h>

#include <SFML/System/Vector2.hpp>

namespace un {

class SceneTreeSystem;

class UN_EXPORT SceneTreeComponent : public Component
{
    friend class SceneTreeSystem;
public:
    SceneTreeComponent();
    ~SceneTreeComponent() = default;

private:
    int32_t m_proxyId;
    sf::Vector2f m_proxyPosition;

};

}

#endif // UN_SCENETREECOMPONENT_H
