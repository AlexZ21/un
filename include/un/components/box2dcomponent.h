#ifndef BOX2DCOMPONENT_H
#define BOX2DCOMPONENT_H

#include <un/un_export.h>
#include <un/un_defines.h>
#include <un/core/component.h>

#include <Box2D/Box2D.h>
#include <SFML/System/Vector2.hpp>

#define SCALE 30.f
#define DEG 57.29577f

namespace un {

class Box2dSystem;

class UN_EXPORT Box2dComponent : public Component
{
    friend class Box2dSystem;
public:
    enum BodyType {
        Static,
        Dynamic
    };

    Box2dComponent();
    ~Box2dComponent() = default;

    b2Body *body() const;
    void createBody(std::vector<sf::Vector2f> vertices,
                    BodyType bodyType = Dynamic,
                    sf::Vector2f center = sf::Vector2f());

    sf::Vector2f position() const;
    void setPosition(float x, float y);
    void setPosition(const sf::Vector2f &position);

    float rotation() const;
    void setRotation(float angle);

    void move(float x, float y);
    void move(const sf::Vector2f &position);

    void rotate(float angle);

    bool isFixedRotation() const;
    void setFixedRotation(bool fixed);

    sf::Vector2f worldCenter() const;

    sf::Vector2f linearVelocity() const;
    void setLinearVelocity(float x, float y);
    void setLinearVelocity(const sf::Vector2f &velocity);

private:
    b2World *m_world;
    b2Body *m_body;

};

}

#endif // BOX2DCOMPONENT_H
