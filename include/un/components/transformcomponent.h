#ifndef UN_TRANSFORMCOMPONENT_H
#define UN_TRANSFORMCOMPONENT_H

#include <un/un_export.h>
#include <un/un_defines.h>
#include <un/core/component.h>

#include <SFML/Graphics/Transform.hpp>

namespace un {

class TransformSystem;

class UN_EXPORT TransformComponent : public Component
{
    friend class TransformSystem;
public:
    TransformComponent();
    ~TransformComponent() = default;

    //! Position
    sf::Vector2f position() const;
    void setPosition(float x, float y);
    void setPosition(const sf::Vector2f &position);

    //! Roatation
    float rotation() const;
    void setRotation(float angle);

    //! Scale
    sf::Vector2f scale() const;
    void setScale(float factorX, float factorY);
    void setScale(const sf::Vector2f &factors);

    //! Origin
    sf::Vector2f origin() const;
    void setOrigin(float x, float y);
    void setOrigin(const sf::Vector2f &origin);

    //! Z index
    float zIndex() const;
    void setZIndex(float zindex);

    //! Actions
    void move(float offsetX, float offsetY);
    void move(const sf::Vector2f &offset);

    void rotate(float angle);

    void scale(float factorX, float factorY);
    void scale(const sf::Vector2f &factor);

    //! Transform
    const sf::Transform &parentTransform() const;
    void setParentTransform(const sf::Transform &transform);
    void unsetParentTransform();

    const sf::Transform &transform() const;
    const sf::Transform &inverseTransform() const;
    void setTransform(const sf::Transform &transform);

    //! If transformed
    bool isChanged() const;

private:
    sf::Vector2f m_position;
    float m_rotation;
    sf::Vector2f m_scale;
    sf::Vector2f m_origin;
    float m_zindex;

    bool m_changed;

    mutable sf::Transform m_parentTransform;
    mutable bool m_multiplyWithParent;

    mutable sf::Transform m_transform;
    mutable bool m_transformNeedUpdate;

    mutable sf::Transform m_inverseTransform;
    mutable bool m_inverseTransformNeedUpdate;

};

}

#endif // UN_TRANSFORMCOMPONENT_H
