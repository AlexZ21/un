#ifndef SCENECOMPONENT_H
#define SCENECOMPONENT_H

#include <un/un_export.h>
#include <un/un_defines.h>
#include <un/core/component.h>

namespace un {

class Scene;

class UN_EXPORT SceneComponent : public Component
{
public:
    SceneComponent(Scene *scene);
    ~SceneComponent() = default;

    Scene *scene() const;

private:
    Scene *m_scene;

};

}

#endif // SCENECOMPONENT_H
