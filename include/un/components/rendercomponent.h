#ifndef UN_RENDERCOMPONENT_H
#define UN_RENDERCOMPONENT_H

#include <un/un_export.h>
#include <un/un_defines.h>
#include <un/core/component.h>
#include <un/graphics/drawable.h>
#include <un/graphics/drawablegroup.h>

namespace un {

class UN_EXPORT RenderComponent : public Component
{
public:
    RenderComponent();
    ~RenderComponent() = default;

    void setDrawableGroup(DrawableGroup *drawableGroup);
    void unsetDrawableGroup();

    DrawableGroup *drawableGroup() const;

    uint32_t state() const;
    void setState(uint32_t state);

    //! For animated textures
    void play();
    void pause();
    void stop();

private:
    uuptr<DrawableGroup> m_drawableGroup;
    uint32_t m_state;

};

}

#endif // UN_RENDERCOMPONENT_H
