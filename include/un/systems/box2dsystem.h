#ifndef BOX2DSYSTEM_H
#define BOX2DSYSTEM_H

#include <un/un_export.h>
#include <un/un_defines.h>
#include <un/core/scenesystem.h>
#include <un/components/box2dcomponent.h>
#include <un/components/transformcomponent.h>

#include <Box2D/Box2D.h>

namespace un {

class UN_EXPORT Box2dSystem: public SceneSystem<Filter::Requires<Box2dComponent, TransformComponent>>
{
public:
    Box2dSystem();
    ~Box2dSystem() = default;

    b2World *world();

    utils::TypeId typeId() {
        return utils::ClassTypeId<BaseSystem>::id<Box2dSystem>();
    }

    void update(sf::Time);

protected:
    void onEntityAdded(const Entity &entity);
    void onEntityRemoved(const Entity &entity);

private:
    b2World m_world;

};

}

#endif // BOX2DSYSTEM_H
