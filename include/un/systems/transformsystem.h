#ifndef TRANSFORMSYSTEM_H
#define TRANSFORMSYSTEM_H

#include <un/un_export.h>
#include <un/un_defines.h>
#include <un/core/scenesystem.h>
#include <un/components/transformcomponent.h>

namespace un {

class UN_EXPORT TransformSystem : public SceneSystem<Filter::Requires<TransformComponent>>
{
public:
    TransformSystem();
    ~TransformSystem() = default;

    utils::TypeId typeId() {
        return utils::ClassTypeId<BaseSystem>::id<TransformSystem>();
    }

    void update(sf::Time elapsed);

};

}

#endif // TRANSFORMSYSTEM_H
