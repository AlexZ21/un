#ifndef SCENETREESYSTEM_H
#define SCENETREESYSTEM_H

#include <un/un_export.h>
#include <un/un_defines.h>
#include <un/core/scenesystem.h>
#include <un/components/scenetreecomponent.h>
#include <un/components/transformcomponent.h>
#include <un/components/rendercomponent.h>
#include <un/utils/quadtreev2.h>

#include <SFML/Graphics/Rect.hpp>

namespace un {

class UN_EXPORT SceneTreeSystem : public SceneSystem<Filter::Requires<SceneTreeComponent,
        TransformComponent, RenderComponent>>
{
public:
    SceneTreeSystem() = default;
    ~SceneTreeSystem() = default;

    template <typename Func>
    void query(const sf::FloatRect &aabb, Func &&callback) {
        m_tree.query(aabb, callback);
    }

    utils::TypeId typeId() {
        return utils::ClassTypeId<BaseSystem>::id<SceneTreeSystem>();
    }

    void update(sf::Time);

protected:
    void onEntityAdded(const Entity &entity);
    void onEntityRemoved(const Entity &entity);

private:
    utils::QuadTreeV2<Entity> m_tree;

};

}

#endif // SCENETREESYSTEM_H
