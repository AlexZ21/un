#ifndef UN_RENDERSYSTEM_H
#define UN_RENDERSYSTEM_H

#include <un/un_export.h>
#include <un/un_defines.h>
#include <un/core/scenesystem.h>
#include <un/components/rendercomponent.h>

#include <SFML/Graphics/RenderTarget.hpp>

namespace un {

class UN_EXPORT RenderSystem : public SceneSystem<Filter::Requires<RenderComponent>>
{
public:
    RenderSystem();
    ~RenderSystem() = default;

    utils::TypeId typeId() {
        return utils::ClassTypeId<BaseSystem>::id<RenderSystem>();
    }

    void update(sf::Time);
    void draw(sf::RenderTarget &target, sf::RenderStates states, sf::Time);

};

}

#endif // UN_RENDERSYSTEM_H
