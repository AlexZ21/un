#ifndef CHILDRENSYSTEM_H
#define CHILDRENSYSTEM_H

#include <un/un_export.h>
#include <un/un_defines.h>
#include <un/core/scenesystem.h>
#include <un/components/childrencomponent.h>
#include <un/components/transformcomponent.h>

namespace un {

class UN_EXPORT ChildrenSystem : public SceneSystem<Filter::Requires<ChildrenComponent,
        TransformComponent>>
{
public:
    ChildrenSystem() = default;
    ~ChildrenSystem() = default;

    utils::TypeId typeId() {
        return utils::ClassTypeId<BaseSystem>::id<ChildrenSystem>();
    }

    void update(sf::Time);

private:
    void updateParentTransform(const Entity &entity);
};

}

#endif // CHILDRENSYSTEM_H
