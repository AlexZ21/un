#ifndef UN_QUADTREEV2_H
#define UN_QUADTREEV2_H

#include <un/un_export.h>
#include <un/un_defines.h>

#include <SFML/Graphics.hpp>

#include <vector>

namespace un {
namespace utils {

class AABB
{
public:
    AABB() = default;
    ~AABB() = default;

    void fromRect(const sf::FloatRect &rect) {
        lowerBound.x = rect.left;
        lowerBound.y = rect.top;
        upperBound.x = lowerBound.x + rect.width;
        upperBound.y = lowerBound.y + rect.height;
    }

    float perimeter() const {
        float wx = upperBound.x - lowerBound.x;
        float wy = upperBound.y - lowerBound.y;
        return 2.0f * (wx + wy);
    }

    void combine(const AABB& aabb) {
        lowerBound = sf::Vector2f(std::min(lowerBound.x, aabb.lowerBound.x),
                                  std::min(lowerBound.y, aabb.lowerBound.y));
        upperBound = sf::Vector2f(std::max(upperBound.x, aabb.upperBound.x),
                                  std::max(upperBound.y, aabb.upperBound.y));
    }

    void combine(const AABB &aabb1, const AABB &aabb2) {
        lowerBound = sf::Vector2f(std::min(aabb1.lowerBound.x, aabb2.lowerBound.x),
                                  std::min(aabb1.lowerBound.y, aabb2.lowerBound.y));
        upperBound = sf::Vector2f(std::max(aabb1.upperBound.x, aabb2.upperBound.x),
                                  std::max(aabb1.upperBound.y, aabb2.upperBound.y));
    }

    bool contains(const AABB& aabb) const {
        bool result = true;
        result = result && lowerBound.x <= aabb.lowerBound.x;
        result = result && lowerBound.y <= aabb.lowerBound.y;
        result = result && aabb.upperBound.x <= upperBound.x;
        result = result && aabb.upperBound.y <= upperBound.y;
        return result;
    }

    static bool isOverlap(const AABB &a, const AABB &b)
    {
        sf::Vector2f d1;
        sf::Vector2f d2;
        d1 = b.lowerBound - a.upperBound;
        d2 = a.lowerBound - b.upperBound;
        if (d1.x > 0.0f || d1.y > 0.0f)
            return false;
        if (d2.x > 0.0f || d2.y > 0.0f)
            return false;
        return true;
    }

    sf::Vector2f lowerBound;	///< the lower vertex
    sf::Vector2f upperBound;	///< the upper vertex
};

const uint32_t NullNode = -1;

template <typename T>
class UN_EXPORT QuadTreeV2
{
public:
    struct Node
    {
        bool isLeaf() const { return child1 == NullNode; }

        AABB aabb;
        T userData;

        int32_t parent;
        int32_t child1;
        int32_t child2;
        int32_t height;
    };

    QuadTreeV2() : m_root(NullNode) {

    }
    ~QuadTreeV2() = default;

    int32_t createProxy(const sf::FloatRect &aabb, T userData) {
        int32_t proxyId = createNode();

        m_nodes[proxyId].aabb.fromRect(aabb);
        m_nodes[proxyId].userData = userData;
        m_nodes[proxyId].height = 0;

        // Fatten the aabb.
        const float extension = 0.1f;
        sf::Vector2f r(extension, extension);
        m_nodes[proxyId].aabb.lowerBound -= r;
        m_nodes[proxyId].aabb.upperBound += r;

        insertLeaf(proxyId);

        return proxyId;
    }

    void destroyProxy(int32_t proxyId) {
        removeLeaf(proxyId);
        destroyNode(proxyId);
    }

    bool moveProxy(int32_t proxyId, const sf::FloatRect &aabb, const sf::Vector2f &position) {
        AABB aabb1;
        aabb1.fromRect(aabb);
        if (m_nodes[proxyId].aabb.contains(aabb1))
            return false;

        removeLeaf(proxyId);

        // Extend AABB.
        AABB b = aabb1;
        const float extension = 0.1f;
        sf::Vector2f r(extension, extension);
        b.lowerBound -= r;
        b.upperBound += r;

        // Predict AABB displacement.
        const float multiplier = 2.0f;
        sf::Vector2f d = multiplier * position;

        if (d.x < 0.0f)
            b.lowerBound.x += d.x;
        else
            b.upperBound.x += d.x;

        if (d.y < 0.0f)
            b.lowerBound.y += d.y;
        else
            b.upperBound.y += d.y;

        m_nodes[proxyId].aabb = b;

        insertLeaf(proxyId);

        return true;
    }

    template <typename Func>
    void query(const sf::FloatRect &aabb, Func &&callback) {
        AABB aabb1;
        aabb1.fromRect(aabb);
        queryHelper(m_root, aabb1, callback);
    }

private:
    template <typename Func>
    void queryHelper(int32_t nodeId, const AABB &aabb, Func &&callback) {
        const Node *n = node(nodeId);
        if (n) {
            if (AABB::isOverlap(aabb, n->aabb)) {
                if (n->isLeaf()) {
                    callback(nodeId, n->userData);
                } else {
                    queryHelper(n->child1, aabb, callback);
                    queryHelper(n->child2, aabb, callback);
                }
            }
        }
    }


    Node *node(int32_t nodeId) {
        if (nodeId >= m_nodes.size())
            return nullptr;
        return &m_nodes[nodeId];
    }

    int32_t createNode() {
        if (!m_destNodes.empty()) {
            int32_t nodeId = m_destNodes.front();
            m_destNodes.erase(m_destNodes.begin());
            return nodeId;
        }
        Node node;
        node.parent = NullNode;
        node.child1 = NullNode;
        node.child2 = NullNode;
        node.height = 0;
//        node.userData = nullptr;
        m_nodes.push_back(node);
        return m_nodes.size() - 1;
    }

    void destroyNode(int32_t nodeId) {
        Node &node = m_nodes[nodeId];
        node.parent = NullNode;
        node.child1 = NullNode;
        node.child2 = NullNode;
        node.height = -1;
//        node.userData = nullptr;
        m_destNodes.push_back(nodeId);
    }

    void insertLeaf(int32_t leaf) {
        if (m_root == NullNode) {
            m_root = leaf;
            m_nodes[m_root].parent = NullNode;
            return;
        }

        // Find the best sibling for this node
        AABB leafAABB = m_nodes[leaf].aabb;
        int32_t index = m_root;
        while (!m_nodes[index].isLeaf()) {
            int32_t child1 = m_nodes[index].child1;
            int32_t child2 = m_nodes[index].child2;

            float area = m_nodes[index].aabb.perimeter();

            AABB combinedAABB;
            combinedAABB.combine(m_nodes[index].aabb, leafAABB);
            float combinedArea = combinedAABB.perimeter();

            // Cost of creating a new parent for this node and the new leaf
            float cost = 2.0f * combinedArea;
            // Minimum cost of pushing the leaf further down the tree
            float inheritanceCost = 2.0f * (combinedArea - area);

            // Cost of descending into child1
            float cost1;
            if (m_nodes[child1].isLeaf()) {
                AABB aabb;
                aabb.combine(leafAABB, m_nodes[child1].aabb);
                cost1 = aabb.perimeter() + inheritanceCost;
            } else {
                AABB aabb;
                aabb.combine(leafAABB, m_nodes[child1].aabb);
                float oldArea = m_nodes[child1].aabb.perimeter();
                float newArea = aabb.perimeter();
                cost1 = (newArea - oldArea) + inheritanceCost;
            }

            // Cost of descending into child2
            float cost2;
            if (m_nodes[child2].isLeaf()) {
                AABB aabb;
                aabb.combine(leafAABB, m_nodes[child2].aabb);
                cost2 = aabb.perimeter() + inheritanceCost;
            } else {
                AABB aabb;
                aabb.combine(leafAABB, m_nodes[child2].aabb);
                float oldArea = m_nodes[child2].aabb.perimeter();
                float newArea = aabb.perimeter();
                cost2 = newArea - oldArea + inheritanceCost;
            }

            // Descend according to the minimum cost.
            if (cost < cost1 && cost < cost2)
                break;

            // Descend
            index = cost1 < cost2 ? child1 : child2;
        }

        int32_t sibling = index;

        // Create a new parent.
        int32_t oldParent = m_nodes[sibling].parent;
        int32_t newParent = createNode();
        m_nodes[newParent].parent = oldParent;
//        m_nodes[newParent].userData = nullptr;
        m_nodes[newParent].aabb.combine(leafAABB, m_nodes[sibling].aabb);
        m_nodes[newParent].height = m_nodes[sibling].height + 1;

        if (oldParent != NullNode) {
            // The sibling was not the root.
            if (m_nodes[oldParent].child1 == sibling)
                m_nodes[oldParent].child1 = newParent;
            else
                m_nodes[oldParent].child2 = newParent;

            m_nodes[newParent].child1 = sibling;
            m_nodes[newParent].child2 = leaf;
            m_nodes[sibling].parent = newParent;
            m_nodes[leaf].parent = newParent;
        } else {
            // The sibling was the root.
            m_nodes[newParent].child1 = sibling;
            m_nodes[newParent].child2 = leaf;
            m_nodes[sibling].parent = newParent;
            m_nodes[leaf].parent = newParent;
            m_root = newParent;
        }

        // Walk back up the tree fixing heights and AABBs
        index = m_nodes[leaf].parent;
        while (index != NullNode) {
            index = balance(index);

            int32_t child1 = m_nodes[index].child1;
            int32_t child2 = m_nodes[index].child2;

            m_nodes[index].height = 1 + std::max(m_nodes[child1].height, m_nodes[child2].height);
            m_nodes[index].aabb.combine(m_nodes[child1].aabb, m_nodes[child2].aabb);

            index = m_nodes[index].parent;
        }

    }

    void removeLeaf(int32_t leaf) {
        if (leaf == m_root) {
            m_root = NullNode;
            return;
        }

        int32_t parent = m_nodes[leaf].parent;
        int32_t grandParent = m_nodes[parent].parent;
        int32_t sibling = m_nodes[parent].child1 == leaf ? m_nodes[parent].child2 : m_nodes[parent].child1;

        if (grandParent != NullNode) {
            // Destroy parent and connect sibling to grandParent.
            if (m_nodes[grandParent].child1 == parent)
                m_nodes[grandParent].child1 = sibling;
            else
                m_nodes[grandParent].child2 = sibling;

            m_nodes[sibling].parent = grandParent;
            destroyNode(parent);

            // Adjust ancestor bounds.
            int32_t index = grandParent;
            while (index != NullNode) {
                index = balance(index);

                int32_t child1 = m_nodes[index].child1;
                int32_t child2 = m_nodes[index].child2;

                m_nodes[index].aabb.combine(m_nodes[child1].aabb, m_nodes[child2].aabb);
                m_nodes[index].height = 1 + std::max(m_nodes[child1].height, m_nodes[child2].height);

                index = m_nodes[index].parent;
            }
        } else {
            m_root = sibling;
            m_nodes[sibling].parent = NullNode;
            destroyNode(parent);
        }
    }

    int32_t balance(int32_t iA) {
        Node *A = node(iA);
        if (A->isLeaf() || A->height < 2)
            return iA;

        int32_t iB = A->child1;
        int32_t iC = A->child2;

        Node *B = node(iB);
        Node *C = node(iC);

        int32_t balance = C->height - B->height;

        // Rotate C up
        if (balance > 1) {
            int32_t iF = C->child1;
            int32_t iG = C->child2;
            Node *F = node(iF);
            Node *G = node(iG);

            // Swap A and C
            C->child1 = iA;
            C->parent = A->parent;
            A->parent = iC;

            // A's old parent should point to C
            if (C->parent != NullNode) {
                if (m_nodes[C->parent].child1 == iA)
                    m_nodes[C->parent].child1 = iC;
                else
                    m_nodes[C->parent].child2 = iC;
            } else {
                m_root = iC;
            }

            // Rotate
            if (F->height > G->height) {
                C->child2 = iF;
                A->child2 = iG;
                G->parent = iA;
                A->aabb.combine(B->aabb, G->aabb);
                C->aabb.combine(A->aabb, F->aabb);
                A->height = 1 + std::max(B->height, G->height);
                C->height = 1 + std::max(A->height, F->height);
            } else {
                C->child2 = iG;
                A->child2 = iF;
                F->parent = iA;
                A->aabb.combine(B->aabb, F->aabb);
                C->aabb.combine(A->aabb, G->aabb);
                A->height = 1 + std::max(B->height, F->height);
                C->height = 1 + std::max(A->height, G->height);
            }

            return iC;
        }

        // Rotate B up
        if (balance < -1)
        {
            int32_t iD = B->child1;
            int32_t iE = B->child2;
            Node *D = node(iD);
            Node *E = node(iE);

            // Swap A and B
            B->child1 = iA;
            B->parent = A->parent;
            A->parent = iB;

            // A's old parent should point to B
            if (B->parent != NullNode) {
                if (m_nodes[B->parent].child1 == iA)
                    m_nodes[B->parent].child1 = iB;
                else
                    m_nodes[B->parent].child2 = iB;
            } else {
                m_root = iB;
            }

            // Rotate
            if (D->height > E->height) {
                B->child2 = iD;
                A->child1 = iE;
                E->parent = iA;
                A->aabb.combine(C->aabb, E->aabb);
                B->aabb.combine(A->aabb, D->aabb);
                A->height = 1 + std::max(C->height, E->height);
                B->height = 1 + std::max(A->height, D->height);
            } else {
                B->child2 = iE;
                A->child1 = iD;
                D->parent = iA;
                A->aabb.combine(C->aabb, D->aabb);
                B->aabb.combine(A->aabb, E->aabb);
                A->height = 1 + std::max(C->height, D->height);
                B->height = 1 + std::max(A->height, E->height);
            }

            return iB;
        }

        return iA;
    }

private:
    uint32_t m_root;
    std::vector<Node> m_nodes;
    std::vector<int32_t> m_destNodes;

};

}
}

#endif // UN_QUADTREEV2_H
