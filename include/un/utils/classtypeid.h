#ifndef UN_CLASSTYPEID_H
#define UN_CLASSTYPEID_H

#include <un/un_export.h>
#include <un/un_defines.h>

#include <atomic>

namespace un {
namespace utils {

using TypeId = size_t;

template <typename T>
class UN_EXPORT ClassTypeId
{
public:
    template <typename U>
    static TypeId id() {
        static const TypeId id = m_next++;
        return id;
    }

private:
    static std::atomic<TypeId> m_next;

};

template <typename T>
std::atomic<TypeId> ClassTypeId<T>::m_next{0};

}
}

#endif // UN_CLASSTYPEID_H
