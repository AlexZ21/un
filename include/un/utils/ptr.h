#ifndef PTR_H
#define PTR_H

#include <un/un_export.h>
#include <un/un_defines.h>
#include <un/utils/log.h>

#include <stdint.h>
#include <vector>
#include <functional>
#include <memory>
#include <atomic>
#include <type_traits>

namespace un {
namespace utils {

class PtrStorage;

template<typename T>
class UN_EXPORT Ptr
{
    friend class PtrStorage;
public:
    Ptr() : m_id(-1), m_p(nullptr) {}
    Ptr(int32_t id, T *p);
    Ptr(const Ptr<T> &other);
    template<typename U>
    Ptr(const Ptr<U> &other);
    ~Ptr();

    int32_t id() const { return m_id; }

    void destroy();
    bool isValid() const;

    T *get() const;
    T *operator->() const;

    Ptr<T> &operator=(const Ptr<T> &other);
    template<typename U>
    Ptr<T> &operator=(const Ptr<U> &other);

    static Ptr<T> create(T *v);

    template<typename... Args>
    static Ptr<T> create(Args&&... args);

private:
    int32_t m_id;
    T *m_p;
};

class UN_EXPORT PtrStorage
{
private:
    struct Memory {
        template<typename T, typename... Args>
        static void *allocate(Args&&... args) {
            void *p = operator new(sizeof(T));
            if (p) new (p) T(std::forward<Args>(args)...);
            return p;
        }
        template<typename T>
        static void destroy(T *ptr) {
            ptr->~T();
            ::operator delete(ptr);
        }
    };

    struct Type {
        virtual ~Type() = default;
        virtual void destroy(void *) {}
    };

    template<typename T>
    struct TType : public Type {
        ~TType() = default;
        void destroy(void *ptr) {
            if (ptr) Memory::destroy(reinterpret_cast<T *>(ptr));
        }
    };

    struct Data {
        Data() : type(nullptr), ptr(nullptr), counter(0) {}
        Data(const Data &other) {
            type = other.type;
            ptr = other.ptr;
            counter.store(other.counter.load());
        }
        ~Data() { destroy(); }
        void destroy() {
            if (type) type->destroy(ptr);
            type = nullptr;
            ptr = nullptr;
            counter.store(0);
        }

        Type *type;
        void *ptr;
        std::atomic_int32_t counter;
    };

    struct Id {
        int32_t get() {
            if (m_freeids.size() > 5) {
                int32_t id = m_freeids.front();
                m_freeids.erase(m_freeids.begin());
                return id;
            }
            return m_nextid++;
        }
        void free(int32_t id) {
            m_freeids.push_back(id);
        }
        template<typename T>
        int32_t type() {
            static const int32_t id = m_nexttypeid++;
            return id;
        }

        int32_t m_nextid = 0;
        std::vector<int32_t> m_freeids;
        int32_t m_nexttypeid = 0;
    };

public:
    static PtrStorage &instance() { static PtrStorage storage; return storage; }

    template<typename T>
    static Ptr<T> create(T *v) {
        PtrStorage &i = instance();

        int32_t typeId = i.m_id.type<T>();
        if (i.m_types.size() <= typeId) {
            i.m_types.resize(typeId + 1);
            i.m_types[typeId].reset(new TType<T>());
        }

        int32_t id = i.m_id.get();
        Data &data = i.m_data[id];
        data.type = i.m_types[typeId].get();
        data.ptr = v;

        Ptr<T> p(id, v);
        return p;
    }

    template<typename T, typename... Args>
    static Ptr<T> create(Args&&... args) {
        return create(reinterpret_cast<T*>(Memory::allocate<T>(std::forward<Args>(args)...)));
    }

    static void destroy(int32_t id) {
        if (isValid(id)) {
            instance().m_data[id].destroy();
            instance().m_id.free(id);
        }
    }

    static bool isValid(int32_t id) {
        PtrStorage &i = instance();
        return id >= 0 && id < instance().m_data.size() && instance().m_data[id].ptr != nullptr;
    }

    template<typename T>
    static Ptr<T> get(int32_t id) {
        return isValid(id) ? Ptr<T>(id, static_cast<T*>(instance().m_data[id].ptr)) : Ptr<T>(-1, nullptr);
    }

    static void inc(int32_t id) {
        if (isValid(id))
            ++instance().m_data[id].counter;
    }

    static void dec(int32_t id) {
        if (isValid(id)) {
            if (!(--instance().m_data[id].counter))
                instance().m_data[id].destroy();
        }
    }

private:
    PtrStorage() {
        m_data.resize(10000);
    }
    ~PtrStorage() = default;

private:
    Id m_id;
    std::vector<std::unique_ptr<Type>> m_types;
    std::vector<Data> m_data;

};

template<typename T>
Ptr<T>::Ptr(int32_t id, T *p) : m_id(id), m_p(p)
{
    PtrStorage::inc(m_id);
}

template<typename T>
Ptr<T>::Ptr(const Ptr<T> &other)
{
    m_id = other.m_id;
    m_p = other.m_p;
    PtrStorage::inc(m_id);
}

template<typename T>
template<typename U>
Ptr<T>::Ptr(const Ptr<U> &other)
{
    if (std::is_convertible<U*, T*>::value || std::is_base_of<U, T>::value) {
        m_id = other.id();
        m_p = static_cast<T*>(other.get());
        PtrStorage::inc(m_id);
    } else {
        m_id = -1;
        m_p = nullptr;
        LERR("Can't convert <", typeid(U).name(), "> to <", typeid(T).name(), ">");
    }
}

template<typename T>
Ptr<T>::~Ptr()
{
    PtrStorage::dec(m_id);
}

template<typename T>
void Ptr<T>::destroy()
{
    PtrStorage::destroy(m_id);
}

template<typename T>
bool Ptr<T>::isValid() const
{
    return PtrStorage::isValid(m_id);
}

template<typename T>
T *Ptr<T>::get() const
{
    return PtrStorage::isValid(m_id) ? m_p : nullptr;
}

template<typename T>
T *Ptr<T>::operator->() const
{
    return get();
}

template<typename T>
Ptr<T> &Ptr<T>::operator=(const Ptr<T> &other)
{
    PtrStorage::dec(m_id);
    m_id = other.m_id;
    m_p = other.m_p;
    PtrStorage::inc(m_id);
    return *this;
}

template<typename T>
template<typename U>
Ptr<T> &Ptr<T>::operator=(const Ptr<U> &other)
{
    if (std::is_convertible<U*, T*>::value || std::is_base_of<U, T>::value) {
        PtrStorage::dec(m_id);
        m_id = other.id();
        m_p = static_cast<T*>(other.get());;
        PtrStorage::inc(m_id);
    } else {
        LERR("Can't convert <", typeid(U).name(), "> to <", typeid(T).name(), ">");
    }
    return *this;
}

template<typename T>
Ptr<T> Ptr<T>::create(T *v)
{
    return PtrStorage::create(v);
}

template<typename T>
template<typename... Args>
Ptr<T> Ptr<T>::create(Args&&... args)
{
    return PtrStorage::create<T>(std::forward<Args>(args)...);
}

}
}

#define UN_PTRS(__class__) \
    public: \
    using ptr = un::utils::Ptr<__class__>; \
    protected: \
    int32_t ptrid = -1; \
    public: \
    template<typename... Args> \
    static ptr create(Args&&... args) { \
    ptr p = ptr::create(std::forward<Args>(args)...); \
    p->ptrid = p.id(); \
    return p; \
    } \
    ptr toPtr() { \
    if (ptrid == -1) { \
    ptr p = ptr::create(this); \
    p->ptrid = p.id(); \
    return p; \
    } else { \
    return ptr(ptrid, this); \
    } \
    }

#define UN_INHERIT_PTRS(__class__) \
    public: \
    using ptr = un::utils::Ptr<__class__>; \
    public: \
    template<typename... Args> \
    static ptr create(Args&&... args) { \
    ptr p = ptr::create(std::forward<Args>(args)...); \
    p->ptrid = p.id(); \
    return p; \
    } \
    ptr toPtr() { \
    if (ptrid == -1) { \
    ptr p = ptr::create(this); \
    p->ptrid = p.id(); \
    return p; \
    } else { \
    return ptr(ptrid, this); \
    } \
    }


#endif // PTR_H
