#ifndef UN_LOG_H
#define UN_LOG_H

#include <un/un_export.h>
#include <un/un_defines.h>

#include <iostream>
#include <sstream>
#include <chrono>
#include <ctime>
#include <iomanip>

#define LENABLE             ::un::utils::Log::enable();
#define LDISABLE            ::un::utils::Log::disable();
#define LFILTER(_filter_)   ::un::utils::Log::setFilter(_filter_);
#define LDEBUG(...)         ::un::utils::Log::debug(__VA_ARGS__);
#define LSDEBUG(...)        ::un::utils::Log::debug_s(__VA_ARGS__);
#define LINFO(...)          ::un::utils::Log::info(__VA_ARGS__);
#define LSINFO(...)         ::un::utils::Log::info_s(__VA_ARGS__);
#define LWARN(...)          ::un::utils::Log::warning(__VA_ARGS__);
#define LSWARN(...)         ::un::utils::Log::warning_s(__VA_ARGS__);
#define LERR(...)           ::un::utils::Log::error(__VA_ARGS__);
#define LSERR(...)          ::un::utils::Log::error_s(__VA_ARGS__);
#define LFATAL(...)         ::un::utils::Log::fatal(__VA_ARGS__);
#define LSFATAL(...)        ::un::utils::Log::fatal_s(__VA_ARGS__);

#define LTOSTR(_expr_)      #_expr_

namespace un {
namespace utils {

class UN_EXPORT Log
{
public:
    enum Level {
        DEBUG = 0x0002,
        INFO = 0x0004,
        WARNING = 0x0008,
        ERROR = 0x00012,
        FATAL = 0x0016
    };

    template <typename... Args>
    static void debug(Args &&...args) {
        Log &log = instance();
        if (!log.m_enable || !(log.m_filter & DEBUG))
            return;
        log.push("DEBUG", false, std::forward<Args>(args)...);
    }

    template <typename... Args>
    static void debug_s(Args &&...args) {
        Log &log = instance();
        if (!log.m_enable || !(log.m_filter & DEBUG))
            return;
        log.push("DEBUG", true, std::forward<Args>(args)...);
    }

    template <typename... Args>
    static void info(Args &&...args) {
        Log &log = instance();
        if (!log.m_enable || !(log.m_filter & INFO))
            return;
        log.push("INFO", false, std::forward<Args>(args)...);
    }

    template <typename... Args>
    static void info_s(Args &&...args) {
        Log &log = instance();
        if (!log.m_enable || !(log.m_filter & INFO))
            return;
        log.push("INFO", true, std::forward<Args>(args)...);
    }

    template <typename... Args>
    static void warning(Args &&...args) {
        Log &log = instance();
        if (!log.m_enable || !(log.m_filter & WARNING))
            return;
        log.push("WARNING", false, std::forward<Args>(args)...);
    }

    template <typename... Args>
    static void warning_s(Args &&...args) {
        Log &log = instance();
        if (!log.m_enable || !(log.m_filter & WARNING))
            return;
        log.push("WARNING", true, std::forward<Args>(args)...);
    }

    template <typename... Args>
    static void error(Args &&...args) {
        Log &log = instance();
        if (!log.m_enable || !(log.m_filter & ERROR))
            return;
        log.push("ERROR", false, std::forward<Args>(args)...);
    }

    template <typename... Args>
    static void error_s(Args &&...args) {
        Log &log = instance();
        if (!log.m_enable || !(log.m_filter & ERROR))
            return;
        log.push("ERROR", true, std::forward<Args>(args)...);
    }

    template <typename... Args>
    static void fatal(Args &&...args) {
        Log &log = instance();
        if (!log.m_enable || !(log.m_filter & FATAL))
            return;
        log.push("FATAL", false, std::forward<Args>(args)...);
    }

    template <typename... Args>
    static void fatal_s(Args &&...args) {
        Log &log = instance();
        if (!log.m_enable || !(log.m_filter & FATAL))
            return;
        log.push("FATAL", true, std::forward<Args>(args)...);
    }

    static void enable() {
        instance().m_enable = true;
    }
    static void disable() {
        instance().m_enable = false;
    }
    static void setFilter(int filter) {
        instance().m_filter = filter;
    }

private:
    static Log &instance();

    Log() : m_enable(false), m_filter(DEBUG | INFO | WARNING | ERROR | FATAL) {}
    ~Log() = default;

    template <typename... Args>
    void push(const std::string &level, bool spacing, Args &&...args) {
        std::string s = spacing ? " " : "";
        m_stream << '[' << currentTime() << ']';
        m_stream << '[' << level << ']' << "\t ";
        using expander = int[];
        (void)expander{0, (void(m_stream << s << std::forward<Args>(args)), 0)...};
        print();
    }

    template <typename Arg>
    void push(std::stringstream &stream, Arg &&arg) {
        stream << std::forward<Arg>(arg);
    }

    std::string currentTime() const {
        std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
        std::time_t now_c = std::chrono::system_clock::to_time_t(now - std::chrono::hours(24));
        std::string currentTime = std::ctime(&now_c);
        currentTime.pop_back();
        return currentTime;
    }

    void print() {
        std::cout << m_stream.str() << std::endl;
        m_stream.str(std::string());
        m_stream.clear();
    }

private:
    std::stringstream m_stream;
    bool m_enable;
    int m_filter;

};

}
}

#endif // UN_LOG_H
