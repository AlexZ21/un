#ifndef BAYAZITDECOMPOSER_H
#define BAYAZITDECOMPOSER_H

#include <un/un_export.h>
#include <un/un_defines.h>

#include <SFML/System/Vector2.hpp>

#include <vector>

namespace un {
namespace utils {

class UN_EXPORT BayazitDecomposer
{
public:
    BayazitDecomposer();
    ~BayazitDecomposer() = default;

    static std::vector<std::vector<sf::Vector2f>> decompose(const std::vector<sf::Vector2f> &vertices);

private:
    static void copy(int32_t i, int32_t j, const std::vector<sf::Vector2f> &vertices,
                     std::vector<sf::Vector2f> &to);

    static sf::Vector2f at(int32_t i, const std::vector<sf::Vector2f> &vertices);

    static bool reflex(int32_t i, const std::vector<sf::Vector2f> &vertices);

    static bool left(const sf::Vector2f &a, const sf::Vector2f &b, const sf::Vector2f &c);
    static bool leftOn(const sf::Vector2f &a, const sf::Vector2f &b, const sf::Vector2f &c);

    static bool right(int32_t i, const std::vector<sf::Vector2f> &vertices);
    static bool right(const sf::Vector2f &a, const sf::Vector2f &b, const sf::Vector2f &c);
    static bool rightOn(const sf::Vector2f &a, const sf::Vector2f &b, const sf::Vector2f &c);

    static float area(const sf::Vector2f &a, const sf::Vector2f &b, const sf::Vector2f &c);

    static sf::Vector2f lineIntersect(const sf::Vector2f &p1, const sf::Vector2f &p2,
                                      const sf::Vector2f &q1, const sf::Vector2f &q2);
    static bool lineIntersect(const sf::Vector2f &point1, const sf::Vector2f &point2,
                              const sf::Vector2f &point3, const sf::Vector2f &point4,
                              bool firstIsSegment, bool secondIsSegment, sf::Vector2f &point);
    static bool lineIntersect(const sf::Vector2f &point1, const sf::Vector2f &point2,
                              const sf::Vector2f &point3, const sf::Vector2f &point4,
                              sf::Vector2f &intersectionPoint);

    static bool canSee(int i, int j, const std::vector<sf::Vector2f> &vertices);

    static bool floatEquals(float value1, float value2);
    static float squareDist(const sf::Vector2f &a, const sf::Vector2f &b);

};

}
}

#endif // BAYAZITDECOMPOSER_H
