#ifndef UN_UTILS_UTILS_H
#define UN_UTILS_UTILS_H

#include <un/un_export.h>
#include <un/un_defines.h>

#include <SFML/Graphics.hpp>

#include <stdint.h>

namespace un {
namespace utils {

UN_EXPORT uint64_t currentTimestamp();
UN_EXPORT uint64_t cantorPairing(int64_t v1, int64_t v2);

template <typename T>
UN_EXPORT inline void combine(sf::Rect<T> &rect1, const sf::Rect<T> &rect2) {
    T minLeft = std::min(rect1.left, rect2.left);
    T minTop = std::min(rect1.top, rect2.top);
    rect1.width = std::max(rect1.left + rect1.width,
                           rect2.left + rect2.width) - minLeft;
    rect1.height = std::max(rect1.top + rect1.height,
                            rect2.top + rect2.height) - minTop;
    rect1.left = minLeft;
    rect1.top = minTop;
}

template <class T>
inline void ensureCapacity(T &container, typename T::size_type index) {
    if(container.size() <= index)
        container.resize(index + 1);
}

}
}

#endif // UN_UTILS_UTILS_H
