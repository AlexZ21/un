#ifndef UN_EXPORT_H
#define UN_EXPORT_H

#if !defined(UN_STATIC)
    #if defined(SYSTEM_WIN)
        #define UN_API_EXPORT __declspec(dllexport)
        #define UN_API_IMPORT __declspec(dllimport)

        #ifdef _MSC_VER
            #pragma warning(disable: 4251)
        #endif

    #else // Linux, FreeBSD, Mac OS X
        #if __GNUC__ >= 4
            #define UN_API_EXPORT __attribute__ ((__visibility__ ("default")))
            #define UN_API_IMPORT __attribute__ ((__visibility__ ("default")))
        #else
            #define UN_API_EXPORT
            #define UN_API_IMPORT
        #endif
    #endif
#else
    #define UN_API_EXPORT
    #define UN_API_IMPORT
#endif

#if !defined(UN_STATIC)
#  define UN_EXPORT UN_API_EXPORT
#else
#  define UN_EXPORT UN_API_IMPORT
#endif

#endif // UN_EXPORT_H
