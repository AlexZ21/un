#ifndef UN_COMPONENTSTORAGE_H
#define UN_COMPONENTSTORAGE_H

#include <un/un_export.h>
#include <un/un_defines.h>
#include <un/core/entity.h>
#include <un/core/component.h>
#include <un/utils/classtypeid.h>

namespace un {

class UN_EXPORT ComponentStorage
{
public:    
    ComponentStorage(size_t size);
    ComponentStorage(const ComponentStorage &) = delete;
    ComponentStorage(ComponentStorage &&) = delete;
    ComponentStorage &operator=(const ComponentStorage &) = delete;
    ComponentStorage &operator=(ComponentStorage &&) = delete;

    void addComponent(Entity &entity, Component *component, utils::TypeId componentTypeId);
    void removeComponent(Entity &entity, utils::TypeId componentTypeId);
    void removeAllComponents(Entity &entity);

    Component *component(const Entity &entity, utils::TypeId componentTypeId) const;
    ComponentMask componentTypeList(const Entity &entity) const;
    std::vector<Component *> components(const Entity &entity) const;

    bool hasComponent(const Entity &entity, utils::TypeId componentTypeId) const;

    void resize(size_t size);
    void clear();

private:
    struct EntityComponents {
        EntityComponents() = default;
        EntityComponents(EntityComponents &&e) :
            components(std::move(e.components)),
            componentTypeList(std::move(e.componentTypeList)) {}

        std::array<uuptr<Component>, MAX_AMOUNT_OF_COMPONENTS> components;
        ComponentMask componentTypeList;
    };

    const std::array<uuptr<Component>, MAX_AMOUNT_OF_COMPONENTS> &componentsImpl(const Entity &entity) const;
    std::array<uuptr<Component>, MAX_AMOUNT_OF_COMPONENTS> &componentsImpl(const Entity &entity);

private:
    std::vector<EntityComponents> m_componentEntries;

};

}

#endif // UN_COMPONENTSTORAGE_H
