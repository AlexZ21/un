#ifndef UN_WORLD_H
#define UN_WORLD_H

#include <un/un_export.h>
#include <un/un_defines.h>
#include <un/core/entity.h>
#include <un/core/system.h>
#include <un/utils/classtypeid.h>

#include <unordered_map>

namespace un {

class UN_EXPORT World
{
    friend class Entity;
public:
    World(size_t entityPoolSize);
    World(const World &) = delete;
    World(World &&) = delete;
    World &operator=(const World &) = delete;
    World &operator=(World &&) = delete;
    ~World() = default;

    //! Systems
    bool addSystem(BaseSystem *system);
    bool removeSystem(BaseSystem *system);
    void removeAllSystems();
    bool systemExists(BaseSystem *system);

    //! Entities
    Entity createEntity();
    std::vector<Entity> createEntities(size_t amount);
    void destroyEntity(const Entity &entity);
    void destroyEntities(const std::vector<Entity> &entities);
    void enableEntity(const Entity &entity);
    void disableEntity(const Entity &entity);
    bool isEnabled(const Entity &entity) const;
    bool isValid(const Entity &entity) const;
    Entity entity(size_t index);
    const std::vector<Entity> &entities() const;

    void refresh();
    void clear();

private:
    void checkForResize(size_t amountOfEntitiesToBeAllocated);
    void resize(size_t amount);

private:
    struct SystemDeleter {
        void operator() (BaseSystem *system) const {
            system->m_world = nullptr;
            system->m_entities.clear();
        }
    };

    struct EntityAttributes {
        struct Attribute {
            bool activated = false;
            std::vector<bool> systems;
        };

        explicit EntityAttributes(size_t amountOfEntities) :
            componentStorage(amountOfEntities),
            attributes(amountOfEntities) {}

        void resize(size_t amountOfEntities) {
            componentStorage.resize(amountOfEntities);
            attributes.resize(amountOfEntities);
        }

        void clear() {
            componentStorage.clear();
            attributes.clear();
        }

        ComponentStorage componentStorage;
        std::vector<Attribute> attributes;
    };

    struct EntityCache {
        void clearTemp() {
            killed.clear();
            enabled.clear();
            disabled.clear();
        }

        void clear() {
            alive.clear();
            clearTemp();
        }

        std::vector<Entity> alive;
        std::vector<Entity> killed;
        std::vector<Entity> enabled;
        std::vector<Entity> disabled;
    };

    std::unordered_map<utils::TypeId, uuptr<BaseSystem, SystemDeleter>> m_systems;
    EntityAttributes m_entityAttributes;
    EntityCache m_entityCache;
    EntityIdPool m_entityIdPool;

};

}

#endif // UN_WORLD_H
