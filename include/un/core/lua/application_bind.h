#ifndef APPLICATION_BIND_H
#define APPLICATION_BIND_H

#include <un/core/application.h>

#include <sol/sol.hpp>

namespace un {

inline void luaApplicationBind(sol::state *state)
{
    // un namespace
    sol::table unTable = state->get<sol::table>("un");

    // Application table and methods
    sol::table appTable = unTable.create_named("Application");
    appTable["showWindow"] = &Application::showWindow;
}

}

#endif // APPLICATION_BIND_H
