#ifndef SFML_BIND_H
#define SFML_BIND_H

#include <un/core/application.h>

#include <sol/sol.hpp>
#include <SFML/Graphics.hpp>

namespace un {

inline void luaSfmlBind(sol::state *state)
{
    sol::table sfTable = state->create_table("sf");

    // Vectors
    sfTable.new_usertype<sf::Vector2i>
            ("Vector2i",
             sol::constructors<sf::Vector2i(), sf::Vector2i(float, float)>(),
             "x", &sf::Vector2i::x,
             "y", &sf::Vector2i::y);

    sfTable.new_usertype<sf::Vector2u>
            ("Vector2u",
             sol::constructors<sf::Vector2u(), sf::Vector2u(uint32_t, uint32_t)>(),
             "x", &sf::Vector2u::x,
             "y", &sf::Vector2u::y);

    sfTable.new_usertype<sf::Vector2f>
            ("Vector2f",
             sol::constructors<sf::Vector2f(), sf::Vector2f(float, float)>(),
             "x", &sf::Vector2f::x,
             "y", &sf::Vector2f::y);

    // Rects
    sfTable.new_usertype<sf::IntRect>
            ("IntRect",
             sol::constructors<sf::IntRect(int32_t, int32_t, int32_t, int32_t), sf::IntRect(sf::Vector2i, sf::Vector2i)>(),
             "left", &sf::IntRect::left,
             "top", &sf::IntRect::top,
             "width", &sf::IntRect::width,
             "height", &sf::IntRect::height,
             "contains",
             sol::overload(static_cast<bool(sf::IntRect::*)(int32_t, int32_t) const>(&sf::IntRect::contains),
                           static_cast<bool(sf::IntRect::*)(const sf::Vector2i &) const>(&sf::IntRect::contains)),
             "intersects",
             sol::overload(static_cast<bool(sf::IntRect::*)(const sf::IntRect &) const>(&sf::IntRect::intersects),
                           static_cast<bool(sf::IntRect::*)(const sf::IntRect &, sf::IntRect &) const>
                           (&sf::IntRect::intersects))
             );

    sfTable.new_usertype<sf::FloatRect>
            ("FloatRect",
             sol::constructors<sf::FloatRect(float, float, float, float), sf::FloatRect(sf::Vector2f, sf::Vector2f)>(),
             "left", &sf::FloatRect::left,
             "top", &sf::FloatRect::top,
             "width", &sf::FloatRect::width,
             "height", &sf::FloatRect::height,
             "contains",
             sol::overload(static_cast<bool(sf::FloatRect::*)(float, float) const>(&sf::FloatRect::contains),
                           static_cast<bool(sf::FloatRect::*)(const sf::Vector2f &) const>(&sf::FloatRect::contains)),
             "intersects",
             sol::overload(static_cast<bool(sf::FloatRect::*)(const sf::FloatRect &) const>(&sf::FloatRect::intersects),
                           static_cast<bool(sf::FloatRect::*)(const sf::FloatRect &, sf::FloatRect &) const>
                           (&sf::FloatRect::intersects))
             );

}

}

#endif // SFML_BIND_H
