#ifndef CORE_LUA_BIND_H
#define CORE_LUA_BIND_H

#include "application_bind.h"
#include "sfml_bind.h"

#include <sol/sol.hpp>

namespace un {

inline void luaCoreBind(sol::state *state)
{
    luaApplicationBind(state);
    luaSfmlBind(state);
}

}

#endif // CORE_LUA_BIND_H
