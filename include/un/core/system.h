#ifndef UN_SYSTEM_H
#define UN_SYSTEM_H

#include <un/un_export.h>
#include <un/un_defines.h>
#include <un/core/entity.h>
#include <un/core/filter.h>

#define UN_SYSTEM_TYPE_ID(_class_) \
    utils::TypeId typeId() { \
    return utils::ClassTypeId<BaseSystem>::id<_class_>(); \
    }

namespace un {

class World;

class BaseSystem
{
    friend class World;
public:
    BaseSystem(const Filter &filter);
    virtual ~BaseSystem() = default;

    virtual utils::TypeId typeId() = 0;

    World *world() const;
    const Filter &filter() const;
    const std::vector<Entity> &entities() const;

private:
    virtual void initialize() {}
    virtual void onEntityAdded(const Entity &entity) {}
    virtual void onEntityRemoved(const Entity &entity) {}

    void add(const Entity &entity);
    void remove(const Entity &entity);

    void setWorld(World *world);

private:
    World *m_world;
    Filter m_filter;
    std::vector<Entity> m_entities;
};

template <class RequireList, class ExcludeList = Filter::Excludes<>>
class System : public BaseSystem
{
public:
    System() : BaseSystem{Filter::make<RequireList, ExcludeList>()} {}
    ~System() = default;
};

}

#endif // UN_SYSTEM_H
