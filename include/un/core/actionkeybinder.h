#ifndef UN_ACTIONKEYBINDER_H
#define UN_ACTIONKEYBINDER_H

#include <un/un_export.h>
#include <un/un_defines.h>

#include <unordered_map>
#include <functional>

#include <SFML/Window/Event.hpp>

namespace un {

class UN_EXPORT ActionKeyBinder
{
public:
    enum InputType {
        Hold,
        PressOnce,
        ReleaseOnce
    };

    enum InputDeviceType
    {
        Keyboard,
        Mouse,
        Joystick
    };

    struct ShortcutState {
        std::string actionName;
        InputType inputType;
        bool activated = false;
    };

    ActionKeyBinder();
    ~ActionKeyBinder() = default;

    void addAction(const std::string &name, const std::function<void()> &callback);
    void removeAction(const std::string &name);
    void clearAllActions();

    void bind(const std::string &actionName, InputType inputType,
              sf::Keyboard::Key code, bool alt = false, bool control = false,
              bool shift = false, bool system = false);
    void bind(const std::string &actionName, InputType inputType, sf::Mouse::Button button);
    void bind(const std::string &actionName, InputType inputType,
              uint32_t joystickId, uint32_t button);
    void unbind(const std::string &actionName);
    void unbindAll();

    void update(const sf::Event &event);

private:
    size_t sfmlEventHash(const sf::Event &event);

private:
    std::unordered_multimap<size_t, ShortcutState> m_shortcuts;
    std::unordered_map<std::string, std::function<void()>> m_actions;

};

}

#endif // UN_ACTIONKEYBINDER_H
