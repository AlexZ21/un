#ifndef UN_FILTER_H
#define UN_FILTER_H

#include <un/un_export.h>
#include <un/un_defines.h>
#include <un/core/component.h>
#include <un/core/componentstorage.h>
#include <un/utils/classtypeid.h>

namespace un {

class UN_EXPORT Filter
{
public:
    template <class... Args>
    struct TypeList { };

    struct BaseRequires { };
    struct BaseExcludes { };

    template <class... Args>
    struct Requires : TypeList<Args...>, BaseRequires {};

    template <class... Args>
    struct Excludes : TypeList<Args...>, BaseExcludes {};

    Filter() = default;
    Filter(ComponentMask requires, ComponentMask excludes);
    ~Filter() = default;

    bool doesPassFilter(const ComponentMask& typeList) const;

    template <class... Args>
    static ComponentMask types(TypeList<Args...> typeList) { return ComponentMask(); }

    template <class T, class... Args>
    static ComponentMask types(TypeList<T, Args...> typeList) {
        if (!std::is_base_of<Component, T>::value)
            return ComponentMask();
        return ComponentMask().set(utils::ClassTypeId<Component>::id<T>()) | types(TypeList<Args...>());
    }

    template <class RequireList, class ExcludeList>
    static Filter make() {
        if (!std::is_base_of<BaseRequires, RequireList>::value || !std::is_base_of<BaseExcludes, ExcludeList>::value)
            return Filter();
        return Filter{types(RequireList{}), types(ExcludeList{})};
    }

private:
    ComponentMask m_requires;
    ComponentMask m_excludes;

};

}

#endif // UN_FILTER_H
