#ifndef UN_COMPONENT_H
#define UN_COMPONENT_H

#include <un/un_export.h>
#include <un/un_defines.h>
#include <un/utils/classtypeid.h>

#include <type_traits>
#include <vector>
#include <bitset>

namespace un {

class UN_EXPORT Component
{
public:
    Component() = default;
    virtual ~Component() = default;    
};

const int32_t MAX_AMOUNT_OF_COMPONENT_MASK = 32;
const int32_t MAX_AMOUNT_OF_COMPONENTS = 32;
using ComponentMask = std::bitset<MAX_AMOUNT_OF_COMPONENT_MASK>;

}

#endif // UN_COMPONENT_H
