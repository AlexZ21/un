#ifndef UN_EVENTS_H
#define UN_EVENTS_H

#include <un/un_export.h>
#include <un/un_defines.h>
#include <un/core/manager.h>
#include <un/utils/ptr.h>

#include <SFML/Window/Event.hpp>

#include <queue>
#include <mutex>
#include <functional>
#include <unordered_map>

namespace un {

struct UN_EXPORT Event
{
    virtual ~Event() = default;

    enum Type {
        // SFML event types
        Closed,
        Resized,
        LostFocus,
        GainedFocus,
        TextEntered,
        KeyPressed,
        KeyReleased,
        MouseWheelMoved,
        MouseWheelScrolled,
        MouseButtonPressed,
        MouseButtonReleased,
        MouseMoved,
        MouseEntered,
        MouseLeft,
        JoystickButtonPressed,
        JoystickButtonReleased,
        JoystickMoved,
        JoystickConnected,
        JoystickDisconnected,
        TouchBegan,
        TouchMoved,
        TouchEnded,
        SensorChanged
    };

    int32_t type = -1;
};

//! SFML events
struct UN_EXPORT ClosedEvent : public Event
{
    ClosedEvent() { type = Event::Closed; }
};

struct UN_EXPORT ResizedEvent : public Event
{
    ResizedEvent() { type = Event::Resized; }
    uint32_t width = 0;
    uint32_t height = 0;
};

struct UN_EXPORT LostFocusEvent : public Event
{
    LostFocusEvent() { type = Event::LostFocus; }
};

struct UN_EXPORT GainedFocusEvent : public Event
{
    GainedFocusEvent() { type = Event::GainedFocus; }
};

struct UN_EXPORT TextEnteredEvent : public Event
{
    TextEnteredEvent() { type = Event::TextEntered; }
    uint32_t unicode = 0;
};

struct UN_EXPORT KeyPressedEvent : public Event
{
    KeyPressedEvent() { type = Event::KeyPressed; }
    sf::Keyboard::Key code = sf::Keyboard::Unknown;
    bool alt = false;
    bool control = false;
    bool shift = false;
    bool system = false;
};

struct UN_EXPORT KeyReleasedEvent : public Event
{
    KeyReleasedEvent() { type = Event::KeyReleased; }
    sf::Keyboard::Key code = sf::Keyboard::Unknown;
    bool alt = false;
    bool control = false;
    bool shift = false;
    bool system = false;
};

struct UN_EXPORT MouseWheelMovedEvent : public Event
{
    MouseWheelMovedEvent() { type = Event::MouseWheelMoved; }
    int32_t delta = 0;
    int32_t x = 0;
    int32_t y = 0;
};

struct UN_EXPORT MouseWheelScrolledEvent : public Event
{
    MouseWheelScrolledEvent() { type = Event::MouseWheelScrolled; }
    sf::Mouse::Wheel wheel = sf::Mouse::VerticalWheel;
    float delta = 0;
    int32_t x = 0;
    int32_t y = 0;
};

struct UN_EXPORT MouseButtonPressedEvent : public Event
{
    MouseButtonPressedEvent() { type = Event::MouseButtonPressed; }
    sf::Mouse::Button button = sf::Mouse::ButtonCount;
    int32_t x = 0;
    int32_t y = 0;
};

struct UN_EXPORT MouseButtonReleasedEvent : public Event
{
    MouseButtonReleasedEvent() { type = Event::MouseButtonReleased; }
    sf::Mouse::Button button = sf::Mouse::ButtonCount;
    int32_t x = 0;
    int32_t y = 0;
};

struct UN_EXPORT MouseMovedEvent : public Event
{
    MouseMovedEvent() { type = Event::MouseMoved; }
    int32_t x = 0;
    int32_t y = 0;
};

struct UN_EXPORT MouseEnteredEvent : public Event
{
    MouseEnteredEvent() { type = Event::MouseEntered; }
};

struct UN_EXPORT MouseLeftEvent : public Event
{
    MouseLeftEvent() { type = Event::MouseLeft; }
};

struct UN_EXPORT JoystickButtonPressedEvent : public Event
{
    JoystickButtonPressedEvent() { type = Event::JoystickButtonPressed; }
    uint32_t joystickId = 0;
    uint32_t button = 0;
};

struct UN_EXPORT JoystickButtonReleasedEvent : public Event
{
    JoystickButtonReleasedEvent() { type = Event::JoystickButtonReleased; }
    uint32_t joystickId = 0;
    uint32_t button = 0;
};

struct UN_EXPORT JoystickMovedEvent : public Event
{
    JoystickMovedEvent() { type = Event::JoystickMoved; }
    uint32_t joystickId = 0;
    sf::Joystick::Axis axis = sf::Joystick::X;
    float position = 0;
};

struct UN_EXPORT JoystickConnectedEvent : public Event
{
    JoystickConnectedEvent() { type = Event::JoystickConnected; }
    uint32_t joystickId = 0;
};

struct UN_EXPORT JoystickDisconnectedEvent : public Event
{
    JoystickDisconnectedEvent() { type = Event::JoystickDisconnected; }
    uint32_t joystickId = 0;
};

struct UN_EXPORT TouchBeganEvent : public Event
{
    TouchBeganEvent() { type = Event::TouchBegan; }
    uint32_t finger = 0;
    int32_t x = 0;
    int32_t y = 0;
};

struct UN_EXPORT TouchMovedEvent : public Event
{
    TouchMovedEvent() { type = Event::TouchMoved; }
    uint32_t finger = 0;
    int32_t x = 0;
    int32_t y = 0;
};

struct UN_EXPORT TouchEndedEvent : public Event
{
    TouchEndedEvent() { type = Event::TouchEnded; }
    uint32_t finger = 0;
    int32_t x = 0;
    int32_t y = 0;
};

struct UN_EXPORT SensorChangedEvent : public Event
{
    SensorChangedEvent() { type = Event::SensorChanged; }
    sf::Sensor::Type sensorType = sf::Sensor::Count;
    float x = 0;
    float y = 0;
    float z = 0;
};

Event *createSfmlEvent(const sf::Event &sfmlEvent);

struct EventListener
{
    EventListener() = default;
    EventListener(const std::function<void (Event *)> &callback) : callback(callback) {}
    std::function<void (Event *)> callback = nullptr;
};

class UN_EXPORT Events : public Manager
{
public:
    UN_INHERIT_PTRS(Events)

    Events();
    ~Events() = default;

    void sendEvent(int32_t type);
    void sendEvent(Event *event);

    void setEventListener(int32_t type, EventListener *eventListener);
    void unsetEventListener(EventListener *eventListener);

    void update(sf::Time);

private:
    std::queue<uuptr<Event>> m_events;
    std::unordered_multimap<int32_t, uuptr<EventListener>> m_eventListeners;
    std::mutex m_eventsmx;

};

}

#endif // UN_EVENTS_H
