#ifndef LUA_H
#define LUA_H

#include <un/un_export.h>
#include <un/un_defines.h>
#include <un/core/manager.h>
#include <un/utils/ptr.h>

#include <sol/sol.hpp>

namespace un {

class UN_EXPORT Lua : public Manager
{
public:
    UN_INHERIT_PTRS(Lua)

    Lua();
    ~Lua() = default;

    void script(const std::string &script);
    void file(const std::string &filePath);

private:
    void bind();

private:
    sol::state m_state;

};

}

#endif // LUA_H
