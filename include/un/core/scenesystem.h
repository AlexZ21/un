#ifndef UN_SCENESYSTEM_H
#define UN_SCENESYSTEM_H

#include <un/un_export.h>
#include <un/un_defines.h>
#include <un/core/system.h>

#include <SFML/System/Time.hpp>

namespace un {

class UN_EXPORT BaseSceneSystem : public BaseSystem
{
public:
    BaseSceneSystem(const Filter &filter) : BaseSystem(filter) {}
    ~BaseSceneSystem() = default;
    virtual void update(sf::Time elapsed) = 0;
};

template <class RequireList, class ExcludeList = Filter::Excludes<>>
class UN_EXPORT SceneSystem : public BaseSceneSystem
{
public:
    SceneSystem() : BaseSceneSystem{Filter::make<RequireList, ExcludeList>()} {}
    ~SceneSystem() = default;
};

}

#endif // UN_SCENESYSTEM_H
