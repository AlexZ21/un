#ifndef UN_APPLICATION_H
#define UN_APPLICATION_H

#include <un/un_export.h>
#include <un/un_defines.h>
#include <un/core/manager.h>
#include <un/core/events.h>
#include <un/utils/log.h>

#include <SFML/Graphics/RenderWindow.hpp>

#include <unordered_map>
#include <typeindex>
#include <thread>
#include <atomic>
#include <mutex>

namespace un {

class Scene;
class ActionKeyBinder;

class UN_EXPORT Application
{
public:
    static Application *instance();
    static Application *create();

    //! Application running control funcs
    static int exec();
    static void exit();

    //! Window
    static void showWindow(uint32_t width, uint32_t height, std::string title, bool fullscreen = false);
    static void closeWindow();
    static sf::RenderWindow *window();

    //! Managers
    template<typename T, typename... Args>
    static bool setManager(Args&&... args) {
        if (!m_instance || !std::is_convertible<T, Manager>::value) {
            LSERR(LTOSTR(T), "is not manager");
            return false;
        }
        std::type_index ti = typeid(T);
        if (m_instance->m_managers.find(ti) != m_instance->m_managers.end()) {
            LSERR(LTOSTR(T), "already setted");
            return false;
        }

        auto it = m_instance->m_managers.emplace(ti, T::create(std::forward<Args>(args)...));
        if (it.first->second->isUpdatable())
            m_instance->m_updatableManages.push_back(it.first->second.get());
        return true;
    }

    template<typename T>
    static bool unsetManager() {
        if (!m_instance)
            return false;
        auto it = m_instance->m_managers.find(std::type_index(typeid(T)));
        if (it == m_instance->m_managers.end())
            return false;
        if (it->second->isUpdatable())
            std::remove(m_instance->m_updatableManages.begin(), m_instance->m_updatableManages.end(), it->second.get());
        m_instance->m_managers.erase(it);
        return true;
    }

    static void unsetAllManagers();

    template<typename T>
    static T *manager() {
        if (!m_instance)
            return nullptr;
        auto it = m_instance->m_managers.find(std::type_index(typeid(T)));
        if (it == m_instance->m_managers.end())
            return nullptr;
        return static_cast<T*>(it->second.get());
    }

    //! Events
    static void sendEvent(int32_t type);
    static void sendEvent(Event *event);
    static void setEventListener(int32_t type, EventListener *eventListener);
    static void unsetEventListener(EventListener *eventListener);

    //! Scene
    static Scene *scene();
    static void setScene(Scene *scene);

    //! Action key binder
    static ActionKeyBinder *actionKeyBinder();
    static void setActionKeyBinder(ActionKeyBinder *actionKeyBinder);

private:
    Application();
    ~Application();

    //! Loop funcs
    void mainLoop();
    void tick(sf::Time elapsed);

private:
    static Application *m_instance;

    sf::Time m_tickTime; ///< Loop tick time: 16 msec
    std::atomic_bool m_running;

    uint32_t m_width; ///< Window width
    uint32_t m_height; ///< Window height
    std::string m_title; ///< Window title
    bool m_fullscreen; ///< Open window in fullscreen
    uuptr<sf::RenderWindow> m_window;

    std::unordered_map<std::type_index, Manager::ptr> m_managers;
    std::vector<Manager *> m_updatableManages; ///< Managers which update every tick

    uuptr<Scene> m_scene; ///< Current scene
    uuptr<ActionKeyBinder> m_actionKeyBinder;

};

}

#endif // UN_APPLICATION_H
