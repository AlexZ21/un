#ifndef UN_MANAGER_H
#define UN_MANAGER_H

#include <un/un_export.h>
#include <un/un_defines.h>
#include <un/utils/ptr.h>

#include <SFML/System/Time.hpp>

namespace un {

class UN_EXPORT Manager
{
public:
    UN_PTRS(Manager)

    virtual ~Manager() = default;

    bool isUpdatable() const { return m_updateble; }
    virtual void update(sf::Time) {}

protected:
    Manager(bool updatable = false) : m_updateble(updatable) {}

private:
    bool m_updateble;

};

}

#endif // UN_MANAGER_H
