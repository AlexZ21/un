#ifndef UN_ENTITY_H
#define UN_ENTITY_H

#include <un/un_export.h>
#include <un/un_defines.h>
#include <un/core/component.h>

#include <vector>

namespace un {

class World;

class UN_EXPORT Entity
{
    friend class World;
public:
    struct Id {
        bool operator==(const Id &other) const {
            return index == other.index && counter == other.counter;
        }
        int32_t index = -1;
        int32_t counter = -1;
    };

    Entity();
    Entity(World *world, Id id);
    ~Entity();

    World *world() const;
    Id id() const { return m_id; }

    bool isValid() const;
    bool isEnabled() const;
    void enable();
    void disable();
    void destroy();

    template <typename T, typename... Args>
    T *addComponent(Args&&... args) {
        if (!std::is_base_of<Component, T>())
            return nullptr;
        T *component = new T{std::forward<Args>(args)...};
        addComponent(component, utils::ClassTypeId<Component>::id<T>());
        return component;
    }

    template <typename T>
    void removeComponent() {
        if (!std::is_base_of<Component, T>())
            return;
        removeComponent(utils::ClassTypeId<Component>::id<T>());
    }

    void removeAllComponents();

    template <typename T>
    T *component() const {
        if (!std::is_base_of<Component, T>())
            return nullptr;
        return static_cast<T*>(component(utils::ClassTypeId<Component>::id<T>()));
    }

    template <typename T>
    bool hasComponent() const {
        if (!std::is_base_of<Component, T>())
            return false;
        return hasComponent(utils::ClassTypeId<Component>::id<T>());
    }

    std::vector<Component *> components() const;
    ComponentMask componentTypeList() const;

    bool operator==(const Entity &entity) const;
    bool operator!=(const Entity &entity) const { return !operator==(entity); }

private:
    void addComponent(Component *component, utils::TypeId componentTypeId);
    void removeComponent(utils::TypeId componentTypeId);
    Component *component(utils::TypeId componentTypeId) const;
    bool hasComponent(utils::TypeId componentTypeId) const;

private:
    World *m_world;
    Id m_id;
};

class UN_EXPORT EntityIdPool
{
public:
    EntityIdPool(size_t poolSize);
    ~EntityIdPool() = default;

    Entity::Id create();
    void remove(Entity::Id id);

    Entity::Id get(size_t index) const;

    bool isValid(Entity::Id id) const;

    size_t size() const;
    void resize(std::size_t size);

    void clear();

private:
    size_t m_poolSize;
    int32_t m_nextid;
    std::vector<Entity::Id> m_freeids;
    std::vector<int32_t> m_counts;
};

}

#endif // UN_ENTITY_H
