#ifndef UN_RANDOM_H
#define UN_RANDOM_H

#include <un/un_export.h>
#include <un/un_defines.h>

#include <SFML/System.hpp>

#include <random>
#include <ctime>
#include <functional>

namespace un {

template <typename T>
class Distribution
{
public:
    template <typename U>
    struct Constant {
        Constant(const U &value) : value(value) {}
        U operator() () const { return value; }
        U value;
    };

    template <typename U>
    Distribution(U constant, typename std::enable_if<std::is_convertible<U, T>::value>::type* = nullptr) :
        m_func(Constant<T>(constant)) {}

    template <typename Fn>
    Distribution(Fn func, typename std::enable_if<!std::is_convertible<Fn, T>::value>::type* = nullptr) :
        m_func(func) {}

    ~Distribution() = default;
    T operator()() const { return m_func(); }

private:
    std::function<T ()> m_func;

};

class Random
{
public:
    Random() = delete;
    ~Random() = delete;

    static std::mt19937 &engine() { static std::mt19937 engine(std::time(nullptr)); return engine; }
    static void setRandomSeed(uint32_t seed) {
        engine().seed(seed);
    }

    template <typename T>
    static Distribution<T> uniformInt(T min, T max) {
        return Distribution<T>([=](){
            return Random::randomInt(min, max);
        });
    }

    template <typename T>
    static Distribution<T> uniformReal(T min, T max) {
        return Distribution<T>([=](){
            return Random::randomReal(min, max);
        });
    }

    static Distribution<sf::Vector2f> uniformVectorReal(sf::Vector2f min, sf::Vector2f max) {
        return Distribution<sf::Vector2f>([=](){
            return sf::Vector2f(Random::randomReal(min.x, max.x), Random::randomReal(min.y, max.y));
        });
    }

    static Distribution<sf::Time> uniformTime(sf::Time min,
                                              sf::Time max) {
        return Distribution<sf::Time>([=](){
            return sf::milliseconds(randomInt(min.asMilliseconds(), max.asMilliseconds()));
        });
    }

    static Distribution<sf::Vector2f> rect(sf::Vector2f center, sf::Vector2f halfSize) {
        return Distribution<sf::Vector2f>([=](){
            return sf::Vector2f(randomRealDev(center.x, halfSize.x), randomRealDev(center.y, halfSize.y));
        });
    }

    static Distribution<sf::Vector2f> circle(sf::Vector2f center, float radius) {
        return Distribution<sf::Vector2f>([=](){
            sf::Vector2f radiusVector(center.x + radius * std::cos(randomReal(0.f, 360.f)),
                                      center.y + radius * std::sin(randomReal(0.f, 360.f)));
            return center + radiusVector;
        });
    }

    template <typename T>
    static T randomInt(T min, T max) {
        std::uniform_int_distribution<T> distribution(min, max);
        return distribution(engine());
    }

    template <typename T>
    static T randomReal(T min, T max) {
        std::uniform_real_distribution<T> distribution(min, max);
        return distribution(engine());
    }

    template <typename T>
    static float randomIntDev(T middle, T deviation) {
        return randomInt(middle - deviation, middle + deviation);
    }

    template <typename T>
    static float randomRealDev(T middle, T deviation) {
        return randomReal(middle - deviation, middle + deviation);
    }

};

}

#endif // UN_RANDOM_H
