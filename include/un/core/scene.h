#ifndef UN_SCENE_H
#define UN_SCENE_H

#include <un/un_export.h>
#include <un/un_defines.h>
#include <un/core/world.h>
#include <un/systems/transformsystem.h>
#include <un/systems/childrensystem.h>
#include <un/systems/box2dsystem.h>
#include <un/systems/scenetreesystem.h>
#include <un/systems/rendersystem.h>

#include <SFML/System/Time.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderStates.hpp>

#include <vector>

namespace un {

class BaseSceneSystem;

class UN_EXPORT Scene
{
public:
    Scene();
    virtual ~Scene();

    //! Entities
    Entity createEntity(bool setDefaultComponents = true);
    void destroyEntity(Entity &entity);

    //! Systems
    void addSystem(BaseSceneSystem *system);
    void removeSystem(BaseSceneSystem *system);

    //! Update and render
    virtual void update(sf::Time elapsed);
    virtual void draw(sf::RenderTarget &target, sf::RenderStates states, sf::Time elapsed);

private:
    uuptr<World> m_world;

    uuptr<TransformSystem> m_transformSystem;
    uuptr<ChildrenSystem> m_childrenSystem;
    uuptr<Box2dSystem> m_box2dSystem;
    uuptr<SceneTreeSystem> m_sceneTreeSystem;
    uuptr<RenderSystem> m_renderSystem;

    std::vector<uuptr<BaseSceneSystem>> m_systems;

};

}

#endif // UN_SCENE_H
