#ifndef UN_ASSETS_H
#define UN_ASSETS_H

#include <un/un_export.h>
#include <un/un_defines.h>
#include <un/core/manager.h>
#include <un/utils/ptr.h>

#include <vector>
#include <string>
#include <unordered_map>

namespace un {

class UN_EXPORT AssetsFileLoader
{
public:
    UN_PTRS(AssetsFileLoader)

    AssetsFileLoader() = default;
    virtual ~AssetsFileLoader() = default;
    virtual std::vector<char> readFile(const std::string &path) = 0;
};

class AssetsLocalFileLoader : public AssetsFileLoader
{
public:
    UN_INHERIT_PTRS(AssetsLocalFileLoader)

    AssetsLocalFileLoader(const std::string &path);
    ~AssetsLocalFileLoader() = default;
    std::vector<char> readFile(const std::string &path);

private:
    std::string m_path;

};

class UN_EXPORT Assets : public Manager
{
public:
    UN_INHERIT_PTRS(Assets)

    enum LoaderType {
        Local
    };

    Assets() = default;
    ~Assets() = default;

    void addSource(const std::string &name, LoaderType loaderType, const std::string &path);
    void removeSource(const std::string &name);

    std::vector<char> readFile(const std::string &path);

    static std::string sourceFromPath(const std::string &path);

private:
    std::unordered_map<std::string, AssetsFileLoader::ptr> m_sources;

};

}

#endif // UN_ASSETS_H
