#include <un/systems/collisionsystem.h>
#include <un/utils/log.h>

#include <cmath>

namespace un {

struct Interval
{
    float min = 0;
    float max = 0;
};

float innerProduct(const sf::Vector2f &vector1, const sf::Vector2f &vector2)
{
    return vector1.x * vector2.x + vector1.y * vector2.y;
}

sf::Vector2f normalize(sf::Vector2f vector)
{
    float magnitude = std::sqrt(vector.x * vector.x + vector.y * vector.y);
    vector.x /= magnitude;
    vector.y /= magnitude;
    return vector;
}

sf::Vector2f perpendicular(const sf::Vector2f vector)
{
    return sf::Vector2f(-vector.y, vector.x);
}

Interval project(const Entity &entity, const sf::Vector2f& axis)
{
//    TransformComponent *transformComponent = entity.component<TransformComponent>();
//    const sf::Transform &transform = transformComponent->transform();
//    CollisionComponent *collisionComponent = entity.component<CollisionComponent>();
//    const std::vector<sf::Vector2f> &vertices = collisionComponent->vertices();

//    sf::Vector2f normalizedAxis = normalize(axis);

//    sf::Vector2f point = transform.transformPoint(vertices.at(0));
//    float dot = innerProduct(point, normalizedAxis);

//    Interval interval{dot, dot};

//    for (size_t i = 1; i < vertices.size(); ++i) {
//        point = transform.transformPoint(vertices.at(i));
//        dot = innerProduct(point, normalizedAxis);
//        if (dot < interval.min)
//            interval.min = dot;
//        else if (dot > interval.max)
//            interval.max = dot;
//    }

    return Interval();//interval;
}

bool isOverlap(const Interval &interval1, const Interval &interval2)
{
    return !(interval1.max < interval2.min || interval2.max < interval1.min);
}

void CollisionSystem::update(sf::Time)
{
    for (const Entity &entity : entities())
        isCollided(entity);
}

void CollisionSystem::isCollided(const Entity &entity)
{
    TransformComponent *transformComponent = entity.component<TransformComponent>();
    RenderComponent *renderComponent = entity.component<RenderComponent>();
    SceneTreeComponent *sceneTreeComponent = entity.component<SceneTreeComponent>();
    CollisionComponent *collisionComponent = entity.component<CollisionComponent>();

    sf::FloatRect globalBounds = renderComponent->drawableGroup()->
            globalBounds(renderComponent->state(), transformComponent->transform());

    sceneTreeComponent->treeQuery(globalBounds, [this, &entity, collisionComponent](int32_t, const Entity &e){
        if (entity != e){
            collisionComponent->m_collided = isCollided(entity, e);
//            LDEBUG(collisionComponent->m_collided);
        }
    });
}

bool CollisionSystem::isCollided(const Entity &entity1, const Entity &entity2)
{
//    TransformComponent *transformComponent1 = entity1.component<TransformComponent>();
//    const sf::Transform &transform = transformComponent1->transform();
//    CollisionComponent *collisionComponent1 = entity1.component<CollisionComponent>();
//    const std::vector<sf::Vector2f> &vertices = collisionComponent1->vertices();

//    for (size_t i = 0; i < vertices.size(); ++i) {
//        sf::Vector2f edge = transform.transformPoint(vertices.at((i+1) % vertices.size()))
//            - transform.transformPoint(vertices.at(i));

//        sf::Vector2f perp = perpendicular(edge);

//        Interval projA = project(entity1, perp);
//        Interval projB = project(entity2, perp);

//        if (!isOverlap(projA, projB))
//            return false;
//    }

    return true;
}

}
