#ifndef OBJECTSTORAGE_BIND_H
#define OBJECTSTORAGE_BIND_H

#include <un/core/objectstorage.h>

#include <sol/sol.hpp>

namespace un {

inline void luaObjectStorageBind(sol::state *state)
{
    // un namespace
    sol::table unTable = state->get<sol::table>("un");

}

}

#endif // OBJECTSTORAGE_BIND_H
