#include <un/graphics/tilemap.h>
#include <un/graphics/textures.h>
#include <un/graphics/statictexture.h>
#include <un/core/application.h>
#include <un/core/assets.h>
#include <un/utils/utils.h>
#include <un/utils/log.h>

#include <tinyxml2.h>

#include <algorithm>

namespace un {

TileMap::TileMap(size_t width, size_t height, uint32_t tileWidth, uint32_t tileHeight,
                 StaticTexture *texture, size_t chunkWidth, size_t chunkHeight) :
    m_mapSize(width, height),
    m_tileSize(tileWidth, tileHeight)
{
    if (chunkWidth == 0 || chunkHeight == 0)
        m_chunkSize = m_mapSize;
    else
        m_chunkSize = sf::Vector2<size_t>(chunkWidth, chunkHeight);

    if (texture && width != 0 && height != 0 && tileWidth != 0 && tileHeight != 0) {
        Tileset *tileset = new Tileset();
        tileset->firstGid = 0;
        tileset->name = "tileset_1";
        tileset->columns = texture->rect().width / tileWidth;
        tileset->tileCount = width * height;
        m_tilesets.push_back(uuptr<Tileset>(tileset));

        Layer *layer = new Layer();
        layer->name = "layer_1";
        layer->tileset = tileset;
        updateChuncks(layer);
        m_layers.push_back(uuptr<Layer>(layer));
    }
}

TileMap::TileMap(const std::string &tmxFile, size_t chunkWidth, size_t chunkHeight) :
    m_mapSize(0, 0),
    m_tileSize(0, 0)
{
    m_chunkSize = sf::Vector2<size_t>(chunkWidth, chunkHeight);
    loadMap(tmxFile);
}

void TileMap::setTile(uint32_t layer, size_t tileX, size_t tileY, const sf::IntRect &textureRect)
{
    if (layer >= m_layers.size())
        return;
    Layer *l = m_layers.at(layer).get();
    setTile(l, tileX, tileY, textureRect);
}

void TileMap::setTile(uint32_t layer, size_t tileX, size_t tileY, const sf::Vector2i &texturePos)
{
    setTile(layer, tileX, tileY, sf::IntRect(texturePos.x * m_tileSize.x, texturePos.y * m_tileSize.y,
                                             m_tileSize.x, m_tileSize.y));
}

sf::FloatRect TileMap::localBounds() const
{
    return sf::FloatRect(0, 0, m_mapSize.x * m_tileSize.x, m_mapSize.y * m_tileSize.y);
}

sf::FloatRect TileMap::globalBounds() const
{
    return getTransform().transformRect(localBounds());
}

void TileMap::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    states.transform *= getTransform();
    for (auto &layer : m_layers) {
        states.texture = layer->tileset->texture->source();
        for (auto &it : layer->chunks) {
            target.draw(&it.second->vertices[0], it.second->vertices.size(), sf::PrimitiveType::Quads, states);
        }
    }
}

void TileMap::updateChuncks(TileMap::Layer *layer)
{
    layer->chunks.clear();
    sf::Vector2i count = chuncksCount();

    for (int i = 0; i < count.x; ++i) {
        for (int j = 0; j < count.y; ++j) {
            Chunk *chunk = new Chunk();
            chunk->vertices.resize((m_chunkSize.x + 2) * (m_chunkSize.y + 2) * 4);

            float chunkXOffset = i * m_chunkSize.x * m_tileSize.x;
            float chunkYOffset = j * m_chunkSize.y * m_tileSize.y;

            for (int x = 0; x < m_chunkSize.x; ++x) {
                for (int y = 0; y < m_chunkSize.y; ++y) {
                    sf::Vertex *ptr = &chunk->vertices[(x + y * m_chunkSize.x) * 4];

                    ptr->position.x = chunkXOffset + x * m_tileSize.x;
                    ptr->position.y = chunkYOffset + y * m_tileSize.y;
                    ptr++;

                    ptr->position.x = chunkXOffset + x * m_tileSize.x + m_tileSize.x;
                    ptr->position.y = chunkYOffset + y * m_tileSize.y;
                    ptr++;

                    ptr->position.x = chunkXOffset + x * m_tileSize.x + m_tileSize.x;
                    ptr->position.y = chunkYOffset + y * m_tileSize.y + m_tileSize.y;
                    ptr++;

                    ptr->position.x = chunkXOffset + x * m_tileSize.x;
                    ptr->position.y = chunkYOffset + y * m_tileSize.y + m_tileSize.y;
                }
            }

            layer->chunks.emplace(utils::cantorPairing(i, j), uuptr<Chunk>(chunk));
        }
    }
}

sf::Vector2i TileMap::chuncksCount() const
{
    sf::Vector2i count;

    count.x = m_mapSize.x / m_chunkSize.x;
    if (m_mapSize.x % m_chunkSize.x > 0)
        ++count.x;

    count.y = m_mapSize.x / m_chunkSize.x;
    if (m_mapSize.x % m_chunkSize.x > 0)
        ++count.y;

    return count;
}

void TileMap::setTile(TileMap::Layer *layer, size_t tileX, size_t tileY, const sf::IntRect &textureRect)
{
    sf::Vector2i chunkPos(tileX / m_chunkSize.x, tileY / m_chunkSize.y);

    auto it = layer->chunks.find(utils::cantorPairing(chunkPos.x, chunkPos.y));
    if (it == layer->chunks.end())
        return;

    Chunk *chunk = it->second.get();

    sf::Vector2i tileInChunk(tileX - chunkPos.x * m_chunkSize.x,
                             tileY - chunkPos.y * m_chunkSize.y);

    int index = (tileInChunk.x + tileInChunk.y * m_chunkSize.x) * 4;

    if (index > chunk->vertices.size())
        return;

    sf::Vertex *ptr = &chunk->vertices[index];

    ptr->texCoords.x = textureRect.left;
    ptr->texCoords.y = textureRect.top;
    ptr++;

    ptr->texCoords.x = textureRect.left + textureRect.width;
    ptr->texCoords.y = textureRect.top;
    ptr++;

    ptr->texCoords.x = textureRect.left + textureRect.width;
    ptr->texCoords.y = textureRect.top + textureRect.height;
    ptr++;

    ptr->texCoords.x = textureRect.left;
    ptr->texCoords.y = textureRect.top + textureRect.height;
}

void TileMap::setTile(TileMap::Layer *layer, size_t tileX, size_t tileY, const sf::Vector2i &texturePos)
{
    setTile(layer, tileX, tileY, sf::IntRect(texturePos.x * m_tileSize.x, texturePos.y * m_tileSize.y,
                                             m_tileSize.x, m_tileSize.y));
}

TileMap::Tileset *TileMap::tileset(const std::string &name)
{
    auto it = std::find_if(m_tilesets.begin(), m_tilesets.end(),
                           [name](const uuptr<Tileset> &tileset) { return tileset->name == name; });
    if (it == m_tilesets.end())
        return nullptr;
    return (*it).get();
}

void TileMap::loadMap(const std::string &tmxFile)
{
    LSDEBUG("Loading map file:", tmxFile);

    tinyxml2::XMLDocument doc;
    std::vector<char> mapData = std::move(Application::manager<Assets>()->readFile(tmxFile));
    tinyxml2::XMLError err = doc.Parse(mapData.data(), mapData.size());
    if (err != tinyxml2::XML_SUCCESS) {
        LERR("Error loading tmx file <", tmxFile, ">");
        return;
    }

    tinyxml2::XMLElement *mapElement = doc.FirstChildElement("map");
    if (!mapElement) {
        LERR(tmxFile, ": not found <map> element");
        return;
    }

    // Map attributes
    std::string mapWidth = mapElement->Attribute("width");
    std::string mapHeight = mapElement->Attribute("height");

    if (mapWidth.empty() || mapHeight.empty()) {
        LERR(tmxFile, ": Wrong map size");
        return;
    }

    sf::Vector2<size_t> mapSize(std::atoll(mapWidth.c_str()), std::atoll(mapHeight.c_str()));

    if (mapSize.x == 0 || mapSize.y == 0) {
        LERR(tmxFile, ": Wrong map size");
        return;
    }

    m_mapSize = mapSize;

    std::string tileWidth = mapElement->Attribute("tilewidth");
    std::string tileHeight = mapElement->Attribute("tileheight");

    if (tileWidth.empty() || tileHeight.empty()) {
        LERR(tmxFile,": Wrong tile size");
        return;
    }

    sf::Vector2<uint32_t> tileSize(std::atoi(tileWidth.c_str()), std::atoi(tileHeight.c_str()));

    if (tileSize.x == 0 || tileSize.y == 0) {
        LERR(tmxFile,": Wrong tile size");
        return;
    }

    m_tileSize = tileSize;

    if (m_chunkSize.x == 0 || m_chunkSize.y == 0)
        m_chunkSize = m_mapSize;

    // Tilesets
    tinyxml2::XMLElement *tilesetElement = mapElement->FirstChildElement("tileset");
    if (!tilesetElement) {
        LERR(tmxFile, ": Tileset not found");
        return;
    }

    while (tilesetElement) {
        uint32_t firstGid = std::atoi(tilesetElement->Attribute("firstgid"));
        std::string tilesetFile = tilesetElement->Attribute("source");

        if (tilesetFile.empty()) {
            LERR(tmxFile,": Tileset element: attribute <source> is empty");
            return;
        }

        Tileset *tileset = loadTileset(Assets::sourceFromPath(tmxFile), tilesetFile);
        if (!tileset)
            return;
        tileset->firstGid = firstGid;

        tilesetElement = tilesetElement->NextSiblingElement("tileset");
    }

    // Layers
    tinyxml2::XMLElement *layerElement = mapElement->FirstChildElement("layer");
    if (!layerElement) {
        LERR(tmxFile,": Layers not found");
        m_tilesets.clear();
        return;
    }

    while (layerElement) {
        std::string name = layerElement->Attribute("name");

        std::unordered_map<std::string, std::string> properties;
        tinyxml2::XMLElement *layerPropertiesElement = layerElement->FirstChildElement("properties");
        if (layerPropertiesElement) {

            tinyxml2::XMLElement *layerPropertyElement = layerPropertiesElement->FirstChildElement("property");
            if (!layerPropertyElement) {
                layerElement = layerElement->NextSiblingElement("layer");
                continue;
            }

            while (layerPropertyElement) {
                std::string propName = layerPropertyElement->Attribute("name");
                std::string propValue = layerPropertyElement->Attribute("value");
                if (!propName.empty())
                    properties.emplace(propName, propValue);

                layerPropertyElement = layerPropertyElement->NextSiblingElement("property");
            }

            if (properties.find("tileset") == properties.end()) {
                layerElement = layerElement->NextSiblingElement("layer");
                continue;
            }
        }

        Tileset *ts = tileset(properties.at("tileset"));
        if (!ts) {
            layerElement = layerElement->NextSiblingElement("layer");
            continue;
        }

        // Tiles
        tinyxml2::XMLElement *dataElement = layerElement->FirstChildElement("data");
        if (!dataElement) {
            layerElement = layerElement->NextSiblingElement("layer");
            continue;
        }

        tinyxml2::XMLElement *tileElement = dataElement->FirstChildElement("tile");
        if (!tileElement) {
            layerElement = layerElement->NextSiblingElement("layer");
            continue;
        }

        Layer *layer = new Layer();
        updateChuncks(layer);
        layer->name = name;
        layer->properties = std::move(properties);
        layer->tileset = ts;

        int tilePos = 0;
        while (tileElement) {
            const char *gidattr = tileElement->Attribute("gid");
            if (gidattr == 0) {
                ++tilePos;
                tileElement = tileElement->NextSiblingElement("tile");
                continue;
            }
            int32_t gid = std::atoi(tileElement->Attribute("gid"));
            gid -= ts->firstGid;

            if (gid >= 0) {
                int32_t texX = gid % ts->columns;
                int32_t texY = gid / ts->columns;

                int32_t tileX = tilePos % m_mapSize.x;
                int32_t tileY = tilePos / m_mapSize.x;

                setTile(layer, tileX, tileY, sf::Vector2i(texX, texY));
            }

            ++tilePos;
            tileElement = tileElement->NextSiblingElement("tile");
        }

        m_layers.push_back(uuptr<Layer>(layer));

        layerElement = layerElement->NextSiblingElement("layer");
    }

}

TileMap::Tileset *TileMap::loadTileset(const std::string assetsSource, const std::string &tsxFile)
{
    // Tileset document
    tinyxml2::XMLDocument tsxDoc;
//    tinyxml2::XMLError err = tsxDoc.LoadFile(tsxFile.c_str());
    std::vector<char> tilesetData = std::move(Application::manager<Assets>()->readFile(assetsSource + ":" + tsxFile));
    tinyxml2::XMLError err = tsxDoc.Parse(tilesetData.data(), tilesetData.size());
    if (err != tinyxml2::XML_SUCCESS) {
        LERR("Error laoding tsx file <", tsxFile, ">");
        return nullptr;
    }

    tinyxml2::XMLElement *tilesetElement = tsxDoc.FirstChildElement("tileset");
    if (!tilesetElement) {
        LERR(tsxFile, ": Tileset not found");
        return nullptr;
    }

    std::string name = tilesetElement->Attribute("name");

    size_t tileCount = std::atoi(tilesetElement->Attribute("tilecount"));
    size_t columns = std::atoi(tilesetElement->Attribute("columns"));
    if (tileCount == 0 || columns == 0) {
        LERR(tsxFile, ": Wrong tile count or columns");
        return nullptr;
    }

    tinyxml2::XMLElement *imageElement = tilesetElement->FirstChildElement("image");
    if (!imageElement) {
        LERR(tsxFile, ": Image not found");
        return nullptr;
    }

    std::string imageSource = imageElement->Attribute("source");
    if (imageSource.empty()) {
        LERR(tsxFile, ": Image element: attribute <source> is empty");
        return nullptr;
    }

    std::string imageWidth = imageElement->Attribute("width");
    std::string imageHeight = imageElement->Attribute("height");
    sf::Vector2i imageSize(std::atoi(imageWidth.c_str()), std::atoi(imageHeight.c_str()));

    if (imageSize.x == 0 || imageSize.y == 0) {
        LERR(tsxFile, ": Wrong image size");
        return nullptr;
    }

    StaticTexture *texture = Application::manager<Textures>()->loadStaticTexture(tsxFile + "_" + imageSource,
                                                                                 assetsSource + ":" + imageSource,
                                                                                 sf::IntRect(0, 0, imageSize.x, imageSize.y));
    if (!texture)
        return nullptr;

    // Tileset properties
    std::unordered_map<std::string, std::string> tilesetProperties;
    tinyxml2::XMLElement *tilesetPropertiesElement = tilesetElement->FirstChildElement("properties");
    if (tilesetPropertiesElement) {
        tinyxml2::XMLElement *tilesetPropertyElement = tilesetPropertiesElement->FirstChildElement("property");
        while (tilesetPropertyElement) {
            std::string propName = tilesetPropertyElement->Attribute("name");
            std::string propValue = tilesetPropertyElement->Attribute("value");
            if (!propName.empty())
                tilesetProperties.emplace(propName, propValue);

            tilesetPropertyElement = tilesetPropertyElement->NextSiblingElement("property");
        }
    }

    // Tile properties
    std::unordered_map<size_t, std::unordered_map<std::string, std::string>> tileProperties;
    tinyxml2::XMLElement *tileElement = tilesetElement->FirstChildElement("tile");

    while (tileElement) {
        const char *idattr = tileElement->Attribute("id");
        if (idattr == 0) {
            tileElement = tileElement->NextSiblingElement("tile");
            continue;
        }

        size_t id = std::atoi(idattr);

        std::unordered_map<std::string, std::string> properties;
        tinyxml2::XMLElement *propertiesElement = tileElement->FirstChildElement("properties");
        if (propertiesElement) {
            tinyxml2::XMLElement *propertyElement = propertiesElement->FirstChildElement("property");
            while (propertyElement) {
                std::string propName = propertyElement->Attribute("name");
                std::string propValue = propertyElement->Attribute("value");
                if (!propName.empty())
                    properties.emplace(propName, propValue);

                propertyElement = propertyElement->NextSiblingElement("property");
            }
        }

        tileProperties.emplace(id, std::move(properties));

        tileElement = tileElement->NextSiblingElement("tile");
    }

    Tileset *tileset = new Tileset();
    tileset->name = name;
    tileset->tileCount = tileCount;
    tileset->columns = columns;
    tileset->texture = texture;
    tileset->properties = std::move(tilesetProperties);
    tileset->tileProperties = std::move(tileProperties);

    m_tilesets.push_back(uuptr<Tileset>(tileset));

    return tileset;
}

}
