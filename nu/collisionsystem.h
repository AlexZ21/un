﻿#ifndef COLLISIONSYSTEM_H
#define COLLISIONSYSTEM_H

#include <un/un_export.h>
#include <un/un_defines.h>
#include <un/core/scenesystem.h>
#include <un/components/transformcomponent.h>
#include <un/components/collisioncomponent.h>
#include <un/components/scenetreecomponent.h>
#include <un/components/rendercomponent.h>

namespace un {

class UN_EXPORT CollisionSystem :
        public SceneSystem<Filter::Requires<CollisionComponent, TransformSystem, SceneTreeComponent, RenderComponent>>
{
public:
    CollisionSystem() = default;
    ~CollisionSystem() = default;

    utils::TypeId typeId() {
        return utils::ClassTypeId<BaseSystem>::id<CollisionSystem>();
    }

    bool checkCollision(const Entity &entity1, const Entity &entity2);

    void update(sf::Time);

private:
    void isCollided(const Entity &entity);
    bool isCollided(const Entity &entity1, const Entity &entity2);
};

}

#endif // COLLISIONSYSTEM_H
