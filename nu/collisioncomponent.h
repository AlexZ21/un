#ifndef COLLISIONCOMPONENT_H
#define COLLISIONCOMPONENT_H

#include <un/un_export.h>
#include <un/un_defines.h>
#include <un/core/component.h>

#include <SFML/System/Vector2.hpp>

namespace un {

class CollisionSystem;

class UN_EXPORT CollisionComponent : public Component
{
    friend class CollisionSystem;
public:
    using Polygon = std::vector<sf::Vector2f>;

    CollisionComponent();
    ~CollisionComponent() = default;

    bool isCollided() const;

    void setVertices(std::vector<sf::Vector2f> vertices);
    const std::vector<Polygon> &polygons() const;

private:
    bool m_collided;
    std::vector<Polygon> m_polygons;

};

}

#endif // COLLISIONCOMPONENT_H
