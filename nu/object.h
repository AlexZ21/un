#ifndef OBJECT_H
#define OBJECT_H

#include <un/un_export.h>

#include <vector>
#include <stdint.h>

namespace un {

class ObjectStorage;

class UN_EXPORT Object
{
    friend class ObjectStorage;
public:
    struct Id {
        bool operator==(const Id &other) const {
            return index == other.index && counter == other.counter;
        }
        int32_t index = -1;
        int32_t counter = -1;
    };

    Object();
    Object(Id id, ObjectStorage *storage);
    virtual ~Object() = default;

    const Id &id() const;
    ObjectStorage *storage() const;

    void destroy();
    bool isValid() const;

    template<typename T>
    T *get() const {
        return static_cast<T*>(get());
    }

    bool operator==(const Object &other) const {
        return m_id == other.m_id && m_storage == other.m_storage;
    }

protected:
    void *get() const;

protected:
    Id m_id;
    ObjectStorage *m_storage;

};

template<typename T>
class TObject : public Object
{
    friend class ObjectStorage;
public:
    TObject() = default;
    TObject(Id id, ObjectStorage *storage) : Object(id, storage) {}
    TObject(const Object &other) {
        m_id = other.id();
        m_storage = other.storage();
    }
    ~TObject() = default;

    T *get() const {
        return Object::get<T>();
    }

    TObject<T> &operator=(const Object &other) {
        m_id = other.id();
        m_storage = other.storage();
        return *this;
    }

    T *operator->() {
        return Object::get<T>();
    }
};

class ObjectIdPool
{
public:
    ObjectIdPool(size_t poolSize);
    ~ObjectIdPool() = default;

    Object::Id create();
    void remove(const Object::Id &objectId);
    Object::Id at(size_t index) const;
    bool isValid(const Object::Id &objectId) const;

    size_t size() const;
    void resize(size_t size);

    void clear();

private:
    size_t m_poolSize;
    int32_t m_nextid;
    std::vector<Object::Id> m_freeids;
    std::vector<int32_t> m_counts;
};

}

#endif // OBJECT_H
