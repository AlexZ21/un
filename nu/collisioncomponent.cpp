#include <un/components/collisioncomponent.h>
#include <un/utils/bayazitdecomposer.h>

namespace un {

CollisionComponent::CollisionComponent() :
    m_collided(false)
{

}

bool CollisionComponent::isCollided() const
{
    return m_collided;
}

void CollisionComponent::setVertices(std::vector<sf::Vector2f> vertices)
{
    auto polygons = utils::BayazitDecomposer::decompose(vertices);
    for (Polygon &polygon : polygons) {
        Polygon p = std::move(polygon);
        m_polygons.push_back(p);
    }
}

const std::vector<CollisionComponent::Polygon> &CollisionComponent::polygons() const
{
    return m_polygons;
}

}
