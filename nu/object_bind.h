#ifndef OBJECT_BIND_H
#define OBJECT_BIND_H

#include <un/core/object.h>
#include <un/core/objectstorage.h>

#include <sol/sol.hpp>

namespace sol {
template <typename T>
struct unique_usertype_traits<un::TObject<T>> {
    typedef T type;
    typedef un::TObject<T> actual_type;
    static const bool value = true;
    static bool is_null(const actual_type& value) {
        return value.get() == nullptr;
    }
    static type* get (const actual_type& p) {
        return p.get();
    }
};
}

namespace un {

inline void luaObjectBind(sol::state *state)
{
    // un namespace
    sol::table unTable = state->get<sol::table>("un");

    unTable.new_usertype<Object>("Object",
                                 sol::constructors<Object(), Object(Object::Id, ObjectStorage *)>(),
                                 "id", &Object::id,
                                 "storage", &Object::storage,
                                 "destroy", &Object::destroy,
                                 "isValid", &Object::isValid);

}

}

#endif // OBJECT_BIND_H
