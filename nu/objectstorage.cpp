#include <un/core/objectstorage.h>

namespace un {

ObjectStorage *ObjectStorage::m_instance = nullptr;

ObjectStorage *ObjectStorage::create(size_t poolSize)
{
    if (m_instance)
        return m_instance;
    static ObjectStorage storage(poolSize);
    m_instance = &storage;
    return m_instance;
}

ObjectStorage *ObjectStorage::instnce()
{
    return m_instance;
}

void ObjectStorage::destroy(const Object &object, bool destroyData)
{
    if (!m_instance || !m_instance->m_pool.isValid(object.m_id))
        return;
    m_instance->m_pool.remove(object.m_id);
    if (destroyData)
        m_instance->m_dataStorage[object.m_id.index].reset();
    else
        m_instance->m_dataStorage[object.m_id.index].release();
}

void ObjectStorage::clear()
{
    if (!m_instance)
        return;
    m_instance->m_dataStorage.clear();
    m_instance->m_pool.clear();
}

bool ObjectStorage::isValid(const Object &object)
{
    if (!m_instance)
        return false;
    return m_instance->m_pool.isValid(object.m_id);
}

void ObjectStorage::resize(size_t poolSize)
{
    if (!m_instance)
        return;
    m_instance->m_dataStorage.resize(poolSize);
    m_instance->m_pool.resize(poolSize);
}

void *ObjectStorage::get(const Object &object)
{
    if (!m_instance || !m_instance->m_pool.isValid(object.m_id))
        return nullptr;
    return m_instance->m_dataStorage[object.m_id.index]->data;

}

ObjectStorage::ObjectStorage(size_t poolSize) :
    m_pool(poolSize),
    m_dataStorage(poolSize)
{

}

}
