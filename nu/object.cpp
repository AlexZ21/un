#include <un/core/object.h>
#include <un/core/objectstorage.h>

namespace un {

Object::Object() :
    m_storage(nullptr)
{

}

Object::Object(Object::Id id, ObjectStorage *storage) :
    m_id(id),
    m_storage(storage)
{

}

const Object::Id &Object::id() const
{
    return m_id;
}

ObjectStorage *Object::storage() const
{
    return m_storage;
}

void Object::destroy()
{
    if (!m_storage)
        return;
    m_storage->destroy(*this);
}

bool Object::isValid() const
{
    if (!m_storage)
        return false;
    return m_storage->isValid(*this);
}

void *Object::get() const
{
    if (!m_storage)
        return nullptr;
    return m_storage->get(*this);
}

ObjectIdPool::ObjectIdPool(size_t poolSize) :
    m_poolSize(poolSize),
    m_nextid(0),
    m_counts(poolSize)
{

}

Object::Id ObjectIdPool::create()
{
    Object::Id objectId;
    if(!m_freeids.empty()) {
        objectId = m_freeids.front();
        m_freeids.erase(m_freeids.begin());
    } else {
        objectId.index = m_nextid++;
        objectId.counter = 1;
        m_counts[objectId.index] = objectId.counter;
    }
    return objectId;
}

void ObjectIdPool::remove(const Object::Id &objectId)
{
    int32_t &counter = m_counts[objectId.index];
    ++counter;
    Object::Id fid = {objectId.index, counter};
    m_freeids.push_back(fid);
}

Object::Id ObjectIdPool::at(size_t index) const
{
    return index < m_counts.size() ?
                Object::Id{(int32_t)index, m_counts[index]} : Object::Id{(int32_t)index, 0};
}

bool ObjectIdPool::isValid(const Object::Id &objectId) const
{
    return objectId.index >= m_counts.size() ?
                false : ((objectId.counter == m_counts[objectId.index]) && (objectId.counter > 0));
}

size_t ObjectIdPool::size() const
{
    return m_counts.size();
}

void ObjectIdPool::resize(size_t size)
{
    m_counts.resize(size);
}

void ObjectIdPool::clear()
{
    m_counts.clear();
    m_freeids.clear();
    m_nextid = 0;
}

}
