#ifndef UN_TILEMAP_H
#define UN_TILEMAP_H

#include <un/un_export.h>
#include <un/un_defines.h>
#include <un/graphics/drawable.h>

#include <vector>
#include <unordered_map>
#include <memory>

namespace un {

class StaticTexture;
class AnimatedTextureState;

class UN_EXPORT TileMap : public Drawable
{
public:
    struct Chunk {
        std::vector<sf::Vertex> vertices;
    };

    struct Tileset {
        std::string name;
        StaticTexture *texture = nullptr;
        size_t tileCount = 0;
        size_t columns = 0;
        size_t firstGid = 0;
        std::unordered_map<std::string, std::string> properties;
        std::unordered_map<size_t, std::unordered_map<std::string, std::string>> tileProperties;
    };

    struct Layer {
        std::string name;
        Tileset *tileset = nullptr;
        std::unordered_map<uint64_t, uuptr<Chunk>> chunks;
        std::unordered_map<std::string, std::string> properties;
    };

    TileMap(size_t width, size_t height, uint32_t tileWidth, uint32_t tileHeight,
            StaticTexture *texture, size_t chunkWidth = 64, size_t chunkHeight = 64);
    TileMap(const std::string &tmxFile, size_t chunkWidth = 64, size_t chunkHeight = 64);
    ~TileMap() = default;

    void setTile(uint32_t layer, size_t tileX, size_t tileY, const sf::IntRect &textureRect);
    void setTile(uint32_t layer, size_t tileX, size_t tileY, const sf::Vector2i &texturePos);

    Texture *texture() const { return nullptr; }
    AnimatedTextureState *animatedTextureState() const { return nullptr; }

    sf::FloatRect localBounds() const;
    sf::FloatRect globalBounds() const;

    void draw(sf::RenderTarget &target, sf::RenderStates states) const;

private:
    void updateChuncks(Layer *layer);
    sf::Vector2i chuncksCount() const;

    void setTile(Layer *layer, size_t tileX, size_t tileY, const sf::IntRect &textureRect);
    void setTile(Layer *layer, size_t tileX, size_t tileY, const sf::Vector2i &texturePos);

    Tileset *tileset(const std::string &name);

    void loadMap(const std::string &tmxFile);
    Tileset *loadTileset(const std::string assetsSource, const std::string &tsxFile);

private:
    sf::Vector2<size_t> m_mapSize;
    sf::Vector2<uint32_t> m_tileSize;
    sf::Vector2<size_t> m_chunkSize;

    std::vector<uuptr<Layer>> m_layers;
    std::vector<uuptr<Tileset>> m_tilesets;

};

}

#endif // UN_TILEMAP_H
