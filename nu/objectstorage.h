#ifndef OBJECTSTORAGE_H
#define OBJECTSTORAGE_H

#include <un/un_export.h>
#include <un/core/object.h>

#include <memory>

namespace un {

class UN_EXPORT ObjectStorage
{
    friend class Object;
public:
    struct DataBase {
        virtual ~DataBase() = default;
        void *data = nullptr;
    };

    template<typename T>
    struct Data : DataBase {
        ~Data() {
            delete reinterpret_cast<T*>(data);
        }
    };

    static ObjectStorage *create(size_t poolSize);
    static ObjectStorage *instnce();

    template<typename T>
    static TObject<T> create(T *v){
        if (!m_instance)
            return Object();
        TObject<T> object(m_instance->m_pool.create(), m_instance);
        Data<T> *data = new Data<T>();
        data->data = v;
        m_instance->m_dataStorage[object.m_id.index].reset(data);
        return object;
    }
    template<typename T, typename... Args>
    static TObject<T> create(Args&&... args) {
        if (!m_instance)
            return TObject<T>();
        return create(new T(std::forward<Args>(args)...));
    }
    static void destroy(const Object &object, bool destroyData = true);
    static void clear();
    static bool isValid(const Object &object);

    static void resize(size_t poolSize);

    static void *get(const Object &object);
    template<typename T>
    T *get(const Object &object) const {
        if (!m_instance || !m_instance->m_pool.isValid(object.m_id))
            return nullptr;
        return static_cast<T*>(m_instance->m_dataStorage[object.m_id.index]->data);
    }

private:
    ObjectStorage(size_t poolSize);
    ~ObjectStorage() = default;

private:
    static ObjectStorage *m_instance;
    std::vector<std::unique_ptr<DataBase>> m_dataStorage;
    ObjectIdPool m_pool;
};

}

#endif // OBJECTSTORAGE_H
