###############################################################################
#
# Description       : CMake build script for zipper
# Original author(s): Frank Bergmann <fbergman@caltech.edu>
# Organization      : California Institute of Technology
#
###############################################################################

set(CMAKE_LEGACY_CYGWIN_WIN32 0)
cmake_minimum_required(VERSION 2.6)
project(zipper)

if (UNIX)
  set(ZIPPER_LIBRARY Zipper)
else()
  set(ZIPPER_LIBRARY libZipper)
endif()

set(USE_ZLIB ON)
add_definitions( -DUSE_ZLIB )

include_directories(BEFORE ${LIBZ_INCLUDE_DIR})
include_directories(BEFORE ${CMAKE_CURRENT_BINARY_DIR})
include_directories(BEFORE ${CMAKE_CURRENT_SOURCE_DIR})
include_directories(BEFORE ${CMAKE_CURRENT_SOURCE_DIR}/zipper)
include_directories(BEFORE ${CMAKE_CURRENT_SOURCE_DIR}/minizip)

file(GLOB MINIZIP_SOURCES 
  ${CMAKE_CURRENT_SOURCE_DIR}/minizip/*.c
  ${CMAKE_CURRENT_SOURCE_DIR}/minizip/*.h
  ${CMAKE_CURRENT_SOURCE_DIR}/minizip/aes/*.c
  ${CMAKE_CURRENT_SOURCE_DIR}/minizip/aes/*.h
)

FILE(COPY ${CMAKE_CURRENT_SOURCE_DIR}/minizip/zip.h
          ${CMAKE_CURRENT_SOURCE_DIR}/minizip/unzip.h
          ${CMAKE_CURRENT_SOURCE_DIR}/minizip/ioapi_mem.h
     DESTINATION
          ${CMAKE_CURRENT_BINARY_DIR}/zipper
)

list(REMOVE_ITEM MINIZIP_SOURCES
${CMAKE_CURRENT_SOURCE_DIR}/minizip/minizip.c
${CMAKE_CURRENT_SOURCE_DIR}/minizip/miniunz.c
)

if(UNIX AND NOT CYGWIN)
  list(REMOVE_ITEM MINIZIP_SOURCES
  ${CMAKE_CURRENT_SOURCE_DIR}/minizip/iowin32.c
  )
endif()
source_group(minizip FILES ${MINIZIP_SOURCES})


file(GLOB ZIPPER_SOURCES
  ${CMAKE_CURRENT_SOURCE_DIR}/zipper/*.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/zipper/*.h
)
source_group(zipper FILES ${ZIPPER_SOURCES})

option(BUILD_SHARED_VERSION "Build the shared version of the library." OFF)
if (BUILD_SHARED_VERSION)
  add_library (${ZIPPER_LIBRARY} SHARED ${ZIPPER_SOURCES} ${MINIZIP_SOURCES} )
  
  if (UNIX)
    SET_TARGET_PROPERTIES(${ZIPPER_LIBRARY} PROPERTIES
                          SOVERSION ${ZIPPER_VERSION_MAJOR}
                          VERSION ${ZIPPER_VERSION_MAJOR}.${ZIPPER_VERSION_MINOR}.${ZIPPER_VERSION_PATCH})
  endif()
  
  target_link_libraries(${ZIPPER_LIBRARY} ${LIBZ_LIBRARY} ${EXTRA_LIBS})
endif()

option(BUILD_STATIC_VERSION "Build the static version of the library." ON)
if (BUILD_STATIC_VERSION)
  add_library (${ZIPPER_LIBRARY}-static STATIC ${ZIPPER_SOURCES} ${MINIZIP_SOURCES})
  
  target_link_libraries(${ZIPPER_LIBRARY}-static ${LIBZ_LIBRARY} ${EXTRA_LIBS})
endif()

